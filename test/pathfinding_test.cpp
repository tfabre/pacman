#include <gtest/gtest.h>
#include <memory>
#include <stack>

#include "pacman/model/character/Positionned.hpp"
#include "pacman/model/character/enemy/PathFinding.hpp"
#include "pacman/util/Vector2.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"020000000000002001000__00010020000001111112111011111101112111111000000200100" \
"0000001002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000011111111111100111111111" \
"1110020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")

class PathFindingTest: public ::testing::Test
{
protected:
    std::stack< pm::Vector2<float> >* stk;
    pm::TileMap* tm;
    pm::PathFinding* pf;
    void SetUp()
    {
        stk = new std::stack< pm::Vector2<float> >;
        tm = new pm::TileMap(X, Y, MAP, nullptr);
        pf = new pm::PathFinding( tm );
    }

    void TearDown()
    {
        delete pf;
        delete tm;
        delete stk;
    }
};

TEST_F(PathFindingTest, distance_test)
{
    ASSERT_EQ( 10, pf->getDistance(1,1,6,6) );
    ASSERT_EQ( 10, pf->getDistance(6,6,1,1) );
}

TEST_F(PathFindingTest, path_test_1)
{
    pf->path( stk, 2, 2, 6, 4 );
    pm::Vector2<float> v = stk->top();
    ASSERT_EQ( 2, v.x );
    ASSERT_EQ( 1, v.y );

    stk->pop();
    v = stk->top();
    ASSERT_EQ( 3, v.x );
    ASSERT_EQ( 1, v.y );

    stk->pop();
    v = stk->top();
    ASSERT_EQ( 4, v.x );
    ASSERT_EQ( 1, v.y );

    stk->pop();
    v = stk->top();
    ASSERT_EQ( 5, v.x );
    ASSERT_EQ( 1, v.y );

    stk->pop();
    v = stk->top();
    ASSERT_EQ( 6, v.x );
    ASSERT_EQ( 1, v.y );

    stk->pop();
    v = stk->top();
    ASSERT_EQ( 6, v.x );
    ASSERT_EQ( 2, v.y );

    stk->pop();
    v = stk->top();
    ASSERT_EQ( 6, v.x );
    ASSERT_EQ( 3, v.y );

    stk->pop();
    v = stk->top();
    ASSERT_EQ( 6, v.x );
    ASSERT_EQ( 4, v.y );
}
