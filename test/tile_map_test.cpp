#include <gtest/gtest.h>

#include "pacman/util/Vector2.hpp"
#include "pacman/model/item/ItemFactory.hpp"
#include "pacman/model/map/TileMap.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"020000000000002001000__00010020000001111112111011111101112111111000000200100" \
"0000001002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000011111111111100111111111" \
"1110020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")

class TileMapTest: public ::testing::Test
{
protected:
    pm::TileMap tm{X, Y, MAP, nullptr};

    void SetUp() {}
    void TearDown() {}
};

TEST_F(TileMapTest, get_tile_wall_test)
{
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.2f, 0.f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.4f, 0.f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.6f, 0.f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.8f, 0.f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.9f, 0.f)));

    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.1f, 0.1f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.3f, 0.3f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.5f, 0.5f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.7f, 0.7f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.9f, 0.9f)));

    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.1f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.3f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.5f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.7f)));
    ASSERT_EQ(pm::WALL, tm.getTile(pm::Vector2f(0.f, 0.9f)));    
}

TEST_F(TileMapTest, get_tile_ground_test)
{
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.2f, 1.f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.4f, 1.f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.6f, 1.f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.8f, 1.f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.9f, 1.f)));

    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.1f, 1.1f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.3f, 1.3f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.5f, 1.5f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.7f, 1.7f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.9f, 1.9f)));

    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.1f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.3f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.5f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.7f)));
    ASSERT_EQ(pm::GROUND, tm.getTile(pm::Vector2f(1.f, 1.9f)));
}

TEST_F(TileMapTest, get_item_collision_test)
{
    const pm::Item none = pm::ItemFactory::getItem(pm::NONE);
    const pm::Item dot  = pm::ItemFactory::getItem(pm::DOT);

    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.2f, 1.f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.4f, 1.f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.6f, 1.f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.8f, 1.f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.9f, 1.f)));

    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.1f, 1.1f)));
    ASSERT_EQ(dot, tm.getItem(pm::Vector2f(1.3f, 1.3f)));
    ASSERT_EQ(dot, tm.getItem(pm::Vector2f(1.5f, 1.5f)));
    ASSERT_EQ(dot, tm.getItem(pm::Vector2f(1.7f, 1.7f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.9f, 1.9f)));

    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.1f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.3f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.5f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.7f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.f, 1.9f)));
}

TEST_F(TileMapTest, get_power_pellet_test)
{
    const pm::Item pp = pm::ItemFactory::getItem(pm::POWER_PELLET);

    ASSERT_EQ(pp, tm.getItem(pm::Vector2f(1.3f, 3.3f)));
    ASSERT_EQ(pp, tm.getItem(pm::Vector2f(1.5f, 3.5f)));
    ASSERT_EQ(pp, tm.getItem(pm::Vector2f(1.7f, 3.7f)));
}

TEST_F(TileMapTest, get_none_test)
{
    const pm::Item none = pm::ItemFactory::getItem(pm::NONE);

    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.3f, 14.3f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.5f, 14.5f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.7f, 14.7f)));
}

TEST_F(TileMapTest, get_none_in_wall_test)
{
    const pm::Item none = pm::ItemFactory::getItem(pm::NONE);

    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.3f, 11.3f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.5f, 11.5f)));
    ASSERT_EQ(none, tm.getItem(pm::Vector2f(1.7f, 11.7f)));
}
