#include <gtest/gtest.h>

#include "pacman/model/character/enemy/behavior/decorator/Eatable.hpp"

namespace
{

class BehaviorImpl: public pm::EnemyBehavior
{
public:
    using pm::EnemyBehavior::EnemyBehavior;

    virtual void move()
    {
        pm::Vector2f pos = this->getPosition();
        pos.x -= 0.5;
        this->setPosition(pos);
    }
};

class Obs: public pm::Observer
{
public:
    Obs(bool s=false): state(s)
    {}

    virtual void actualisate(const pm::Event & event) override
    {
        state = (event == pm::EAT_GHOST);
    }

    bool getState() const
    {
        return state;
    }

private:
    bool state;
};

}

class EatableTest: public ::testing::Test
{
    protected:
    Obs* obs;
    pm::EnemyBehavior* behavior;
    pm::Pacman* pacman;
    pm::TileMap* tm;

    void SetUp()
    {
        pm::Vector2f pos(0.f, 0.f);
        tm = new pm::TileMap(2, 1, std::string("11"), nullptr);
        pacman = new pm::Pacman(pos, 0.1, tm, pos, pm::LEFT);
        pacman->setSize(0.5);
        pos = pm::Vector2f(2.f, 0.f);
        behavior = new pm::Eatable(new BehaviorImpl(pos, 0.1, 0.5, tm, pacman,
                                                    nullptr));

        obs = new Obs();
        static_cast<pm::Eatable*>(behavior)->addObserver(*obs);
    }

    void TearDown()
    {
        delete obs;
        delete behavior;
        delete pacman;
        delete tm;
    }
};

TEST_F(EatableTest, eatable_test)
{
    behavior->move();
    ASSERT_FLOAT_EQ(behavior->getPosition().x, 1.5);
    ASSERT_FLOAT_EQ(behavior->getPosition().y, 0.f);
    ASSERT_FALSE(obs->getState());

    behavior->move();
    ASSERT_FLOAT_EQ(behavior->getPosition().x, 1.f);
    ASSERT_FLOAT_EQ(behavior->getPosition().y, 0.f);
    ASSERT_TRUE(obs->getState());
}

