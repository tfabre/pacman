INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/../include)

SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/build/test)

# Directories
SET(SRC_DIR       ${CMAKE_SOURCE_DIR}/src)
SET(PACMAN_DIR    ${SRC_DIR}/pacman)
SET(UTIL_DIR      ${PACMAN_DIR}/util)
SET(MODEL_DIR     ${PACMAN_DIR}/model)
SET(CHAR_DIR      ${MODEL_DIR}/character)
SET(ENEMY_DIR     ${CHAR_DIR}/enemy)
SET(BEHAVIOR_DIR  ${ENEMY_DIR}/behavior)
SET(DECORATOR_DIR ${BEHAVIOR_DIR}/decorator)
SET(MAP_DIR       ${MODEL_DIR}/map)
SET(ITEM_DIR      ${MODEL_DIR}/item)

SET(DEPENDENCES ${UTIL_DIR}/Subject.cpp
                ${UTIL_DIR}/Observer.cpp
                ${UTIL_DIR}/Timer.cpp
                ${MODEL_DIR}/GameState.cpp
                ${MODEL_DIR}/GameFactory.cpp
                ${MODEL_DIR}/ConfModel.cpp
                ${CHAR_DIR}/Positionned.cpp
                ${CHAR_DIR}/Pacman.cpp
                ${ITEM_DIR}/Item.cpp
                ${ITEM_DIR}/ItemFactory.cpp
                ${MAP_DIR}/TileMap.cpp
                ${MAP_DIR}/Level.cpp
                ${ENEMY_DIR}/PathFinding.cpp
                ${ENEMY_DIR}/Ghost.cpp
                ${BEHAVIOR_DIR}/EnemyBehavior.cpp
                ${BEHAVIOR_DIR}/Shadow.cpp
                ${BEHAVIOR_DIR}/Speedy.cpp
                ${BEHAVIOR_DIR}/Bashful.cpp
                ${BEHAVIOR_DIR}/Pokey.cpp
                ${BEHAVIOR_DIR}/Patrolling.cpp
                ${BEHAVIOR_DIR}/Leak.cpp
                ${BEHAVIOR_DIR}/BackHome.cpp
                ${DECORATOR_DIR}/EnemyBehaviorDecorator.cpp
                ${DECORATOR_DIR}/PowerPelletAffected.cpp
                ${DECORATOR_DIR}/PacmanEater.cpp
                ${DECORATOR_DIR}/Eatable.cpp)

SET(TEST_FILES tests.cpp
               observer_test.cpp
               pathfinding_test.cpp
               game_state_test.cpp
               game_factory_test.cpp
               positionned_test.cpp
               pacman_test.cpp
               item_test.cpp
               tile_map_test.cpp
               level_test.cpp
               enemy_behavior_test.cpp
               shadow_behavior_test.cpp
               speedy_behavior_test.cpp
               bashful_behavior_test.cpp
               pokey_behavior_test.cpp
               patrolling_behavior_test.cpp
               leak_behavior_test.cpp
               backhome_behavior_test.cpp
               enemy_behavior_decorator_test.cpp
               pacman_eater_test.cpp
               eatable_test.cpp
               ghost_test.cpp
               power_pellet_affected_test.cpp)

FIND_PACKAGE(GTest REQUIRED)
FIND_PACKAGE(Threads REQUIRED)

ADD_EXECUTABLE(tests ${TEST_FILES} ${DEPENDENCES})
TARGET_LINK_LIBRARIES(tests ${GTEST_BOTH_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})

ADD_TEST(NAME tests
         COMMAND tests)
