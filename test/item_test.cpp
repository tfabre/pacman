#include <gtest/gtest.h>

#include "pacman/model/item/ItemFactory.hpp"

TEST(ItemFactoryTest, getItemValue)
{
    pm::Item dot = pm::ItemFactory::getItem(pm::DOT);
    ASSERT_EQ(10, dot.getScore());
}