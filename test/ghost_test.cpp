#include <gtest/gtest.h>

#include "pacman/model/character/enemy/Ghost.hpp"
#include "pacman/model/GameFactory.hpp"

namespace
{

class BehaviorImpl: public pm::EnemyBehavior
{
public:
    using pm::EnemyBehavior::EnemyBehavior;

    void move()
    {
        pm::Vector2f pos = this->getPosition();
        pos.x += 1;
        this->setPosition(pos);
    }
};

}

class GhostTest: public ::testing::Test
{
protected:
    pm::Ghost* ghost;
    pm::TileMap* tm;
    
    void SetUp()
    {
        pm::Vector2f pos(0.f, 0.f);
        tm = new pm::TileMap(2, 1, "11", nullptr);
        ghost = new pm::Ghost(new BehaviorImpl(pos, 0.1, 1.f, tm, nullptr,
                                               nullptr), nullptr);
    }

    void TearDown()
    {
        pm::GameFactory::deleteGameModel();
        delete ghost;
        delete tm;
    }
};

TEST_F(GhostTest, move_test)
{
    ghost->move();
    ASSERT_FLOAT_EQ(ghost->getPosition().x, 1);
    ASSERT_FLOAT_EQ(ghost->getPosition().y, 0);
}
