#include <gtest/gtest.h>

#include <chrono>
#include <thread>

#include "pacman/util/types.hpp"
#include "pacman/util/Observer.hpp"
#include "pacman/model/map/TileMap.hpp"
#include "pacman/model/character/Pacman.hpp"
#include "pacman/model/character/enemy/behavior/decorator/PowerPelletAffected.hpp"

#define WAIT_DURATION 2.f

class Subj: public pm::Subject
{
public:
    ~Subj() {};
};

class BehaviorRightImpl: public pm::EnemyBehavior
{
public:
    using pm::EnemyBehavior::EnemyBehavior;

    void move()
    {
        pm::Vector2f pos = this->getPosition();
        pos.x += getSpeed();
        this->setPosition(pos);
    }
};

class BehaviorLeftImpl: public pm::EnemyBehavior
{
public:
    using pm::EnemyBehavior::EnemyBehavior;

    void move()
    {
        pm::Vector2f pos = this->getPosition();
        pos.x -= getSpeed();
        this->setPosition(pos);
    }
};

class PowerPelletAffectedTest: public ::testing::Test
{
protected:
    Subj* subj;
    pm::PowerPelletAffected* enemy;
    pm::Pacman* pacman;
    pm::TileMap* tm;
    
    virtual void SetUp()
    {
        subj = new Subj();
        pm::Vector2f pos(0.5, 0.5);
        tm = new pm::TileMap(0, 0, std::string(""), nullptr);
        pacman = new pm::Pacman(pos, 0.1, tm, pos, pm::LEFT);
        
        pm::EnemyBehavior* right = new BehaviorRightImpl(pos, 0.2, 1.f, tm,
                                                         pacman, nullptr);
        pm::EnemyBehavior* left = new BehaviorLeftImpl(pos, 0.2, 1.f, tm,
                                                       pacman, nullptr);
        enemy = new pm::PowerPelletAffected(right, left, WAIT_DURATION);
        subj->addObserver(*enemy);
    }

    virtual void TearDown()
    {
        delete enemy;
        delete pacman;
        delete tm;
        delete subj;
    }
};

TEST_F(PowerPelletAffectedTest, pellet_non_activate_test)
{
    enemy->move();
    ASSERT_FLOAT_EQ(enemy->getPosition().x, .7);
    ASSERT_FLOAT_EQ(enemy->getPosition().y, .5);
}

TEST_F(PowerPelletAffectedTest, pellet_activate_test)
{
    enemy->move();
    enemy->move();
    ASSERT_FLOAT_EQ(enemy->getPosition().x, .9);
    ASSERT_FLOAT_EQ(enemy->getPosition().y, .5);

    subj->notify(pm::EAT_POWER_PELLET);
    enemy->move();
    std::this_thread::sleep_for(pm::fsec(1.f));
    enemy->move();
    ASSERT_FLOAT_EQ(enemy->getPosition().x, .5);
    ASSERT_FLOAT_EQ(enemy->getPosition().y, .5);

    std::this_thread::sleep_for(pm::fsec(1.f));
    enemy->move();
    ASSERT_FLOAT_EQ(enemy->getPosition().x, .7);
    ASSERT_FLOAT_EQ(enemy->getPosition().y, .5);
}
