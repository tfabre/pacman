#include <gtest/gtest.h>

#include "pacman/util/Observer.hpp"
#include "pacman/model/character/Pacman.hpp"
#include "pacman/model/ConfModel.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"020000000000002001000__00010020000001111112111011111101112111111000000200100" \
"0000001002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000011111111111100111111111" \
"1110020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")

namespace pm
{

class ObsTest: public Observer
{
public:
    ObsTest(): state(false) {}
    
    void actualisate(const Event & event)
    {
        state = (event == EAT_ITEM);
    }

    bool getState() const { return this->state; }

private:
    bool state;
};

class PacmanTest: public ::testing::Test
{
protected:
    Pacman* pacman;
    TileMap* tm;
    ObsTest* obs;

    void test_end_to_end(const float& speed);

    void SetUp()
    {
        obs = new ObsTest();
        tm = new TileMap(X, Y, MAP, nullptr);
        Vector2f pos(1.5, 1.5);
        pacman = new Pacman(pos, 0.1, tm, pos, LEFT);
        pacman->addObserver(*obs);
    }

    void TearDown()
    {
        delete pacman;
        delete obs;
        delete tm;
    }
};

TEST_F(PacmanTest, get_surrounding_test)
{
    TileType surround[8];
    Vector2f pos(1.5, 1.5);
    pacman->getSurrounding(pos, surround);

    ASSERT_EQ(surround[0], WALL);
    ASSERT_EQ(surround[1], WALL);
    ASSERT_EQ(surround[2], WALL);
    ASSERT_EQ(surround[3], WALL);
    ASSERT_EQ(surround[4], GROUND);
    ASSERT_EQ(surround[5], WALL);
    ASSERT_EQ(surround[6], GROUND);
    ASSERT_EQ(surround[7], WALL);

    pos = Vector2f(6.5, 5.5);
    pacman->getSurrounding(pos, surround);
    ASSERT_EQ(surround[0], WALL);
    ASSERT_EQ(surround[1], GROUND);
    ASSERT_EQ(surround[2], WALL);
    ASSERT_EQ(surround[3], GROUND);
    ASSERT_EQ(surround[4], GROUND);
    ASSERT_EQ(surround[5], WALL);
    ASSERT_EQ(surround[6], GROUND);
    ASSERT_EQ(surround[7], WALL);
}

TEST_F(PacmanTest, have_collision_with_test)
{
    ASSERT_TRUE(pacman->haveCollisionWith(WALL));
    ASSERT_TRUE(pacman->haveCollisionWith(GHOST_DOOR));
    ASSERT_FALSE(pacman->haveCollisionWith(GROUND));
}

TEST_F(PacmanTest, detect_collision_test)
{
    ASSERT_TRUE(pacman->detectCollision(Vector2f(1.25, 1.25)));
    ASSERT_TRUE(pacman->detectCollision(Vector2f(1.25, 1.75)));
    ASSERT_TRUE(pacman->detectCollision(Vector2f(1.75, 1.25)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(1.75, 1.75)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(1.5, 1.5)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(1.9, 1.5)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(1.5, 1.9)));

    ASSERT_TRUE(pacman->detectCollision(Vector2f(6.2, 5.2)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.25, 5.25)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.25, 5.75)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.75, 5.25)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.75, 5.75)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.5, 5.75)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.5, 5.5)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.5, 5.25)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.25, 5.5)));
    ASSERT_FALSE(pacman->detectCollision(Vector2f(6.75, 5.5)));
}

TEST_F(PacmanTest, move_test)
{
    Vector2f ori = pacman->getPosition();
    pacman->changeDirection(RIGHT);
    pacman->move();
    Vector2f pos = pacman->getPosition();
    const float speed = pacman->getSpeed();
    ASSERT_FLOAT_EQ(pos.x, ori.x + speed);
    ASSERT_FLOAT_EQ(pos.y, ori.y);

    pacman->changeDirection(LEFT);
    pacman->move();
    pos = pacman->getPosition();
    ASSERT_FLOAT_EQ(pos.x, ori.x);
    ASSERT_FLOAT_EQ(pos.y, ori.y);
    
    pacman->move();
    pos = pacman->getPosition();
    ASSERT_FLOAT_EQ(pos.x, ori.x);
    ASSERT_FLOAT_EQ(pos.y, ori.y);
}

TEST_F(PacmanTest, move_notification_test)
{
    EXPECT_FALSE(obs->getState());
    pacman->changeDirection(RIGHT);
    while (pacman->getPosition().x < 2.25)
        pacman->move();

    ASSERT_TRUE(obs->getState());
    ASSERT_EQ(pacman->getEatenItem(), ItemFactory::getItem(DOT));
}

void PacmanTest::test_end_to_end(const float& speed)
{
    tm->setNewMap(2, 1, "11");
    pacman->setPosition(Vector2f(.5, .5));
    pacman->changeDirection(LEFT);
    pacman->setSpeed(speed);
    const float s = (speed < .5) ? speed : 0;
    const float r = (speed < .5) ? 0 : -.1;
    EXPECT_FLOAT_EQ(pacman->getPosition().x, .5);
    EXPECT_FLOAT_EQ(pacman->getPosition().y, .5);
    while (pacman->getPosition().x >= 0 && pacman->getPosition().x <= 1.5 + r)
        pacman->move();
    ASSERT_FLOAT_EQ(pacman->getPosition().x, 1.5 + s);
    ASSERT_FLOAT_EQ(pacman->getPosition().y, .5);
    pacman->changeDirection(RIGHT);
    const float round = .001;

    while (pacman->getPosition().x > (.5 - s + round))
        pacman->move();

    ASSERT_FLOAT_EQ(pacman->getPosition().x, 0.5 - s);
    ASSERT_FLOAT_EQ(pacman->getPosition().y, .5);
    pacman->changeDirection(LEFT);
    while (pacman->getPosition().x >= 0 && pacman->getPosition().x <= 1.5 + r)
        pacman->move();

    ASSERT_FLOAT_EQ(pacman->getPosition().x, 1.5 + s);
    ASSERT_FLOAT_EQ(pacman->getPosition().y, .5);
}

TEST_F(PacmanTest, move_end_to_end_on_map_test)
{
    test_end_to_end(ConfModel::PACMAN::MIN_SPEED);
    test_end_to_end(ConfModel::PACMAN::DEFAULT_SPEED);
    test_end_to_end(ConfModel::PACMAN::SPEED_SUP);
    test_end_to_end(ConfModel::PACMAN::MAX_SPEED);
}

} // namespace pm
