#include <gtest/gtest.h>

#include "pacman/util/Event.hpp"
#include "pacman/util/Subject.hpp"
#include "pacman/util/Observer.hpp"

class Obs: public pm::Observer
{
public:
    Obs(bool s=false): state(s)
    {}

    virtual void actualisate(const pm::Event & event) override
    {
        state = (event == pm::EAT_ITEM);
    }

    bool getState() const
    {
        return state;
    }

private:
    bool state;
};

class Subj: public pm::Subject
{};

class ObserverTest: public ::testing::Test
{
protected:
    Subj subj;
    Obs obs1;
    Obs obs2;

    virtual void SetUp()
    {
        subj.addObserver(obs1);
    }

    virtual void TearDown()
    {
    }
};

TEST_F(ObserverTest, observer_pattern_basic_notify)
{
    EXPECT_FALSE(obs1.getState());
    
    subj.notify(pm::EATEN);
    ASSERT_FALSE(obs1.getState());
    subj.notify(pm::EAT_ITEM);
    ASSERT_TRUE(obs1.getState());
}

TEST_F(ObserverTest, observer_pattern_multiple_notify)
{
    EXPECT_FALSE(obs1.getState());
    EXPECT_FALSE(obs2.getState());

    subj.addObserver(obs2);
    subj.notify(pm::EAT_ITEM);

    ASSERT_TRUE(obs1.getState());
    ASSERT_TRUE(obs2.getState());
}

TEST_F(ObserverTest, observer_pattern_delete_observer)
{
    EXPECT_FALSE(obs1.getState());
    EXPECT_FALSE(obs2.getState());

    subj.addObserver(obs2);
    subj.notify(pm::EAT_ITEM);

    ASSERT_TRUE(obs1.getState());
    ASSERT_TRUE(obs2.getState());

    subj.delObserver(obs2);
    subj.notify(pm::EATEN);

    ASSERT_FALSE(obs1.getState());
    ASSERT_TRUE(obs2.getState());
}
