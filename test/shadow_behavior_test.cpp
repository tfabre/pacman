#include <gtest/gtest.h>
#include <memory>
#include <stack>

#include "pacman/model/character/enemy/behavior/Shadow.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"020000000000002001000__00010020000001111112111011111101112111111000000200100" \
"0000001002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000011111111111100111111111" \
"1110020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")
#define SPEED 0.1

class ShadowBehaviorTest: public ::testing::Test
{
protected:
    pm::TileMap* tm;
    pm::Pacman* pacman;
    pm::PathFinding* pf;
    pm::EnemyBehavior* eb;

    void SetUp()
    {
        pm::Vector2f pos(14.5, 29.5);
        tm = new pm::TileMap(X, Y, MAP, nullptr);
        pacman = new pm::Pacman(pos, 0.1, tm, pos, pm::LEFT);
        pf = new pm::PathFinding( tm );
        eb = new pm::Shadow( pm::Vector2f(1.5,1.5), SPEED, 0, tm, pacman, pf );
    }

    void TearDown()
    {
        delete pacman;
        delete tm;
        delete pf;
        delete eb;
    }
};

TEST_F(ShadowBehaviorTest, shadow_test )
{
    pm::Vector2f currentPos;
    do
    {
        currentPos = eb->getPosition();
        eb->move();
        switch( eb->getDirection() )
        {
        case pm::Direction::TOP:
            ASSERT_FLOAT_EQ( eb->getPosition().x, currentPos.x );
            ASSERT_FLOAT_EQ( eb->getPosition().y, (currentPos.y - SPEED) );
            break;
        case pm::Direction::LEFT:
            ASSERT_FLOAT_EQ( eb->getPosition().x, (currentPos.x - SPEED) );
            ASSERT_FLOAT_EQ( eb->getPosition().y, currentPos.y );
            break;
        case pm::Direction::BOTTOM:
            ASSERT_FLOAT_EQ( eb->getPosition().x, currentPos.x );
            ASSERT_FLOAT_EQ( eb->getPosition().y, (currentPos.y + SPEED) );
            break;
        case pm::Direction::RIGHT:
            ASSERT_FLOAT_EQ( eb->getPosition().x, (currentPos.x + SPEED) );
            ASSERT_FLOAT_EQ( eb->getPosition().y, currentPos.y );
            break;
        case pm::Direction::NO_DIRECTION:
            break;
        }
    } while( !eb->getPath()->empty() );

    ASSERT_TRUE( eb->getPath()->empty() );
    eb->move();
    // The path is empty after move() call because the ghost is already on the same square that Pacman
    ASSERT_TRUE( eb->getPath()->empty() );
}
