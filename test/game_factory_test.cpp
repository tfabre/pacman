#include <gtest/gtest.h>

#include "pacman/model/GameFactory.hpp"

class GameFactoryTest: public ::testing::Test
{
protected:
    void SetUp()
    {}

    void TearDown()
    {
        pm::GameFactory::deleteGameModel();
    }
};

TEST_F(GameFactoryTest, get_tile_map_test)
{
    pm::TileMap* tilemap = pm::GameFactory::getTileMap();
    ASSERT_EQ(tilemap->getXLimit(), 28);
    ASSERT_EQ(tilemap->getYLimit(), 31);
    ASSERT_EQ(tilemap->getTile(pm::Vector2f(0.f, 0.f)), pm::WALL);
    ASSERT_EQ(tilemap->getTile(pm::Vector2f(1.f, 1.f)), pm::GROUND);
    ASSERT_EQ(tilemap->getTile(pm::Vector2f(13.f, 12.f)), pm::GHOST_DOOR);
    ASSERT_EQ(tilemap->getItem(pm::Vector2f(1.f, 1.f)),
              pm::ItemFactory::getItem(pm::NONE));
    ASSERT_EQ(tilemap->getItem(pm::Vector2f(1.5, 1.5)),
              pm::ItemFactory::getItem(pm::DOT));
    ASSERT_EQ(tilemap->getItem(pm::Vector2f(1.5, 3.5)),
              pm::ItemFactory::getItem(pm::POWER_PELLET));
}

TEST_F(GameFactoryTest, get_pacman_test)
{
    pm::Pacman* pacman = pm::GameFactory::getPacman();
    ASSERT_EQ(pacman->getTileMap(), pm::GameFactory::getTileMap());
}
