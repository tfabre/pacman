#include <gtest/gtest.h>
#include <memory>
#include <stack>

#include "pacman/model/character/enemy/behavior/BackHome.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"020000000000002001000__00010020000001111112111011-11101112111111000000200100" \
"0000001002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000011111111111100111111111" \
"1110020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")
#define SPEED 0.1

class BackHomeBehaviorTest: public ::testing::Test
{
protected:
    pm::TileMap* tm;
    pm::Pacman* pacman;
    pm::PathFinding* pf;
    pm::EnemyBehavior* eb;

    void SetUp()
    {
        pm::Vector2f pos(6.5, 22.5);
        tm = new pm::TileMap(X, Y, MAP, nullptr);
        pacman = new pm::Pacman(pos, 0.1, tm, pos, pm::TOP);
        pf = new pm::PathFinding( tm );
        eb = new pm::BackHome( pm::Vector2f(26.5,29.5), SPEED, 0, tm, pacman, pf );
    }

    void TearDown()
    {
        delete pacman;
        delete tm;
        delete pf;
        delete eb;
    }
};

TEST_F(BackHomeBehaviorTest, backHome_pacman_look_on_top_test )
{
    pm::Vector2f currentPos;
    do
    {
        currentPos = eb->getPosition();
        eb->move();
        switch( eb->getDirection() )
        {
        case pm::Direction::TOP:
            ASSERT_FLOAT_EQ( eb->getPosition().x, currentPos.x );
            ASSERT_FLOAT_EQ( eb->getPosition().y, (currentPos.y - SPEED) );
            break;
        case pm::Direction::LEFT:
            ASSERT_FLOAT_EQ( eb->getPosition().x, (currentPos.x - SPEED) );
            ASSERT_FLOAT_EQ( eb->getPosition().y, currentPos.y );
            break;
        case pm::Direction::BOTTOM:
            ASSERT_FLOAT_EQ( eb->getPosition().x, currentPos.x );
            ASSERT_FLOAT_EQ( eb->getPosition().y, (currentPos.y + SPEED) );
            break;
        case pm::Direction::RIGHT:
            ASSERT_FLOAT_EQ( eb->getPosition().x, (currentPos.x + SPEED) );
            ASSERT_FLOAT_EQ( eb->getPosition().y, currentPos.y );
            break;
        case pm::Direction::NO_DIRECTION:
            break;
        }
        if( eb->getPosition().x == 13.5 && eb->getPosition().y == 14.5 )
            break;
    } while( !eb->getPath()->empty() );

    ASSERT_FLOAT_EQ( eb->getPosition().x, 13.5 );
    ASSERT_FLOAT_EQ( eb->getPosition().y, 14.5 );
}
