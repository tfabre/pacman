#include <gtest/gtest.h>

#include "pacman/model/GameState.hpp"

class GameStateTest: public ::testing::Test
{
protected:
    pm::TileMap* map;
    pm::Pacman* pacman;
    pm::GameState* state;

    void SetUp()
    {
        pm::Vector2f pos(0.f, 0.5);
        map = new pm::TileMap(2, 1, std::string("22"), nullptr);
        pacman = new pm::Pacman(pos, 0.5, map, pos, pm::RIGHT);
        pacman->setSize(0.5);
        state = new pm::GameState(pacman, nullptr);
    }

    void TearDown()
    {
        delete state;
        delete pacman;
        delete map;
    }
};

TEST_F(GameStateTest, score_increment_test)
{
    EXPECT_EQ(state->getScore(), 0);
    pacman->move();
    ASSERT_EQ(state->getScore(), pm::ItemFactory::getItem(pm::DOT).getScore());
}

TEST_F(GameStateTest, life_increment_test)
{
    pm::uint32 keyScore = static_cast<pm::uint32>(
        pm::ItemFactory::getItem(pm::KEY).getScore());
    map->setNewMap(2, 1, std::string("KK"));
    EXPECT_EQ(state->getScore(), 0);
    EXPECT_EQ(state->getLifeNumber(), 2);
    pacman->move();
    ASSERT_EQ(state->getScore(), keyScore);
    pacman->move();
    pacman->move();
    ASSERT_EQ(state->getScore(), keyScore * 2);
    ASSERT_EQ(state->getLifeNumber(), 3);
}
