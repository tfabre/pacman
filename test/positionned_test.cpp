#include <gtest/gtest.h>
#include <memory>

#include "pacman/model/character/Positionned.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"020000000000002001000__00010020000001111112111011111101112111111000000200100" \
"0000001002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000011111111111100111111111" \
"1110020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")

class Character: public pm::Positionned
{
public:
    using pm::Positionned::Positionned;
};

class PositionnedTest: public ::testing::Test
{
protected:
    Character* character;
    pm::TileMap* tm;
    void SetUp()
    {
        tm = new pm::TileMap(X, Y, MAP, nullptr);
        character = new Character(pm::Vector2f(2.f, 2.f), 2.5, 0.25, tm);
    }

    void TearDown()
    {
        delete tm;
        delete character;
    }
};

TEST_F(PositionnedTest, getter_test)
{
    ASSERT_FLOAT_EQ(character->getPosition().x, 2.f);
    ASSERT_FLOAT_EQ(character->getPosition().y, 2.f);
    ASSERT_FLOAT_EQ(character->getSpeed(), 2.5);
    ASSERT_EQ(character->getTileMap(), tm);
}

TEST_F(PositionnedTest, setter_test)
{
    character->setPosition(pm::Vector2f(3.3, 3.3));
    ASSERT_FLOAT_EQ(character->getPosition().x, 3.3);
    ASSERT_FLOAT_EQ(character->getPosition().y, 3.3);

    character->setSpeed(2.1);
    ASSERT_FLOAT_EQ(character->getSpeed(), 2.1);

    delete tm;
    tm = new pm::TileMap(X, Y, MAP, nullptr);
    character->setTileMap(tm);
    ASSERT_EQ(character->getTileMap(), tm);
}

TEST_F(PositionnedTest, collision_detection_test)
{
    std::shared_ptr<Character> c1(
        new Character(pm::Vector2f(2.f, 2.f), 2.5, 0.5, tm));
    std::shared_ptr<Character> c2(
        new Character(pm::Vector2f(3.f, 2.f), 2.5, 0.5, tm));

    ASSERT_TRUE(pm::Positionned::detectCollision(c1.get(), c2.get()));
    c2.reset(new Character(pm::Vector2f(3.1, 2.f), 2.5, 0.5, tm));
    ASSERT_FALSE(pm::Positionned::detectCollision(c1.get(), c2.get()));
}
