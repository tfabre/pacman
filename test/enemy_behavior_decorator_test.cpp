#include <gtest/gtest.h>

#include "pacman/model/character/enemy/behavior/decorator/EnemyBehaviorDecorator.hpp"

namespace
{

class BehaviorImpl: public pm::EnemyBehavior
{
public:
    using pm::EnemyBehavior::EnemyBehavior;

    void move()
    {
        pm::Vector2f pos = this->getPosition();
        pos.x += 1;
        this->setPosition(pos);
    }
};

class BehaviorDecoratorImpl: public pm::EnemyBehaviorDecorator
{
public:
    using pm::EnemyBehaviorDecorator::EnemyBehaviorDecorator;

    void move()
    {
        this->getBehavior()->move();
        pm::Vector2f position = this->getBehavior()->getPosition();
        position.y += 1;
        this->setPosition(position);
        this->getBehavior()->setPosition(position);
    }
};

}

class EnemyBehaviorDecoratorTest: public ::testing::Test
{
protected:
    pm::EnemyBehavior* behavior;

    void SetUp()
    {
        pm::Vector2f pos(0.f, 0.f);
        behavior = new BehaviorDecoratorImpl(
            new BehaviorImpl(pos, 0.1, 1.f, nullptr, nullptr, nullptr));
    }

    void TearDown()
    {
        delete behavior;
    }
};

TEST_F(EnemyBehaviorDecoratorTest, move_decoration_test)
{
    behavior->move();
    ASSERT_FLOAT_EQ(behavior->getPosition().x, 1);
    ASSERT_FLOAT_EQ(behavior->getPosition().y, 1);
}
