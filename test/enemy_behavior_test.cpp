#include <gtest/gtest.h>

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"020000000000002001000__00010020000001111112111011111101112111111000000200100" \
"0000001002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000011111111111100111111111" \
"1110020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")

class BehaviorImpl: public pm::EnemyBehavior
{
public:
    using pm::EnemyBehavior::EnemyBehavior;

    void move()
    {
        pm::Vector2f pos = this->getPosition();
        pos.x += 1;
        this->setPosition(pos);
    }
};

class EnemyBehaviorTest: public ::testing::Test
{
protected:
    BehaviorImpl* behavior;
    pm::TileMap* tm;
    pm::Pacman* pacman;
    pm::PathFinding* pf;
    
    void SetUp()
    {
        pm::Vector2f pos(0.f, 0.f);
        tm = new pm::TileMap(X, Y, MAP, nullptr);
        pacman = new pm::Pacman(pos, 0.1, tm, pos, pm::LEFT);
        behavior = new BehaviorImpl(pos, 0.1, 1.f, tm, pacman, pf);
        pf = new pm::PathFinding( tm );
    }

    void TearDown()
    {
        delete behavior;
        delete pacman;
        delete tm;
        delete pf;
    }
};

TEST_F(EnemyBehaviorTest, move_test)
{
    behavior->move();
    ASSERT_FLOAT_EQ(behavior->getPosition().x, 1);
    ASSERT_FLOAT_EQ(behavior->getPosition().y, 0);
}
