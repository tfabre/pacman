#include <gtest/gtest.h>

#include "pacman/model/map/Level.hpp"

#define X 28
#define Y 31
#define MAP std::string("0000000000000000000000000000022222222222200222222222" \
"2220020000200000200200000200002003000020000020020000020000300200002000002002" \
"0000020000200222222222222222222222222220020000200200000000200200002002000020" \
"0200000000200200002002222220022220022220022222200000002000001001000002000000" \
"000000200000100100000200000000000020011111111110020000000000002001000__00010" \
"0200000000000020010111111010020000001111112111011111101112111111000000200101" \
"1111101002000000000000200100000000100200000000000020011111111110020000000000" \
"0020010000000010020000000000002001000000001002000000022222222222200222222222" \
"2220020000200000200200000200002002000020000020020000020000200322002222222112" \
"2222220022300002002002000000002002002000000200200200000000200200200002222220" \
"0222200222200222222002000000000020020000000000200200000000002002000000000020" \
"02222222222222222222222222200000000000000000000000000000")

namespace pm
{

class LevelTest: public ::testing::Test
{
protected:
    TileMap* tilemap;
    Level* level;

    void SetUp()
    {
        tilemap = new TileMap();
        level = new Level("asset/map/level.pm", tilemap);
    }

    void TearDown()
    {
        delete tilemap;
        delete level;
    }
};

TEST_F(LevelTest, set_next_tile_map_level_test)
{
    EXPECT_EQ(tilemap->getXLimit(), 0);
    EXPECT_EQ(tilemap->getYLimit(), 0);
    level->setNextTileMapLevel();

    ASSERT_EQ(tilemap->getXLimit(), X);
    ASSERT_EQ(tilemap->getYLimit(), Y);
    ASSERT_EQ(tilemap->getTile(Vector2f(0.f, 0.f)), WALL);
    ASSERT_EQ(tilemap->getTile(Vector2f(1.f, 1.f)), GROUND);
    ASSERT_EQ(tilemap->getTile(Vector2f(13.f, 12.f)), GHOST_DOOR);
    ASSERT_EQ(tilemap->getItem(Vector2f(1.f, 1.f)), ItemFactory::getItem(NONE));
    ASSERT_EQ(tilemap->getItem(Vector2f(1.5, 1.5)), ItemFactory::getItem(DOT));
    ASSERT_EQ(tilemap->getItem(Vector2f(1.5, 3.5)),
              ItemFactory::getItem(POWER_PELLET));

    level->setNextTileMapLevel();
    ASSERT_EQ(tilemap->getXLimit(), X);
    ASSERT_EQ(tilemap->getYLimit(), Y);
    ASSERT_EQ(tilemap->getTile(Vector2f(0.f, 0.f)), WALL);
    ASSERT_EQ(tilemap->getTile(Vector2f(1.f, 1.f)), GROUND);
    ASSERT_EQ(tilemap->getTile(Vector2f(13.f, 12.f)), GHOST_DOOR);
    ASSERT_EQ(tilemap->getItem(Vector2f(1.f, 1.f)), ItemFactory::getItem(NONE));
    ASSERT_EQ(tilemap->getItem(Vector2f(1.5, 1.5)), ItemFactory::getItem(DOT));
    ASSERT_EQ(tilemap->getItem(Vector2f(1.5, 3.5)),
              ItemFactory::getItem(POWER_PELLET));
}

TEST_F(LevelTest, set_next_level_on_eol_event_test)
{
    EXPECT_EQ(tilemap->getXLimit(), 0);
    EXPECT_EQ(tilemap->getYLimit(), 0);
    level->actualisate(END_OF_LEVEL);

    ASSERT_EQ(tilemap->getXLimit(), X);
    ASSERT_EQ(tilemap->getYLimit(), Y);
    ASSERT_EQ(tilemap->getTile(Vector2f(0.f, 0.f)), WALL);
    ASSERT_EQ(tilemap->getTile(Vector2f(1.f, 1.f)), GROUND);
    ASSERT_EQ(tilemap->getTile(Vector2f(13.f, 12.f)), GHOST_DOOR);
    ASSERT_EQ(tilemap->getItem(Vector2f(1.f, 1.f)), ItemFactory::getItem(NONE));
    ASSERT_EQ(tilemap->getItem(Vector2f(1.5, 1.5)), ItemFactory::getItem(DOT));
    ASSERT_EQ(tilemap->getItem(Vector2f(1.5, 3.5)),
              ItemFactory::getItem(POWER_PELLET));
}

} // namespace pm
