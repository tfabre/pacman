#ifndef PACMAN_TIMER_HPP
#define PACMAN_TIMER_HPP

#include <chrono>
#include "pacman/util/types.hpp"

namespace pm
{

/*!
** \brief Timer class for time measure and eventual pause during measure
*/
class Timer
{
public:
    /*!
    ** \brief Default constructor
    */
    Timer();

    /*!
    ** \brief Default destructor
    */
    ~Timer();

    /*!
    ** \brief Launch the timer
    */
    void start();

    /*!
    ** \brief Pause timer
    */
    void pause();

    /*!
    ** \brief Resume timer from a pause
    */
    void resume();

    /*!
    ** \brief Return elapsed time since start()
    **
    ** \return Elasped time since start()
    */
    fsec getElapsedTime();

    /*!
    ** \brief Indicate if timer is paused or not
    **
    ** \return true if timer is paused false otherwise
    */
    bool isInPause() const;

    /*!
    ** \brief Return the time stamp of the actual date
    **
    ** \return Actual date time stamp
    */
    static TimeStamp getActualTimeStamp();

    /*!
    ** \brief Convert actual date to string
    **
    ** \param Date format to follow for convertion
    ** \return Date to string
    */
    static std::string actualTimeToString(const std::string&
                                          format="%d/%m/%y %T");

    /*!
    ** \brief Convert a time point in string depending on given format
    **
    ** \param tp Time point to convert
    ** \param format Format to follow for convertion
    ** \return TimePoint in string format
    */
    static std::string timePointToString(const TimePoint& tp,
                                         const std::string& format);

private:
    TimePoint begin;    ///< The moment when timer was launched
    TimePoint lapBegin; ///< The moment when pause was called
    bool      isPaused; ///< Indicate if the timer is paused
};

}

#endif // PACMAN_TIMER_HPP
