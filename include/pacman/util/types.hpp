#ifndef PACMAN_TYPES_HPP
#define PACMAN_TYPES_HPP

#include <chrono>
#include <cstdint>
#include <tuple>

#include "pacman/util/Vector2.hpp"

namespace pm
{

/*!
** \defgroup IntegerTypedef
** \brief Basic integer typedef
** @{
*/
typedef int8_t   int8;  ///< 8 bit  signed integer value
typedef int16_t  int16; ///< 16 bit signed integer value
typedef int32_t  int32; ///< 32 bit signed integer value
typedef int64_t  int64; ///< 64 bit signed integer value

typedef uint8_t  uint8;  ///< 8 bit  unsigned integer value
typedef uint16_t uint16; ///< 16 bit unsigned integer value
typedef uint32_t uint32; ///< 32 bit unsigned integer value
typedef uint64_t uint64; ///< 64 bit unsigned integer value
/*! @} */

/*!
** \defgroup ChronoTypedef
** \brief Chrono typedef
** @{
*/
typedef std::chrono::high_resolution_clock Clock; ///< Clock type
typedef Clock::duration::rep TimeStamp;           ///< Time stamp type
typedef std::chrono::time_point<Clock> TimePoint; ///< Time values type
typedef std::chrono::duration<float> fsec;   ///< Second floating duration type 
typedef Clock::duration ClockDuration; ///< Default duration unit for Clock type
/*! @} */

/*!
** \defgroup UtilTypedef
** \brief Util typedef
** @{
*/
typedef std::tuple<Vector2<uint32>, Vector2<uint32>> UIntVectorPair;
///< Pair of vector
typedef std::tuple<uint64, TimePoint> ScoreLine;
///< Score line type for GameState
/*! @} */

} // namespace pm

#endif // PACMAN_TYPES_HPP
