#ifndef PACMAN_RANDOM_HPP
#define PACMAN_RANDOM_HPP

#include <random>

#include "pacman/util/types.hpp"

namespace pm
{

struct Random
{
    Random()
    {
    }

    float getRandom( float min = 0., float max = 1. )
    {
        std::random_device rd;
        return ((int)(rd()/10000+(min*100)) % (int)(max*100)) / 100.;
    }
};

}

#endif // PACMAN_RANDOM_HPP
