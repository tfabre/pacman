#ifndef PACMAN_VECTOR2_HPP
#define PACMAN_VECTOR2_HPP

namespace pm
{

/*!
** \brief Represent a 2D vector
*/
template<typename T>
struct Vector2
{
    /// \brief Default constructor
    Vector2(T x=0, T y=0): x(x), y(y)
    {}

    T x; ///< x vector part
    T y; ///< y vector part
};

typedef Vector2<float> Vector2f; ///< Represent float vector

}
#endif //  PACMAN_VECTOR2_HPP
