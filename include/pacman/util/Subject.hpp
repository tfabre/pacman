#ifndef PACMAN_SUBJECT_HPP
#define PACMAN_SUBJECT_HPP

#include <vector>
#include "pacman/util/Observer.hpp"
#include "pacman/util/Event.hpp"

namespace pm
{

/*!
** \brief Represents a Subject that could be observed by an Observer
** 
** Implementation of Observer design pattern
*/
class Subject
{
public:
    /*!
    ** \brief Default asbstract virtual destructor
    */
    virtual ~Subject() = 0;

    /*!
    ** \brief Add Observer to the observers list
    **
    ** \param obs The observer to add
    */
    virtual void addObserver(Observer & obs) final;

    /*!
    ** \brief Take off the observer given in parameter from the observers list
    **
    ** \param obs The observer to take off
    */
    virtual void delObserver(Observer & obs) final;

    /*!
    ** \brief Send the event given in parameter to each observers
    **
    ** \param event The event type to send
    */
    virtual void notify(const Event & event) final;

private:
    std::vector<Observer*> observers; ///< Observers list
};

}

#endif // PACMAN_SUBJECT_HPP
