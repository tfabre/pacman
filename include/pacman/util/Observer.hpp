#ifndef PACMAN_OBSERVER_HPP
#define PACMAN_OBSERVER_HPP

#include "pacman/util/Event.hpp"

namespace pm
{

/*!
** \brief Represent a class that observe a Subject
** 
** Implementation of Observer design pattern
*/
class Observer
{
public:
    /*!
    ** \brief Default destructor
    */
    virtual ~Observer() = 0;

    /*!
    ** \brief Actualisate the object depending on the event given in parameter
    **
    ** \param event The event type
    */
    virtual void actualisate(const Event & event) = 0;
};

}

#endif // PACMAN_OBSERVER_HPP
