#ifndef PACMAN_EVENT_HPP
#define PACMAN_EVENT_HPP

namespace pm
{

/*!
** \brief Describe a pacman event type
** \enum Event
*/
enum Event
{
    EAT_ITEM,         ///< Pacman has eat an item
    EAT_POWER_PELLET, ///< Pacman has eat a Power Pellet
    EAT_GHOST,        ///< Pacman has eat a Ghost
    EATEN,            ///< Pacman is eaten
    END_OF_LEVEL,     ///< Level is finished
    FRUIT_APPEAR,     ///< When fruit appear on the maze
    FRUIT_EATEN,      ///< When fruit is eaten
    FRUIT_DISAPPEAR   ///< When fruit disappear
};

}

#endif // PACMAN_EVENT_HPP
