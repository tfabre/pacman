#ifndef PACMAN_MAP_VIEW_HPP
#define PACMAN_MAP_VIEW_HPP

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include "pacman/model/map/TileMap.hpp"
#include "pacman/model/GameFactory.hpp"
#include "pacman/util/types.hpp"

namespace pm
{

enum TileSetType
{
    TOP_LEFT_CORNER_TILE = 0,
    TOP_RIGHT_CORNER_TILE = 1,
    BOT_RIGHT_CORNER_TILE = 2,
    BOT_LEFT_CORNER_TILE = 3,
    VERTICAL_WALL_TILE = 4,
    HORIZONTAL_WALL_TILE = 5,
    RIGHT_ENDED_WALL_TILE = 6,
    TOP_ENDED_WALL_TILE = 7,
    BOT_ENDED_WALL_TILE = 8,
    LEFT_ENDED_WALL_TILE = 9,
    T_CORNER_TOP_TILE = 10,
    T_CORNER_BOT_TILE = 11,
    T_CORNER_LEFT_TILE = 12,
    T_CORNER_RIGHT_TILE = 13,
    NONE_TILE = 14,
    DOT_TILE = 15,
    POWER_PILLET_TILE = 16,
    GHOST_DOOR_TILE = 17,
    HALF_CHERRY = 18,
    HALF_STRAWBERRY = 21,
    HALF_ORANGE = 23,
    HALF_APPLE = 25,
    HALF_MELON = 28,
    HALF_GALBOSS = 30,
    HALF_BELL = 32,
    HALF_KEY = 35,
    WHITE_TILE = 20
};

/*!
** \brief Display the maze to the screen
*/
class MapView: public sf::Drawable
{
public:
    /*!
    ** \brief Default constructor
    */
    MapView(TileMap* const tm = GameFactory::getTileMap());

    /*!
    ** \brief Default destructor
    */
    ~MapView();

    /*!
    ** \brief Load map from file
    */
    void loadMap();

    /*!
    ** \brief Do tranformation before map draw on screen
    **
    ** Check if fruit are visible or not. It allows to send event if the time
    ** is over.
    */
    void drawMap();

    /*!
    ** \brief Draw fruit where they should be drawn
    */
    void drawFruit();

    /*!
    ** \brief Delete fruit from the view
    */
    void removeFruit();

    /*!
    ** \brief Remove item representation that is at the given coordinate
    */
    void removeItem(const Vector2f& pos);

    /*!
    ** \brief Call pauseFruitAppearing() if pause is true and
    **        resumerFruitAppearing() otherwise
    **
    ** \param pause Pause or resume
    */
    void setPause(const bool& pause);

    /*!
    ** \brief display score where the fruit were
    */
    void displayScore();

private:
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

    /*!
    ** \brief Fill surround given in parameter array with tile type that
    **        surround the given position
    **
    ** \param pos Position used to compute surround
    ** \param surround Array to fill
    */
    void getSurrounding(const Vector2f& pos, TileType surround[]) const;

    /*!
    ** \brief Fill transformedMap with the tile set type depending on them
    **        position
    */
    void convertModelMapToDisplayMap();

    /*!
    ** \brief Fill tranformedMap with a wall type depending on the given
    **        coordinate
    **
    ** \param x X coordinate
    ** \param y Y coordinate
    ** \param s The 8 tile surrounding the tile to the given coordinate
    */
    void setWallType(const uint32& x, const uint32& y, const TileType s[]);

    /*!
    ** \brief Fill transformedMap with a fruit type depending on the given
    **        coordinate
    **
    ** \param x X coordinate
    ** \param y Y coordinate
    ** \param s The 8 tile surrounding the tile to the given coordinate
    ** \note the x paramater is not const because it's modified if a fruit tile
    **       is found
    */
    void setItemType(uint32& x, const uint32& y, const Item& actualItem);

    /*!
    ** \brief Fill texture to the quad given in parameter
    **
    ** \param quad Quad to fill
    ** \param tu Texture x origin
    ** \param tv Texture y origin
    */
    void fillTexture(sf::Vertex* const quad, const int32& tu, const int32& tv);

    /*!
    ** \brief Fill color to the quad given in parameter
    **
    ** \param quad Quad to fille
    ** \param color Color used to fill the quad
    */
    void fillColor(sf::Vertex* const quad, const sf::Color& color);

    /*!
    ** \brief Return tile set fruit corresponding to the fruit in the current
    **        level
    **
    ** \return Tile set corresponding to the fruit in actual level
    */
    TileSetType getFruitTileSet() const;

    sf::VertexArray vertices;            ///< Vertices
    sf::Texture tileset;                 ///< Texture
    const sf::Vector2u tileSize{34, 34}; ///< Tile on texture size

    uint32** transformedMap;             ///< Map with tile set

    TileMap* map;                        ///< Map
    std::vector<UIntVectorPair> fruitCoordinates; ///< Fruit coordinates
    sf::Vector2<uint32> mapLimit;

    sf::Font font;        ///< Font for score display
    sf::Text score;       ///< Score text
    sf::Clock scoreClock; ///< Clock for score display
};

}

#endif // PACMAN_MAP_VIEW_HPP
