#ifndef PACMAN_MAP_EDITOR_HPP
#define PACMAN_MAP_EDITOR_HPP

#include "pacman/view/View.hpp"
#include "pacman/view/Game.hpp"
#include "pacman/view/MapView.hpp"

namespace pm
{

class MapEditor: public View
{
public:
    MapEditor(Game* const parent);
    
    ~MapEditor();

    virtual void drawView();

    TileMap *tilemap;

    void actualisateMarkerPos(const uint8 x, const uint8 y);

    Game* getParent() const;

private:
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

    Game* parent;     ///< Parent
    sf::Font pacfont;        ///< Font ues for texts

    MapView *map;
    
    sf::RectangleShape marker; ///< a circle to see where the next tile will be placed

    sf::Texture texture;    ///< texture for sprite buttons

    sf::Sprite addWallSprite;   ///< sprite for addWall button
    sf::Sprite addNoneSprite;   ///< sprite for addNone button
    sf::Sprite addDotSprite;    ///< sprite for addDot button
    sf::Sprite addPowerPelletSprite;    ///< sprite for addPowerPellet button
    sf::Sprite addGhostDoorSprite;  ///< sprite for ghost door button

    sf::CircleShape addPacmanPosShape;  ///< shape to place pacman button
    sf::RectangleShape addFruitPosShape;    ///< shape to place fruit button
    sf::RectangleShape saveTileSprite;  ///< shape for save level button
};


}


#endif // PACMAN_MAP_EDITOR_HPP
