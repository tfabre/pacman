#ifndef PACMAN_SCORE_VIEW_HPP
#define PACMAN_SCORE_VIEW_HPP

#include "pacman/view/View.hpp"
#include "pacman/view/Game.hpp"

namespace pm
{

/*!
** \brief Score screen
*/
class ScoreView: public View
{
public:
    /*!
    ** \brief Default constructor
    **
    ** \param parent Parent interface
    */
    ScoreView(Game* const parent);

    virtual void drawView() override;

    /*!
    ** \brief Return parent class
    **
    ** \return Parent interface
    */
    Game* getParent() const;

    /*!
    ** \brief Update score text with de most recent score
    */
    void updateScoreText();

    /*!
    ** \brief Fetch scores from GameState and cast them in string
    **
    ** \return String of scores
    */
    std::string getScoreText() const;

private:
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

    Game* parent;
    sf::Font font;
    sf::Text titleText;
    sf::Text scoreText;
};

} // namespace pm

#endif // PACMAN_SCORE_VIEW_HPP
