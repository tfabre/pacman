#ifndef PACMAN_VIEW_HPP
#define PACMAN_VIEW_HPP

#include <vector>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "pacman/controller/Controller.hpp"

namespace pm
{

class Controller;

/*!
** \brief Abstract base class for all graphical classes
*/
class View: public sf::Drawable
{
public:
    /*!
    ** \brief Default constructor
    */
    View();

    /*!
    ** \brief Default virtual constructor
    */
    virtual ~View() = 0;

    /*!
    ** \brief Operation to do before drawing the view element
    */
    virtual void drawView() = 0;

    /*!
    ** \brief Add controller to the view
    **
    ** \param ctrl Controller to add
    */
    virtual void addController(Controller* const ctrl) final;

    /*!
    ** \brief Call controllers to handle event on view
    **
    ** \param event Event type
    */
    virtual void checkEvent(const sf::Event & event) final;

    /*!
    ** \brief Call controllers to check for extra event not based on SFML
    **        sf::Event
    */
    virtual void checkExtraEvent() final;

    /*!
    ** \brief Return the actual window that contains view
    **
    ** \return the window object
    */
    static sf::RenderWindow& getWindow();

private:
    std::vector<Controller*> controllers; ///< The controller list
    static sf::RenderWindow window;       ///< The window that contains views
};

} // namespace pm

#endif // PACMAN_VIEW_HPP
