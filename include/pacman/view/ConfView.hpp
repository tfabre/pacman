#ifndef PACMAN_CONF_VIEW_HPP
#define PACMAN_CONF_VIEW_HPP

#include <string>
#include "pacman/util/types.hpp"

namespace pm
{

/*!
** \brief Contains values for view configuration (global variables, etc)
*/
class ConfView
{
public:
    /*!
    ** \brief represent paths to assets
    */
    static class PATH
    {
    public:
        static const std::string ASSET;      ///< Asset folder
        static const std::string ASSET_FONT; ///< Font folder
        static const std::string ASSET_TEXT; ///< Texture folder
        static const std::string ASSET_MUSIC; ///< Music folder
    } path;

    /*!
    ** \brief Represent resource file
    */
    static class FILE
    {
    public:
        static const std::string PACMAN_FONT;     ///< Path to the font file
        static const std::string PACMAN_FONT_NUM;
        ///< Path to the numeric font file
        static const std::string MAP_TEXTURE;     ///< Path to map texture file
        static const std::string PACMAN_TEXTURE;
        ///< Path to pacman texture file
        static const std::string GHOST_TEXTURE;
        ///< Path to shadow texture file
        static const std::string LIFE_TEXTURE; ///< Path to life texture file
        static const std::string FRUITS_TEXTURE; ///< Path to fruits texture file

        static const std::string MENU_MUSIC;        ///< Path to menu music file
        static const std::string PLAY_MUSIC;        ///< Path to play music file
        static const std::string EAT_DOT_SOUND;     ///< Path to eat_dot_sound file
        static const std::string EAT_ITEM_SOUND;    ///< Path to eat_item_sound file
        static const std::string DEATH_SOUND;       ///< Path to death sound file
    } file;

    /*!
    ** \brief Represent UI texts
    */
    static class TEXT
    {
    public:
        static const std::string TITLE;      ///< Title text
        static const uint32 TITLE_SIZE{50};  ///< Title text size
        static const std::string PLAY;       ///< Play menu text
        static const std::string SCORE;      ///< Score menu text
        static const std::string MAP_EDITOR; ///< Map editor menu text
        static const std::string OPTION;     ///< Option menu text
        static const uint32 MENU_SIZE{30};   ///< Menu text size
    } text;

    /*!
    ** \brief Represent Window properties
    */
    static class WINDOW
    {
    public:
        static const uint32 FRAMERATE_LIMITE{30}; ///< Framerate limit
        static const uint32 DEFAULT_WIDTH{560};   ///< Default window's width
        static const uint32 DEFAULT_HEIGHT{720};  ///< Default window's height
        static const std::string TITLE;           ///< Window's title
    } window;

    /*!
    ** \brief Represent Game first interface properties
    */
    static class GAME
    {
    public:
        static const uint32 MENU_GAP{80}; ///< Gap between the menu texts
        static const uint32 ORIGINAL_MENU_Y_POSITION{180};
        ///< First Y position for the menu
        static const uint32 PLAY_Y{ORIGINAL_MENU_Y_POSITION + MENU_GAP * 0};
        ///< Y play menu position
        static const uint32 SCORE_Y{ORIGINAL_MENU_Y_POSITION + MENU_GAP * 1};
        ///< Y score menu position
        static const uint32 MAP_EDITOR_Y{ORIGINAL_MENU_Y_POSITION + MENU_GAP*2};
        ///< Y map editor menu position
        static const uint32 OPTION_Y{ORIGINAL_MENU_Y_POSITION + MENU_GAP * 3};
        ///< Y option menu position
    } gameview;

    /*!
    ** \brief Represen Pacman view interface
    */
    static class PACVIEW
    {
    public:
        static const uint32 TILE_WIDTH{20};               ///< Tile width
        static const uint32 TILE_HEIGHT{20};              ///< Tile height
        static const uint32 MENU_HEIGHT{TILE_HEIGHT * 3}; ///< Menu heigth
        static const uint32 TEXTURE_TILE_WIDTH{34};     ///< Texture tile width
        static const uint32 TEXTURE_TILE_HEIGHT{34};    ///< Texture tile height
    } pacview;

    static class GHOSTVIEW
    {
    public:
        static const uint32 GHOST_TILE_WIDTH{32};
        static const uint32 GHOST_TILE_HEIGHT{32};
        static const uint32 EYE_TOP{0 * GHOST_TILE_WIDTH};
        static const uint32 EYE_BOTTOM{1 * GHOST_TILE_WIDTH};
        static const uint32 EYE_LEFT{2 * GHOST_TILE_WIDTH};
        static const uint32 EYE_RIGHT{3 * GHOST_TILE_WIDTH};
        static const uint32 GHOST1{4 * GHOST_TILE_WIDTH};
        static const uint32 GHOST2{5 * GHOST_TILE_WIDTH};
        static const uint32 DEAD_EYE{6 * GHOST_TILE_WIDTH};
    } ghostview;

protected:
    ConfView();                    ///< Private constructor for no instanciation
    ConfView(const ConfView&);            ///< Private copy constructor
    ConfView& operator=(const ConfView&); ///< Private copy operator

};

}

#endif // PACMAN_CONF_VIEW_HPP
