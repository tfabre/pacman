#ifndef PACMAN_VIEW_TYPE_VIEW_HPP
#define PACMAN_VIEW_TYPE_VIEW_HPP

#include "pacman/util/types.hpp"

namespace pm
{

/*!
** \brief Reprensent the different view displayable from Game interface
*/
enum class ViewType: uint32
{
    MENU = 0,       ///< Menu view
    PACMAN = 1,     ///< Pacman game view
    SCORES = 2,     ///< Score view
    MAP_EDITOR = 3, ///< Level editor view
    OPTION = 4,     ///< Option view
    GAME_OVER = 5   ///< Game over screen
};

}

#endif // PACMAN_VIEW_TYPE_VIEW_HPP
