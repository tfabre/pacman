#ifndef PACMAN_PLAY_HPP
#define PACMAN_PLAY_HPP

#include "pacman/util/Observer.hpp"
#include "pacman/view/View.hpp"
#include "pacman/view/Game.hpp"
#include "pacman/view/PacmanView.hpp"
#include "pacman/view/GhostView.hpp"
#include "pacman/view/MapView.hpp"
#include "pacman/model/character/Pacman.hpp"

namespace pm
{

/*!
** \brief Represent the game interface
*/ 
class Play: public View, public Observer
{
public:
    /*!
    ** \brief Default constructor
    **
    ** \parent parent Parent game interface
    */
    Play(Game* const parent);

    /*!
    ** \brief Draw the Play view
    **
    ** \see View::drawView()
    */
    virtual void drawView();

    /*!
    ** \brief Try to change the pacman direction
    **
    ** \param direction New pacman direction
    */
    void changePacmanDirection(const Direction& direction);

    /*!
    ** \brief observe the Pacman and when he eat an item, it ask MapView to
    **        remove the item from the display
    */
    virtual void actualisate(const Event& event) override;

    /*!
    ** \brief Indicate if the interface is in pause or not
    **
    ** \return true if the progamme is paused, false otherwise
    */
    bool isPaused() const;

    /*!
    ** \brief Set the program in pause
    */
    void pause();

    /*!
    ** \brief Resume the program from a pause
    */
    void resume();

    /*!
    ** \brief Return Play parent view
    */
    Game* getParent() const;

private:
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

    /*!
    ** \brief Retry pacman changing direction if
    **        changePacmanDirection(const Direction& direction) failed
    */
    void changePacmanDirection();
    Game* parent;      ///< Parent interface
    PacmanView pacman; ///< The pacman graphical representation
    MapView map;       ///< Map graphical representation
    GhostView blinky;  ///< Ghost representation
    GhostView pinky;   ///< Pinky representation
    GhostView inky;    ///< Inky representation
    GhostView clyde;   ///< Clyde representation

    Direction lastDirection; ///< The last direction input
    bool directionChanged;
    ///< If the direction has changed on changePacmanDirection(const Direction&)

    sf::Font pacfont;        ///< Font ues for texts
    sf::Text scoreText;      ///< Title text

    sf::Sprite lifeSprite;   ///< sprite tfor life
    sf::Texture texture;     ///< texture for life
    sf::Text lifeText;       ///< Remaining life text

    sf::Text pauseText;      ///< Text for pause
    bool paused;             ///< If the interface is in pause or not
};

}

#endif // PACMAN_PLAY_HPP
