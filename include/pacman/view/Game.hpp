#ifndef PACMAN_GAME_VIEW_HPP
#define PACMAN_GAME_VIEW_HPP

#include <map>
#include <vector>
#include "pacman/view/View.hpp"
#include "pacman/view/ViewType.hpp"

namespace pm
{

/*!
** \brief Represent the Game first interface (menu) and entry point
*/
class Game: public View
{
public:
    /*!
    ** \brief Default constructor
    */
    Game();

    /*!
    ** \brief Default destructor
    */
    ~Game();

    /*!
    ** \copydoc pm::View::drawView()
    */
    virtual void drawView() override;

    /*!
    ** \brief The main loop
    */
    void run();

    /*!
    ** \brief allow to change the current view to option or game interface for
    **        exemple
    **
    ** \param type View type to change
    */
    void setCurrentView(const ViewType & type);



    /*!
    ** \brief allow to change the current music in game
    **
    ** \param filename path to filename
    ** \param loop true to loop the music (default = true)
    ** \param volume volume number /100 (default = 25)
    */
    void setCurrentMusic(const std::string filename, bool loop=true, uint8 volume=25);


    /*!
    ** \brief Delete and rebuild indicated view
    **
    ** \param type The view type to rebuild
    */
    void rebuildView(const ViewType& type);

    /*!
    ** \brief Return all text that represent menu to the interface
    **
    ** \return A list of the different texts that represent the menus
    */
    std::vector<sf::Text> getMenuTexts() const;

    /*!
    ** \brief Focus text by color them in yellow
    **
    ** \param txt Text to focus
    ** \param focus Indicate if we focus are not
    */
    void focusText(const sf::Text & txt, const bool & focus);

private:
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

    sf::Font pacfont;       ///< Font ues for texts
    sf::Text titleText;     ///< Title text
    sf::Text playText;      ///< Play menu text
    sf::Text scoreText;     ///< Score menu text
    sf::Text mapEditorText; ///< Map Editor menu text
    sf::Text optionText;    ///< Option menu text

    View* currentView;      ///< Current view to display
    std::map<ViewType, View*> views; ///< All the view to display

    sf::Music music;
    std::string currentlyPlaying;
};

} // namespace pm

#endif // PACMAN_GAME_VIEW_HPP
