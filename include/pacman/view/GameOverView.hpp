#ifndef PACMAN_GAME_OVER_VIEW_HPP
#define PACMAN_GAME_OVER_VIEW_HPP

#include "pacman/view/View.hpp"
#include "pacman/view/Game.hpp"

namespace pm
{

/*!
** \brief Game over screen
*/
class GameOverView: public View
{
public:
    /*!
    ** \brief Default constructor
    **
    ** \param parent Parent interface
    */
    GameOverView(Game* const parent);

    virtual void drawView() override;

    /*!
    ** \brief Return parent class
    **
    ** \return Parent interface
    */
    Game* getParent() const;

private:
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

    Game* parent;
    sf::Font font;
    sf::Text gameOver;
};

}

#endif // PACMAN_GAME_OVER_VIEW_HPP
