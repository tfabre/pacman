#ifndef PACMAN_PACMAN_VIEW_HPP
#define PACMAN_PACMAN_VIEW_HPP

#include <SFML/Graphics.hpp>
#include "pacman/model/character/Pacman.hpp"

namespace pm
{

/*!
** \brief Represent the pacman entity on the screen
*/
class PacmanView: public sf::Drawable
{
public:
    /*!
    ** \brief Default constructor
    */
    PacmanView();

    /*!
    ** \brief Default destructor
    */
    ~PacmanView();

    /*!
    ** \brief Draw Pacman on the window
    */
    void drawPacman();

    /*!
    ** \brief Return a reference to pacman
    **
    ** \return Reference to pacman
    */
    Pacman* getPacman() const;

    /*!
    ** \brief Set pacman in pause
    **
    ** \param pause Set in pause or not
    */
    void setPause(const bool& pause);

private:
    /*!
    ** \brief Change pacman orientation depending on pacman model direction
    */
    void changePacmanOrientation();

    /*!
    ** \brief Change pacman animation each centisencond
    */
    void animatePacman();

    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;
    Pacman* pacman; ///< Pacman entity

    sf::Sprite sprite;   ///< Sprite
    sf::Texture texture; ///< Texture
    sf::Clock clock;     ///< Internal clock to animate pacman

    bool paused;         ///< Indicate if pacman is on pause or not
};

}

#endif // PACMAN_PACMAN_VIEW_HPP
