#ifndef PACMAN_GHOST_VIEW_HPP
#define PACMAN_GHOST_VIEW_HPP

#include <SFML/Graphics.hpp>
#include "pacman/model/character/enemy/Ghost.hpp"

namespace pm
{

/*!
** \brief Represent the ghost entity on the screen
*/
class GhostView: public sf::Drawable
{
public:
    /*!
    ** \brief Default constructor
    **
    ** \param ghost The ghost to display
    ** \param color Ghost color
    */
    GhostView(Ghost* const ghost, const sf::Color& color);

    /*!
    ** \brief Default destructor
    */
    ~GhostView();

    /*!
    ** \brief Draw ghost on the window
    */
    void drawGhost();

    /*!
    ** \brief Return a reference to ghost
    **
    ** \return Reference to ghost
    */
    Ghost* getGhost() const;

    /*!
    ** \brief Set view in pause
    */
    void pause();

    /*!
    ** \brief Resume from pause
    */
    void resume();

private:
    virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

    /*!
    ** \brief Set ghost eyes color and direction depending on direction
    **
    ** \param dir Ghost direction
    */
    void setEyeDirectionAndColor(const Direction& dir);

    Ghost* ghost; ///< Ghost entity

    sf::Sprite ghostSprite;   ///< Ghost sprite
    sf::Sprite eye;     ///< Ghost eye sprite
    sf::Texture texture; ///< Texture
    sf::Color color;     ///< Ghost color
    sf::Clock clock;     ///< internal clock
    bool isPaused;       ///< Indicate if the view is in pause or not

    sf::Font font;       ///< Font for score display
    sf::Text score;      ///< Score text
    sf::Clock textClock; ///< Clock for score display
    bool isDead;         ///< Indicate if ghost is dead or not
};

}

#endif // PACMAN_GHOST_VIEW_HPP
