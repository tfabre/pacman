#ifndef PACMAN_GAME_CTRL_HPP
#define PACMAN_GAME_CTRL_HPP

#include "pacman/controller/Controller.hpp"

namespace pm
{

/*!
** \brief Controller for Game view
*/
class GameCtrl: public Controller
{
public:
    /*!
    ** \brief Default constructor
    **
    ** \see Controller::Controller()
    */
    using Controller::Controller;

    /*!
    ** \brief Dault destructor
    **
    ** \see Controller::~Controller
    */
    virtual ~GameCtrl();

    /*!
    ** \brief Check for pacman move event (arrow key input by Player)
    **
    ** \see Controller::checkEvent(const sf::Event& event)
    */
    void checkEvent(const sf::Event & event);
};

} // namespace pm

#endif // PACMAN_GAME_CTRL_HPP
