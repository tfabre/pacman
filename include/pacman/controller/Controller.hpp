#ifndef PACMAN_CONTROLLER_HPP
#define PACMAN_CONTROLLER_HPP

#include <SFML/Window.hpp>
#include "pacman/view/View.hpp"

namespace pm
{

class View;

/*!
** \brief Reprensent controller base class
*/
class Controller
{
public:
    /*!
    ** \brief Basic constructor that link controller to a view
    **
    ** \param view View to link with the controller
    */
    Controller(View* const view);

    /*!
    ** \brief Default virtual constructor
    */
    virtual ~Controller() = 0;

    /*!
    ** \brief Check for event type and do view or model modification if the
    **        program must react to the event
    ** 
    ** \param event Event type
    */
    virtual void checkEvent(const sf::Event & event) = 0;

    /*!
    ** \brief Check for event not based on SFML event (game event)and do view
    **        or model modification if the program must react to the event
    */
    virtual void checkExtraEvent();

    /*!
    ** \brief Return a reference to the view
    **
    ** \return A reference to the view the controller is binded with
    */
    View* getView() const;

private:
    View* view; ///< The view binded to controller
};

} // namespace pm

#endif // PACMAN_CONTROLLER_HPP
