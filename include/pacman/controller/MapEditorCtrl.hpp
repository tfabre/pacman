#ifndef PACMAN_MAP_EDITOR_CTRL_HPP
#define PACMAN_MAP_EDITOR_CTRL_HPP

#include "pacman/controller/Controller.hpp"

#define ADD_WALL_TILE 0
#define ADD_NONE_TILE 1
#define ADD_DOT_TILE  2
#define ADD_POWER_PELLET_TILE 3
#define ADD_GHOST_WALL_TILE 4
#define ADD_PACMAN_TILE 5
#define ADD_FRUIT_TILE 6
#define SAVE_LEVEL 7

#define PACMAN_NUMBER_FOR_FILE 10
#define FRUIT_NUMBER_FOR_FILE 11

namespace pm
{

class MapEditorCtrl: public Controller
{
public:

    // using Controller::Controller;
    MapEditorCtrl(View* const v);

    virtual ~MapEditorCtrl();

    virtual void checkEvent(const sf::Event& event);

private:

    sf::Vector2i currentPos;

};


} // namespace pm


#endif // PACMAN_MAP_EDITOR_CTRL_HPP