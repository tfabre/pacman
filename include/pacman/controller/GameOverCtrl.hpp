#ifndef PACMAN_GAME_OVER_CTRL_HPP
#define PACMAN_GAME_OVER_CTRL_HPP

#include "pacman/controller/Controller.hpp"

namespace pm
{

/*!
** \brief Controller for GameOverView
*/
class GameOverCtrl: public Controller
{
public:
    /*!
    ** \brief Using default Controller constructor
    **
    ** \see Controller::Controller()
    */
    using Controller::Controller;

    /*!
    ** \brief Default destructor
    */
    ~GameOverCtrl();

    /*!
    ** \brief Check for event to go to the menu or to continue to play
    **
    ** \see Controller::checkEvent(const sf::Event& event)
    */
    virtual void checkEvent(const sf::Event& event) override;
};

} // namespace pm

#endif // PACMAN_GAME_OVER_CTRL_HPP
