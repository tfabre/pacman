#ifndef PACMAN_PLAY_CTRL_HPP
#define PACMAN_PLAY_CTRL_HPP

#include "pacman/controller/Controller.hpp"

namespace pm
{

/*!
** \brief Controller for Play view
*/
class PlayCtrl: public Controller
{
public:
    /*!
    ** \brief Default constructor
    **
    ** \see Controller::Controller()
    */
    using Controller::Controller;

    /*!
    ** \brief Dault destructor
    **
    ** \see Controller::~Controller
    */
    virtual ~PlayCtrl();

    /*!
    ** \brief Check for pacman move event (arrow key input by Player)
    **
    ** \see Controller::checkEvent(const sf::Event& event)
    */
    virtual void checkEvent(const sf::Event& event);

    /*!
    ** \brief Check for game life number and set GameOverView if life number is
    **        lower than 0
    */
    virtual void checkExtraEvent() override;
};

}

#endif // PACMAN_PLAY_CTRL_HPP
