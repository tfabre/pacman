#ifndef PACMAN_SCORE_CTRL_HPP
#define PACMAN_SCORE_CTRL_HPP

#include "pacman/controller/Controller.hpp"

namespace pm
{

/*!
** \brief Controller for ScoreView
*/
class ScoreCtrl: public Controller
{
public:
    /*!
    ** \brief Using default Controller constructor
    **
    ** \see Controller::Controller()
    */
    using Controller::Controller;

    /*!
    ** \brief Default destructor
    */
    ~ScoreCtrl();

    /*!
    ** \brief Check for event to go back to the menu
    **
    ** \see Controller::checkEvent(const sf::Event& event)
    */
    virtual void checkEvent(const sf::Event& event) override;
};

} // namespace pm

#endif // PACMAN_SCORE_CTRL_HPP
