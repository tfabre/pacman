#ifndef PACMAN_POSITIONNED_HPP
#define PACMAN_POSITIONNED_HPP

#include "pacman/util/Vector2.hpp"
#include "pacman/model/map/TileMap.hpp"

namespace pm
{

/*!
** \brief Represent something that have position on the map
*/
class Positionned
{
public:
    /*!
    ** \brief Construct a positionned object & set position and speed and
    **        tilemap value
    **
    ** \param position The original position
    ** \param speed The original speed
    ** \param size The entity size
    ** \param tilemap The tile map
    */
    Positionned(const Vector2f & pos, const float & speed, const float & size,
                TileMap* tm);

    /*!
    ** \brief Default virtual destructor
    */
    virtual ~Positionned() = 0;

    /*!
    ** \brief Return the current position
    ** 
    ** \return The current position
    */
    Vector2f getPosition() const;

    /*!
    ** \brief Return the current speed
    ** 
    ** \return The current speed
    */
    float getSpeed() const;

    /*!
    ** \brief Return the current TileMap
    ** 
    ** \return The current TileMap
    */
    TileMap* getTileMap() const;

    /*!
    ** \brief Return the current entity size
    **
    ** \return The current size
    */
    float getSize() const;

    /*!
    ** \brief Set a new position
    **
    ** \param position The new position
    */
    virtual void setPosition(const Vector2f & pos);

    /*!
    ** \brief Set the new speed
    **
    ** \param speed The new speed
    */
    void setSpeed(const float & speed);

    /*!
    ** \brief Set a new TileMap
    **
    ** \param tile_map The new tile map
    */
    void setTileMap(TileMap* tm);

    /*!
    ** \brief Set new size
    **
    ** \param size the new size
    */
    void setSize(const float & size);

    /*!
    ** \brief Detect if the current entity had collision with the one passed in
    **        parameter
    **
    ** \param pos The entity to test collision with
    ** \return true if there is collision with the entity passed in parameter,
    **         false otherwise
    */
    bool haveCollisionWith(const Positionned* pos) const;

    /*!
    ** \brief Detect if there is a collision between the 2 entity passed in
    **        paramter
    **
    ** \param p1 The first entity
    ** \param p2 The second entity
    ** \return true if there is a collision between p1 and p2 false otherwise
    */
    static bool detectCollision(const Positionned* p1, const Positionned* p2);

private:
    Vector2f position; ///< The current position on the map
    float speed;       ///< The current speed
    float size;        ///< The entity size
    TileMap* tileMap;  ///< The current tile map
};

}

#endif // PACMAN_POSITIONNED_HPP
