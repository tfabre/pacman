#ifndef PACMAN_GHOST_HPP
#define PACMAN_GHOST_HPP

#include "pacman/util/Event.hpp"
#include "pacman/util/Observer.hpp"
#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"
#include "pacman/model/character/enemy/behavior/BackHome.hpp"

namespace pm
{

/*!
** \brief Interface to manipulate Ghost entity
*/
class Ghost: public Observer
{
public:
    /*!
    ** \brief Default Ghost constructor
    */
    Ghost(EnemyBehavior* const eb, EnemyBehavior* const backHome);

    /*!
    ** \brief Default destructor that free EnemyBehavior reference
    */
    ~Ghost();

    /*!
    ** \brief Move Ghost entity
    */
    void move();

    /*!
    ** \brief Get ghost position
    **
    ** \return Ghost position
    */
    Vector2f getPosition() const;
    
    /*!
    ** \brief Get ghost direction
    **
    ** \return Ghost direction
    */
    Direction getDirection() const;

    /*!
    ** \brief Return ghost behavior
    **
    ** \return Ghost behavior
    */
    EnemyBehavior* getBehavior() const;

    /*!
    ** \brief Return ghost state
    **
    ** \return Ghost state
    */
    EnemyState getState() const;
    
    /*!
    ** \see Observer::actualisate(const Event& event)
    */
    void actualisate(const Event& event) override;

    /*!
    ** \brief replace Enemy to their origin
    */
    void replace();

    /*!
    ** \brief Set the ghost in pause
    */
    void pause();

    /*!
    ** \brief Resume the ghost from pause
    */
    void resume();

private:
    EnemyBehavior* behavior; ///< Ghost behavior
    EnemyBehavior* backHome; ///< Back home behavior
    Vector2f origin;         ///< Origin point
};

}

#endif // PACMAN_GHOST_HPP
