#ifndef PACMAN_SPEEDY_HPP
#define PACMAN_SPEEDY_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

class Speedy: public EnemyBehavior
{
public:
    Speedy( const Vector2f & pos, const float & speed, const float & size,
            TileMap* tm, Pacman* const pm, PathFinding* pf );
    virtual void move() override;
};

}

#endif // PACMAN_SPEEDY_HPP
