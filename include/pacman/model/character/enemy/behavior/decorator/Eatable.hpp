#ifndef PACMAN_EATABLE_HPP
#define PACMAN_EATABLE_HPP

#include "pacman/model/character/enemy/behavior/decorator/EnemyBehaviorDecorator.hpp"
#include "pacman/util/Event.hpp"
#include "pacman/util/Subject.hpp"

namespace pm
{

/*!
** \brief Represent the eatable enemy characteristic
*/
class Eatable: public EnemyBehaviorDecorator, public Subject
{
public:
    /*!
    ** \brief Default constructor
    ** \see EnemyBehaviorDecorator
    */
    Eatable(EnemyBehavior* mainBehavior);

    /*!
    ** \brief Move the enemy and check if it isn't eaten by pacman
    */
    virtual void move() override;

    virtual void clearPath() override;

    virtual void setDirection(const Direction& direction) override;

    virtual void setPosition(const Vector2f& position) override;
};

}

#endif // PACMAN_EATABLE_HPP
