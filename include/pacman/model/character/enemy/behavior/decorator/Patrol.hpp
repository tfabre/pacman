#ifndef PACMAN_PATROL_HPP
#define PACMAN_PATROL_HPP

#include "pacman/util/types.hpp"
#include "pacman/util/Event.hpp"
#include "pacman/util/Observer.hpp"
#include "pacman/model/character/enemy/behavior/decorator/EnemyBehaviorDecorator.hpp"

namespace pm
{

/*!
** \brief Represent the behavior change between patrol and chase
*/
class Patrol: public EnemyBehaviorDecorator // , public Observer
{
public:
    /*!
    ** \brief Patrol decorator constructor
    **
    ** \param mainBehavior The default behavior
    ** \param pBehavior The enemy patrol behavior
    ** \param patrolDuration The patrol effect duration
    ** \param chaseDuration The chase duration
    */
    Patrol(EnemyBehavior* const mainBehavior, EnemyBehavior* const pBehavior,
           const float& patrolDuration, const float chaseDuration);

    /*!
    ** \brief Desctructor
    */
    virtual ~Patrol();

    // /*!
    // ** Respond to EAT_POWER_PELLET
    // */
    // virtual void actualisate(const Event & event) override;

    /*!
    ** \brief Choose the correct move() depending on the activation of the power
    **        pellet effect
    */
    virtual void move() override;

    virtual void clearPath() override;

    virtual void setDirection(const Direction& direction) override;

    virtual void setPosition(const Vector2f& position) override;

    /*!
    ** \see EnemyBehavior::replace(const Vector2f& orgin)
    */
    virtual void replace(const Vector2f& position) override;

    /*!
    ** \see EnemyBehavior::pause()
    */
    virtual void pause() override;

    /*!
    ** \see EnemyBehavior::resume()
    */
    virtual void resume() override;

private:
    /*!
    ** The behavior of when power pellet is activated
    */
    EnemyBehavior* patrolBehavior;

    Timer timer; ///< Internal timer
    float patrolDuration; ///< Patrol duration
    float chaseDuration;  ///< Chase duration
    bool isPatrolling; ///< Indicate if it is in patrol
};

}

#endif // PACMAN_PATROL_HPP
