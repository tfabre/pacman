#ifndef PACMAN_ENEMY_BEHAVIOR_DECORATOR_HPP
#define PACMAN_ENEMY_BEHAVIOR_DECORATOR_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

/*!
** \brief Decorate a behavior by modifying it
*/
class EnemyBehaviorDecorator: public EnemyBehavior
{
public:
    /*!
    ** \brief Construct new Decorator
    **
    ** \param behavior The behavior to decorate
    */
    EnemyBehaviorDecorator(EnemyBehavior* behavior);

    /*!
    ** \brief Destroy the decorator
    */
    virtual ~EnemyBehaviorDecorator();

    /*!
    ** \brief Modifiate the enemy position
    ** This is the decorated methode
    */
    virtual void move() = 0;

    /*!
    ** \brief Return the enemy behavior
    **
    ** \return The enemy behavior
    */
    EnemyBehavior* getBehavior() const;

    /*!
    ** \see EnemyBehavior::replace(const Vector2f& orgin)
    */
    virtual void replace(const Vector2f& origin) override;

private:
    EnemyBehavior* behavior; ///< The behavior to decorate
};

}

#endif // PACMAN_ENEMY_BEHAVIOR_DECORATOR_HPP
