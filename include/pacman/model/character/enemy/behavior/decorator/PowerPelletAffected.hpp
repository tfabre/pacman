#ifndef PACMAN_POWER_PELLET_AFFECTED_HPP
#define PACMAN_POWER_PELLET_AFFECTED_HPP

#include "pacman/util/types.hpp"
#include "pacman/util/Event.hpp"
#include "pacman/util/Observer.hpp"
#include "pacman/util/Timer.hpp"
#include "pacman/model/character/enemy/behavior/decorator/EnemyBehaviorDecorator.hpp"

namespace pm
{

/*!
** \brief Represent the behavior change when Power Pellet is eaten
*/
class PowerPelletAffected: public EnemyBehaviorDecorator, public Observer
{
public:
    /*!
    ** \brief PowerPelletAffected decorator constructor
    **
    ** \param mainBehavior The default behavior
    ** \param ppaBehavior The enemy behavior when power pellet is eaten
    ** \param duration The power pellet effect duration
    */
    PowerPelletAffected(EnemyBehavior* mainBehavior, EnemyBehavior* ppaBehavior,
                        const float & duration);

    /*!
    ** \brief Desctructor
    */
    virtual ~PowerPelletAffected();

    /*!
    ** Respond to EAT_POWER_PELLET
    */
    virtual void actualisate(const Event & event) override;

    /*!
    ** \brief Choose the correct move() depending on the activation of the power
    **        pellet effect
    */
    virtual void move() override;

    virtual void clearPath() override;

    virtual void setDirection(const Direction& direction) override;

    virtual void setPosition(const Vector2f& position) override;

    /*!
    ** \see EnemyBehavior::replace(const Vector2f& orgin)
    */
    virtual void replace(const Vector2f& origin) override;

    /*!
    ** \see EnemyBehavior::pause()
    */
    virtual void pause() override;

    /*!
    ** \see EnemyBehavior::resume()
    */
    virtual void resume() override;

private:
    /*!
    ** The behavior of when power pellet is activated
    */
    EnemyBehavior* affectedPelletBehavior;

    Timer timer; ///< Internal timer
    float duration; ///< Power pellet effect duration
    bool isEffectActive; ///< Indicate if the effect is enabled
};

}

#endif // PACMAN_POWER_PELLET_AFFECTED_HPP
