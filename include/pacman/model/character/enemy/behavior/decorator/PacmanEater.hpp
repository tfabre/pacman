#ifndef PACMAN_PACMAN_EATER_HPP
#define PACMAN_PACMAN_EATER_HPP

#include "pacman/model/character/enemy/behavior/decorator/EnemyBehaviorDecorator.hpp"
#include "pacman/util/Event.hpp"
#include "pacman/util/Subject.hpp"

namespace pm
{
/*!
** \brief Represent the pacman eating faculty for enemy
*/
class PacmanEater: public EnemyBehaviorDecorator, public Subject
{
public:
    /*!
    ** \brief Default constructor
    ** \see EnemyBehaviorDecorator
    */
    PacmanEater(EnemyBehavior* behavior);

    /*!
    ** \brief Move the enemy and check if pacman is eaten
    */
    virtual void move() override;

    virtual void clearPath() override;
    
    virtual void setDirection(const Direction& direction) override;

    virtual void setPosition(const Vector2f& position) override;
};

}

#endif // PACMAN_PACMAN_EATER_HPP
