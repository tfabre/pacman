#ifndef PACMAN_ENEMY_BEHAVIOR_HPP
#define PACMAN_ENEMY_BEHAVIOR_HPP

#include <stack>
#include <cmath>

#include "pacman/util/Vector2.hpp"
#include "pacman/model/character/enemy/PathFinding.hpp"
#include "pacman/model/character/Pacman.hpp"
#include "pacman/model/character/Positionned.hpp"
#include "pacman/model/character/enemy/behavior/EnemyState.hpp"

namespace pm
{

/*!
** \brief Represent the enemy behavior
*/
class EnemyBehavior: public Positionned
{
public:
    /*!
    ** \brief Construct an EnemyBehavior with a reference to pacman
    **
    ** \param position The original position
    ** \param speed The original speed
    ** \param size The enemy size
    ** \param tilemap The tile map
    ** \param pacman A reference to the pacman
    */
    EnemyBehavior(const Vector2f & pos, const float & speed, const float & size,
                  TileMap* tm, Pacman* const pacman, PathFinding* pf );

    /*!
    ** \brief Destruct an EnemyBehavior
    */
    virtual ~EnemyBehavior();

    /*!
    ** \brief Moves the enemy
    */
    virtual void move() = 0;

    /*!
    ** \brief Returns a reference to pacman
    **
    ** \return The pacman
    */
    Pacman* getPacman();

    /*!
    ** \brief Returns a reference to the path enemy
    **
    ** \return The path of enemy
    */
    std::stack< Vector2<float> >* getPath() const;

    /*!
    ** \brief Returns a reference to the pathfinding
    **
    ** \return The PathFinding object
    */
    PathFinding* getPathFinding();

    /*!
    ** \brief Returns the current direction where goes the enemy
    **
    ** \return The direction of enemy
    */
    Direction getDirection() const;

    virtual void setDirection(const Direction& direction);

    bool isPacmanOnView( uint32 distance );

    bool attackPacmanOnView( uint32 distance );

    bool finishMove();

    /*!
    ** \brief Set enemy state
    **
    ** \param state New EnemyState to set
    */
    void setState(const EnemyState& state);

    /*!
    ** \brief Return current state
    **
    ** \return Current state
    */
    EnemyState getState() const;

    /*!
    ** \brief Reset and recompute path
    */
    virtual void clearPath();

    /*!
    ** \brief Replace enemy to the orgin point and reset its state
    **
    ** \param origin The point the replace the enemy
    */
    virtual void replace(const Vector2f& origin);

    /*!
    ** \brief Set pause in behavior
    */
    virtual void pause();

    /*!
    ** \brief Resume from pause
    */
    virtual void resume();

protected:
    void reposition();
    void moveTo( Direction dir );
    void attack();
    std::stack< Vector2<float> >* path; ///< The path of enemy

private:
    Pacman* const pacman; ///< A reference to the pacman
    PathFinding* const pf; ///< A reference to the pathfinding
    Direction dir;

    EnemyState state;
};

}

#endif // PACMAN_ENEMY_BEHAVIOR_HPP
