#ifndef PACMAN_POKEY_HPP
#define PACMAN_POKEY_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

class Pokey: public EnemyBehavior
{
public:
    Pokey( const Vector2f & pos, const float & speed, const float & size,
            TileMap* tm, Pacman* const pm, PathFinding* pf );
    virtual void move() override;
};

}

#endif // PACMAN_POKEY_HPP
 

