#ifndef PACMAN_PATROLLING_HPP
#define PACMAN_PATROLLING_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

class Patrolling: public EnemyBehavior
{
public:
    Patrolling( const Vector2f & pos, const float & speed, const float & size,
                TileMap* tm, Pacman* const pacman, PathFinding* pf );
    virtual void move() override;
};

}

#endif // PACMAN_PATROLLING_HPP
