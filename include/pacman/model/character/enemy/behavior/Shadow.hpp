#ifndef PACMAN_SHADOW_HPP
#define PACMAN_SHADOW_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

class Shadow: public EnemyBehavior
{
public:
    Shadow( const Vector2f & pos, const float & speed, const float & size,
            TileMap* tm, Pacman* const pm, PathFinding* pf );
    virtual void move() override;
};

}

#endif // PACMAN_SHADOW_HPP
