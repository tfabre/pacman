#ifndef PACMAN_LEAK_HPP
#define PACMAN_LEAK_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

class Leak: public EnemyBehavior
{
public:
    Leak( const Vector2f & pos, const float & speed, const float & size,
            TileMap* tm, Pacman* const pm, PathFinding* pf );
    virtual void move() override;
};

}

#endif // PACMAN_LEAK_HPP
