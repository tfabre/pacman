#ifndef PACMAN_ENEMY_STATE_HPP
#define PACMAN_ENEMY_STATE_HPP

namespace pm
{

/*!
** \brief Represent different enemy state
*/
enum class EnemyState
{
    NORMAL,    ///< Normal state
    LEAK,      ///< Leak state
    BACK_HOME  ///< Back home state (when enemy is eaten)
};

} // namespace pm

#endif // PACMAN_ENEMY_STATE_HPP
