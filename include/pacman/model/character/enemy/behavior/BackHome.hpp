#ifndef PACMAN_BACK_HOME_HPP
#define PACMAN_BACK_HOME_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

class BackHome: public EnemyBehavior
{
public:
    BackHome( const Vector2f & pos, const float & speed, const float & size,
            TileMap* tm, Pacman* const pm, PathFinding* pf );
    virtual void move() override;
};

}

#endif // PACMAN_BACK_HOME_HPP
