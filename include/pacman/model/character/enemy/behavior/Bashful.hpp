#ifndef PACMAN_BASHFUL_HPP
#define PACMAN_BASHFUL_HPP

#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

class Bashful: public EnemyBehavior
{
public:
    Bashful( const Vector2f & pos, const float & speed, const float & size,
             TileMap* tm, Pacman* const pm, PathFinding* pf,
             EnemyBehavior* shadow );
    virtual void move() override;
private:
    EnemyBehavior* sh;
};

}

#endif // PACMAN_BASHFUL_HPP
