#ifndef MAPFINDING_H_INCLUDED
#define MAPFINDING_H_INCLUDED

#include <iostream>
#include <stack>
#include <vector>

#include "pacman/util/types.hpp"
#include "pacman/util/Vector2.hpp"
#include "pacman/model/map/TileType.hpp"
#include "pacman/model/map/TileMap.hpp"
#include "pacman/model/character/Direction.hpp"

namespace pm
{

struct s_square
{
    Vector2<float> pos;
    float g, h;
    s_square* parent;

    s_square( float x, float y, float g, float h, s_square* parent ) : pos(x, y), g(g), h(h), parent(parent)
    {
    }

    s_square( Vector2<float> pos, float g, float h, s_square* parent ) : pos(pos), g(g), h(h), parent(parent)
    {
    }

    float getScore()
    {
        return g + h;
    }
};

class PathFinding
{
private:
    std::vector<s_square*> openList;
    std::vector<s_square*> closeList;
    TileMap* tm;
    void freeLists();

public:
    PathFinding( TileMap* tm ) : tm(tm)
    {
    }

    ~PathFinding()
    {
        freeLists();
    }

    void path( std::stack< Vector2<float> >* pathStack, uint32 xStart, uint32 yStart, uint32 xEnd, uint32 yEnd);
    void path( std::stack< Vector2<float> >* pathStack, const Vector2<float>& start, const Vector2<float>& end );
    float getDistance( uint32 xStart, uint32 yStart, uint32 xEnd, uint32 yEnd );
};

} // namespace pm

#endif // MAPFINDING_H_INCLUDED

