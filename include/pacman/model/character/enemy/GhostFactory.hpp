#ifndef PACMAN_GHOST_FACTORY_HPP
#define PACMAN_GHOST_FACTORY_HPP

#include "pacman/util/Vector2.hpp"
#include "pacman/model/character/enemy/Ghost.hpp"
#include "pacman/model/character/enemy/behavior/BackHome.hpp"
#include "pacman/model/character/enemy/behavior/Bashful.hpp"
#include "pacman/model/character/enemy/behavior/Leak.hpp"
#include "pacman/model/character/enemy/behavior/Pokey.hpp"
#include "pacman/model/character/enemy/behavior/Shadow.hpp"
#include "pacman/model/character/enemy/behavior/Speedy.hpp"
#include "pacman/model/character/enemy/behavior/decorator/Eatable.hpp"
#include "pacman/model/character/enemy/behavior/decorator/PacmanEater.hpp"
#include "pacman/model/character/enemy/behavior/decorator/PowerPelletAffected.hpp"

namespace pm
{

/*!
** \brief Build ghosts with good behavior
*/
class GhostFactory
{
public:
    /*!
    ** \brief Build Bashful ghost
    **
    ** \param position Origin ghost position
    ** \param blinky Reference to blinky
    ** \return Bashful
    */
    static Ghost* getNewBashful(const Vector2f & position, const Ghost* blinky);

    /*!
    ** \brief Build Bashful ghost
    **
    ** \param position Origin ghost position
    ** \return Bashful
    */
    static Ghost* getNewPokey(const Vector2f & position);

    /*!
    ** \brief Build Bashful ghost
    **
    ** \param position Origin ghost position
    ** \return Bashful
    */
    static Ghost* getNewShadow(const Vector2f & position);

    /*!
    ** \brief Build Bashful ghost
    **
    ** \param position Origin ghost position
    ** \return Bashful
    */
    static Ghost* getNewSpeedy(const Vector2f & position);

private:
    /*!
    ** \brief Build classic ghost decorator and decorate behavior given in
    **        parameter
    **
    ** \param behavior Behavior to decorate
    ** \return A fully build ghost
    */
    static Ghost* buildDecorator(EnemyBehavior* const behavior);
};

}

#endif // PACMAN_GHOST_FACTORY_HPP
