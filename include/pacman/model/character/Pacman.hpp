#ifndef PACMAN_PACMAN_HPP
#define PACMAN_PACMAN_HPP

#include <gtest/gtest.h>

#include "pacman/util/Subject.hpp"
#include "pacman/util/Vector2.hpp"
#include "pacman/model/item/ItemFactory.hpp"
#include "pacman/model/character/Direction.hpp"
#include "pacman/model/character/Positionned.hpp"

namespace pm
{

/*!
** \brief Represent pacman, its position and its actions
*/
class Pacman: public Positionned, public Subject
{
public:
    /*!
    ** \brief Construct pacman with an original position & direction
    **
    ** \param position The original position
    ** \param speed The original speed
    ** \param tilemap The tile map
    ** \param origin The original pacman position
    ** \param direction The default direction at the beginning of the game
    */
    Pacman(const Vector2f & pos, const float & speed, TileMap* tm,
           const Vector2f & origin, const Direction & direction);

    /*!
    ** \brief Change the pacman position
    */
    void move();

    /*!
    ** \brief Set pacman to its original poisition
    */
    void replace();

    /*!
    ** \brief Change the current pacman direction
    **
    ** \param direction The new pacman direction
    ** \return true if the direction changed
    */
    bool changeDirection(const Direction & direction);

    /*!
    ** \brief Return the item that have been eat by pacman
    **
    ** \return The last item eaten
    */
    Item getEatenItem() const;

    /*!
    ** \brief Return pacman orignal position
    **
    ** \return Pacman original position
    */
    Vector2f getOriginalPosition() const;

    /*!
    **  \brief Return pacman's current direction
    **
    **  \return Current pacman direction
    */
    Direction getCurrentDirection() const;

protected:
    /*!
    ** \brief Fill surrounding array with tile around the tile at the position
    **        given in parameter
    **
    ** \param position The position
    ** \param surrounding The array to fill
    */
    void getSurrounding(const Vector2f & pos, TileType surrounding[]) const;

    /*!
    ** \brief Test if the pacman can go through the tile given in parameter
    **
    ** \param tile The tile to test
    ** \return If the pacman can go through the given tile
    */
    bool haveCollisionWith(const TileType & tile) const;

    /*!
    ** \brief Detect if for the position given in parameter, the pacman can move
    **        to this position
    **
    ** \param position The position to check
    ** \return false if the pacman can move to this position
    */
    bool detectCollision(const Vector2f & pos) const;

    /*!
    ** \brief Compute the new direction depending on the direction
    **
    ** \return New position coordinate
    */
    Vector2f computeNewPosition() const;

    /*!
    ** \brief Compute the x & y coordinate to go through an angle
    **
    ** This method fool detectCollision method to avoid collision detection on
    ** angle by decreasing the speed value. To ensure proper move functioning,
    ** the position's values must be rounded after the collisionDection passed
    ** (to set a normal speed)
    ** \param position Position to compute new position from
    ** \param speed The speed (passed in parameter because it was compute
    **              previously)
    */
    void moveInAngle(Vector2f & position, const float & speed) const;

    // TEST
    friend class PacmanTest;
    FRIEND_TEST(PacmanTest, get_surrounding_test);
    FRIEND_TEST(PacmanTest, have_collision_with_test);
    FRIEND_TEST(PacmanTest, detect_collision_test);

private:
    Direction direction; ///< The current pacman direction
    Vector2f origin; ///< The pacman origin on the map
    Item lastItemEaten; ///< The last item pacman eaten
};

}

#endif // PACMAN_PACMAN_HPP
