#ifndef PACMAN_DIRECTION_HPP
#define PACMAN_DIRECTION_HPP

namespace pm
{

enum Direction
{
    TOP,
    BOTTOM,
    RIGHT,
    LEFT,
    NO_DIRECTION
};

}

#endif // PACMAN_DIRECTION_HPP
