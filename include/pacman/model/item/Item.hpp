#ifndef PACMAN_ITEM_HPP
#define PACMAN_ITEM_HPP

#include "pacman/util/types.hpp"
#include "pacman/model/item/ItemType.hpp"

namespace pm
{

/*!
** \brief Represent an Item (dot, power pellet, fruit...)
*/
class Item
{
public:
    /*!
    ** \brief Construct Item
    **
    ** \param item Item type
    ** \param score Score associate to item type
    */
    Item(const ItemType & it, const uint32 & score);

    /*!
    ** \brief Return the item type (CHERRY, ORANGE, ...)
    **
    ** \return The item type
    */
    ItemType getType() const;

    /*!
    ** \brief Return the score associate to the item
    **
    ** \return The score associate to the item
    */
    uint32 getScore() const;

private:
    ItemType type; ///< Represent the item type (CHERRY, ORANGE, ...)
    uint32 score;   ///< Represent the score associate to the type
};

/*!
** \brief Equality operator
** Test if 2 item are equal
**
** \param i1 The first item
** \param i2 The second item
** \return true if items are equal
*/
bool operator==(const Item & i1, const Item & i2);

/*!
** \brief Inequality operator
** Test if 2 item are not equal
**
** \param i1 The first item
** \param i2 The second item
** \return true if items are not equal
*/    
bool operator!=(const Item & i1, const Item & i2);

} // namespace pm

#endif // PACMAN_ITEM_HPP
