#ifndef PACMAN_ITEM_TYPE_HPP
#define PACMAN_ITEM_TYPE_HPP

namespace pm
{

/*!
** \enum ItemType
** \brief Represent the differents item type
*/
enum ItemType
{
    NONE,         ///< Nothing
    DOT,          ///< Dot
    POWER_PELLET, ///< Power pellet
    CHERRY,       ///< Cherry
    STRAWBERRY,   ///< Strawberry
    ORANGE,       ///< Orange
    APPLE,        ///< Apple
    MELON,        ///< Melon
    GALBOSS,      ///< Galboss
    BELL,         ///< Bell
    KEY,          ///< Key
    FRUIT         ///< Indicate that the item is a fruit
};

} // namespace pm

#endif // PACMAN_ITEM_TYPE_HPP
