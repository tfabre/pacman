#ifndef PACMAN_ITEM_FACTORY_HPP
#define PACMAN_ITEM_FACTORY_HPP

#include <map>
#include "pacman/model/item/Item.hpp"
#include "pacman/model/item/ItemType.hpp"

#define NONE_VALUE 0
#define DOT_VALUE 10
#define POWER_PELLET_VALUE 50
#define CHERRY_VALUE 100
#define STRAWBERRY_VALUE 300
#define ORANGE_VALUE 500
#define APPLE_VALUE 700
#define MELON_VALUE 1000
#define GALBOSS_VALUE 2000
#define BELL_VALUE 3000
#define KEY_VALUE 5000

namespace pm
{

/*!
** \brief Construct Item with the good type and associate score
*/
class ItemFactory
{
public:
    /*!
    ** \brief Give an item depending on the item type given in parameter
    **
    ** \param item Item type we want
    ** \return The item with the associate score
    */
    static Item getItem(const ItemType & it);

private:
    ItemFactory(); ///< Default private constructor
    static std::map<ItemType, Item> items; ///< Association between type & items
};
    
} // namespace pm

#endif // PACMAN_ITEM_FACTORY_HPP
