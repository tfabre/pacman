#ifndef PACMAN_GAME_FACTORY_HPP
#define PACMAN_GAME_FACTORY_HPP

#include "pacman/model/GameState.hpp"
#include "pacman/model/map/Level.hpp"
#include "pacman/model/map/TileMap.hpp"
#include "pacman/model/character/Pacman.hpp"

namespace pm
{

/*!
** \brief Factory to simplify Game model creation
*/
class GameFactory
{
public:
    /*!
    ** \brief Return a pacman instance
    **
    ** \return A pacman instance
    */
    static Pacman* getPacman();

    /*!
    ** \brief Return the tile map
    **
    ** \return Tile map
    */
    static TileMap* getTileMap();

    /*!
    ** \brief Return the game state
    **
    ** \return Tile map
    */
    static GameState* getGameState();

    /*!
    ** \brief Return path finding instance class
    **
    ** \return Path finding instance class
    */
    static PathFinding* getPathFinding();

    /*!
    ** \brief Return level instance class
    **
    ** \return Level instance
    */
    static Level* getLevel();

    /*!
    ** \brief Destroy all game model element and the factory
    **
    ** \attention This method must be called at the end of play session
    */
    static void deleteGameModel();

private:
    GameFactory();  ///< Default private constructor
    ~GameFactory(); ///< Default private destructor

    static GameFactory*& getFactory(); ///< Give reference to factory instance
    static GameFactory* factory; ///< Factory instance

    GameState* state; ///< Game state
    Pacman* pacman;   ///< Pacman
    TileMap* map;     ///< Pacman map
    Level* level;     ///< Pacman level loader
    PathFinding* pathFinding; ///< PathFinding
};

}

#endif // PACMAN_GAME_FACTORY_HPP
