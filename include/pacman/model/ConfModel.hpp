#ifndef PACMAN_CONF_MODEL_HPP
#define PACMAN_CONF_MODEL_HPP

#include <string>
#include "pacman/model/character/Direction.hpp"
#include "pacman/util/types.hpp"

namespace pm
{

/*!
** \brief Contains values for view configuration (global variables, etc)
*/
class ConfModel
{
public:
    /*!
    ** \brief represent paths to assets
    */
    static class PATH
    {
    public:
        static const std::string ASSET;      ///< Asset folder
        static const std::string ASSET_MAP;  ///< Map folder
    } path;
    
    /*!
    ** \brief Represent resource file
    */
    static class FILE
    {
    public:
        static const std::string FIRST_LEVEL; ///< Path to the first level
        static const std::string SCORE;       ///< Path to score file
        static const std::string DEFAULT_MAP_EDITOR_LEVEL; ///< Path to the default mapEditor level
    } file;

    /*!
    ** \brief Represent various constante used in the model
    */
    static class CONST
    {
    public:
        static const uint32 POINTS_FOR_EXTRA_LIFE{10000};
        ///< Number of point needed to gain an extra life 
        static const uint32 ROUND_VALUE{1000};
        ///< Round value for pacman angle move
        static const uint32 ROUND_DECREASE{1 / ROUND_VALUE};
        ///< Round to decrease for pacman angle move
        static const float FRUIT_APPEAR_DURATION;
        ///< fruit appear on the maze duration
        static const uint64 GHOST_VALUE{100};
        ///< Base score when ghost is eaten
        static const uint32 BEST_SCORE_TO_SAVE{10}; ///< Number of score to save
    } const_;

    /*!
    ** \brief Global constants properties for pacman
    */
    static class PACMAN
    {
    public:
        static const float DEFAULT_SIZE;        ///< Pacman default size
        static const float X_ORIGINAL_POSITION; ///< Pacman original X position
        static const float Y_ORIGINAL_POSITION; ///< Pacman original Y position
        static const float MIN_SPEED;           ///< Minimum speed value
        static const float DEFAULT_SPEED;       ///< Pacman default speed
        static const float SPEED_SUP;           ///< Speed greater than default
        static const float MAX_SPEED;           ///< Max speed
        static const Direction DEFAULT_DIRECTION; ///< Pacman default direction
    } pacman;

    /*!
    ** \brief Global constants proterty for ghosts
    */
    static class GHOST
    {
    public:
        static const float DEFAULT_SIZE;    ///< Ghost default size
        static const float LEAK_SPEED;      ///< Ghost speed on leak
        static const float DEFAULT_SPEED;   ///< Ghost default speed
        static const float BACK_HOME_SPEED; ///< Ghost speed on back homing
        static const float POWER_PELLET_DURATION;
        ///< Power pellet effect duration
        static const float PATROLLING_DURATION; ///< Patrolling duration
        static const float CHASING_DURATION;    ///< Chasing duration

    } ghost;

private:
    ConfModel();                   ///< Private constructor for no instanciation
    ConfModel(const ConfModel&);            ///< Priavte copy constructor
    ConfModel& operator=(const ConfModel&); ///< Private copy operator
};

}

#endif // PACMAN_CONF_MODEL_HPP
