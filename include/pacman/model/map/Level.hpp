#ifndef PACMAN_LEVEL_HPP
#define PACMAN_LEVEL_HPP

#include <gtest/gtest.h>

#include <fstream>
#include <string>
#include "pacman/util/Event.hpp"
#include "pacman/util/Observer.hpp"
#include "pacman/util/Vector2.hpp"
#include "pacman/model/map/TileMap.hpp"
#include "pacman/model/item/ItemFactory.hpp"
#include "pacman/model/ConfModel.hpp"

#define PACMAN_NUMBER_FOR_FILE 10
#define FRUIT_NUMBER_FOR_FILE 11
namespace pm
{

class TileMap;

/*!
** \brief Level is a level reader for TileMap
*/
class Level: public Observer
{
public:
    /*!
    ** \brief Construct a new Level reader
    **
    ** \param filename The file name where level is stored
    ** \param tilemap The tile map associate with the level
    ** \param levelNum The levelNumber
    */
    Level(const std::string & filename, TileMap* tm, const uint32& levelNum=1);

    /*!
    ** \brief Destroy a level
    */
    ~Level();

    void actualisate(const Event & event) override;

    /*!
    ** \brief Read the next level and set the new map information to tile map
    */
    void readNextLevel();

    /*!
    ** \brief Return specific fruit type depending on level number
    **
    ** \return Fruit type
    */
    ItemType getFruitTypeInLevel() const;

    uint32 getLevelNumber() const;

    static void saveNewLevel(const TileMap *tm);

    /*!
    ** \brief Reload file and set new level
    */
    void resynch();

protected:
    /*!
    ** \brief Set the new tile map level on tile map
    */
    void setNextTileMapLevel();

    /*!
    ** \brief Read the level information in the file, fill the map limits and
    **        fill mapRead attribute with map information
    */
    void readLevel();

    // TEST
    friend class LevelTest;
    FRIEND_TEST(LevelTest, set_next_tile_map_level_test);
    FRIEND_TEST(LevelTest, read_level_test);

private:
    std::ifstream file;  ///< the file where the level informations are stored
    TileMap* tilemap;    ///< the tile map associated to the level
    std::string mapRead; ///< map read from file
    Vector2<uint32> mapLimit; ///< map limits in x and y

    uint32 levelNumber; ///< Level number
};

}

#endif // PACMAN_LEVEL_HPP
