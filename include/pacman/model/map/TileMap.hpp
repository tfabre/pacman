#ifndef PACMAN_TILE_MAP_HPP
#define PACMAN_TILE_MAP_HPP

#include <string>
#include "pacman/util/Subject.hpp"
#include "pacman/util/Timer.hpp"
#include "pacman/util/Vector2.hpp"
#include "pacman/util/Random.hpp"
#include "pacman/model/item/ItemType.hpp"
#include "pacman/model/item/ItemFactory.hpp"
#include "pacman/model/map/TileType.hpp"
#include "pacman/model/map/Level.hpp"

namespace pm
{

class Level;

/*!
** \brief Contains informations about the current game map
*/
class TileMap: public Subject
{
public:
    TileMap();
    
    /*!
    ** \brief Construct the map from the string model
    **
    ** \param xlimit The horizontal tile number
    ** \param ylimit The vertical tile number
    ** \param map The map in string format
    ** \param level A reference to level
    */
    TileMap(const uint32 & xlimit, const uint32 & ylimit,
            const std::string & map, const Level* const level);

    /*!
    ** \brief Destroy properly the map
    */
    ~TileMap();

    /*!
    ** \brief Provides the tile on the indicated position
    **
    ** \brief position The position where the tile we want is
    ** \return The tile type
    */
    TileType getTile(const Vector2f & position) const;

    /*!
    ** \brief Return the item that is at the given position
    **
    ** \param position The position where the item should be
    ** \return The item at the given position
    */
    Item getItem(const Vector2f & position);

    /*!
    ** \brief Remove the item on map at the given position
    **
    ** \param position The position where the item should be
    */
    void eatItem(const Vector2f& position);

        /*!
    ** \brief Return the square where enemies start
    **
    ** \return The enemies' start square
    */
    Vector2<float> getGhostHomeSquare() const;

    /*!
    ** \brief Return a random square from the map
    **
    ** \return A random square from the map
    */
    Vector2<float> getRandomTile( TileType type, uint32 xMin, uint32 xMax,
                                  uint32 yMin, uint32 yMax) const;

    /*!
    ** \brief Return the distance between pos1 and pos2.
    **
    ** \return The distance between two points
    */
    uint32 getDistance( Vector2f pos1, Vector2f pos2 ) const;

    /*!
    ** \brief Return the horizontal tile number
    **
    ** \return The horizontal tile number
    */
    uint32 getXLimit() const;

    /*!
    ** \brief Return the vertical tile number
    **
    ** \return The vertical tile number
    */
    uint32 getYLimit() const;

    /*!
    ** \brief Change the map
    **
    ** \param xlim The new X limit
    ** \param ylim The new Y limit
    ** \param map The map in string format
    */
    void setNewMap(const uint32 & xlim, const uint32 & ylim,
                   const std::string & map);

    /*!
    ** \brief Set new level to tileMap
    **
    ** \param level Level to set
    */
    void setLevelManager(const Level* const level);

    /*!
    ** \brief Return true if the fruit is visible on the map, false otherwise,
    **        and notify when the fruit disapear
    **
    ** \return true if fruit is visible on the map, false otherwise
    */
    bool isFruitVisible();

    /*!
    ** \brief Pause fruit apparition
    **
    ** \note pause the timer
    */
    void pauseFruitAppearing();

    /*!
    ** \brief Resume fruit apparition
    **
    ** \note resume the timer
    */
    void resumeFruitAppearing();

    /*!
    ** \brief Notify that fruit has been eaten
    */
    void fruitEaten();

    /*!
    ** \brief Return pacman coordinate
    **
    ** \return Pacman coordinate
    */
    Vector2f getPacmanCoordinate() const;

    void insertWallAt(const uint8 x, const uint8 y);

    void insertElementAt(const uint8 x, const uint8 y, const uint8 elem);

    void insertGhostDoorAt(const uint8 x, const uint8 y);

    const Level* getLevel() const;

    Vector2<uint32> getElementAt(uint32 x, uint32 y) const;

private:
    /*!
    ** \brief Fill the map depending on map string formated parameter
    **
    ** \param map The string map format
    */
    void fillMap(const std::string & map);

    /*!
    ** \brief Return the Item code depending on the string map format code
    **
    ** \param tile The current code
    ** \return The code associate to the string format code
    */
    uint8 getItemValue(const char & tile) const;

    /*!
    ** \brief Return the tile map code corresponding to the kind of ground
    **        depending on the kind of tile given
    **
    ** \param tile The code for the ground type
    ** \return The for the tile map corresponding to the ground type
    */
    uint8 getGroundValue(const char & tile) const;

    /*!
    ** \brief Allocate a new map depending on limit
    */
    void allocateNewMap();

    /*!
    ** \brief Destroy properly map
    */
    void destroyCurrentMap();

    /*!
    ** \brief set fruitActive to false and notify fruit that fruit has disapear
    */
    void fruitDisappear();

    uint8*** map; ///< The tile map

    Vector2<uint32> limit; ///< The tile number horizontally(x) & vertically(y)
    uint32 itemCounter;    ///< Count items on the map
    uint32 itemAcc;        ///< Count each dot eaten to trigger fruit appearing
    bool fruitActivate;    ///< Is fruit appearing is activated
    Timer timer;           ///< Timer for fruit appear behavior

    const Level* level; ///< Reference to Level

    Vector2f ghostHomeSquare; ///< The start square of enemies
    Vector2f pacmanPos;
};

}

#endif // PACMAN_TILE_MAP_HPP
