#ifndef PACMAN_TILE_TYPE_HPP
#define PACMAN_TILE_TYPE_HPP

namespace pm
{

enum TileType
{
    WALL,
    GROUND,
    GHOST_DOOR,
    GHOST_HOME
};

}

#endif // PACMAN_TILE_TYPE_HPP
