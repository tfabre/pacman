#ifndef PACMAN_GAME_STATE_HPP
#define PACMAN_GAME_STATE_HPP

#include "pacman/util/Event.hpp"
#include "pacman/util/Observer.hpp"
#include "pacman/util/types.hpp"
#include "pacman/model/character/Pacman.hpp"
#include "pacman/model/character/enemy/Ghost.hpp"
#include "pacman/model/map/Level.hpp"
#include "pacman/model/ConfModel.hpp"

#include <list>
#include <tuple>
#include <vector>
#include <fstream>

namespace pm
{

/*!
** \brief Game state (point and life)
*/
class GameState: public Observer
{
public:
    /*!
    ** \brief Construct game state
    **
    ** \param pacman Reference to pacman (register as observer)
    ** \param level Reference to level
    ** \param life Life number (default = 2)
    ** \param score Score (default = 0)
    */
    GameState(Pacman* const pacman, const Level* const level,
              const uint8& life=2, const uint64& score=0);

    /*!
    ** \brief Return life number
    **
    ** \return Life number
    */
    uint8 getLifeNumber() const;

    /*!
    ** \brief Return current level number
    **
    ** \return Current level number
    */
    uint32 getLevelNumber() const;

    /*!
    ** \brief Return the score
    **
    ** \return Score
    */
    uint64 getScore() const;

    /*!
    ** \brief Add ghost to enemy list
    */
    void addGhost(Ghost* const ghost);

    /*!
    ** \brief Update score and life depending on event given
    **
    ** \param event Event that can change game state
    */
    virtual void actualisate(const Event& event) override;

    /*!
    ** \brief Write best score in save file
    */
    void writeScore();

    /*!
    ** \brief Set current score in the best score list
    **
    ** \return If the score has been emplaced
    */
    bool emplaceScore();

    /*!
    ** \brief Return current score line (score + date)
    **
    ** \return The current ScoreLine
    */
    ScoreLine getScoreToSave() const;

    /*!
    ** \brief Return best score list
    ** 
    ** \return The list of the best scores
    */
    std::list<ScoreLine> getBestScoreList() const;

    /*!
    ** \brief Return eaten ghost score
    **
    ** \return eaten ghost score
    */
    uint64 getEatenGhostScore() const;

protected:
    /*!
    ** \brief Convert binary date read from file to TimePoint
    **
    ** \param file File to read to get TimePoint
    ** \return The TimePoint read
    */
    static TimePoint extractDate(std::ifstream& file);

    /*!
    ** \brief Fill bestScore array with value found in save file
    */
    void loadScore();

private:
    uint8 life;           ///< Life number
    uint64 score;         ///< Score
    uint8 ghostEat;       ///< Number of ghost eat on power pellet effect

    Pacman* const pacman; ///< Reference to pacman
    const Level* const level;   ///< Reference to current level

    std::vector<Ghost*> enemies; ///< Enemy list
    std::list<ScoreLine> bestScore; ///< Array of best score
};

}

#endif // PACMAN_GAME_STATE_HPP
