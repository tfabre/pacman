#! /bin/bash

mkdir gtest
cd gtest/
git clone https://github.com/google/googletest.git
cd googletest/googletest
cmake .
make
cp -r include/gtest /usr/local/include
cp lib*.a /usr/local/lib
