git clone https://github.com/SFML/SFML.git
apt-get --yes --force-yes install cmake libfreetype6-dev libjpeg62-turbo-dev libX11-dev libxrandr-dev libxcb1-dev libx11-xcb-dev libxcb-randr0-dev libxcb-image0-dev libgl1-mesa-dev libflac-dev libogg-dev libvorbis-dev libvorbisenc2 libvorbisfile3 libopenal-dev libudev-dev
cd SFML
mkdir build
cmake .
make
make install
ldconfig -v
