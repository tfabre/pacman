#include "pacman/view/Play.hpp"
#include "pacman/view/ConfView.hpp"
#include "pacman/controller/PlayCtrl.hpp"
#include "pacman/model/GameFactory.hpp"
#include "pacman/model/character/enemy/GhostFactory.hpp"

namespace pm
{

Play::Play(Game* const parent):
    parent(parent), pacman(), map(),
    blinky(GhostFactory::getNewShadow(Vector2f(13.5, 11.5)), sf::Color::Red),
    pinky(GhostFactory::getNewSpeedy(Vector2f(13.5, 14.5)),
          sf::Color(255, 184, 255)),
    inky(GhostFactory::getNewBashful(Vector2f(11.5, 14.5), blinky.getGhost()),
         sf::Color::Cyan),
    clyde(GhostFactory::getNewPokey(Vector2f(15.5, 14.5)),
          sf::Color(255, 184, 81)),
    lastDirection{LEFT}, directionChanged{true}, paused{false}
{
    pacfont.loadFromFile(ConfView::FILE::PACMAN_FONT_NUM);

    scoreText = sf::Text("00", pacfont, ConfView::PACVIEW::TILE_WIDTH);
    scoreText.setPosition(5 * ConfView::PACVIEW::TILE_WIDTH,
                          ConfView::PACVIEW::TILE_HEIGHT);
    pauseText = sf::Text("Pause", pacfont, 180);
    pauseText.setFillColor(sf::Color::Yellow);
    pauseText.setPosition(ConfView::WINDOW::DEFAULT_WIDTH / 2 -
                      pauseText.getLocalBounds().width / 2,
                      ConfView::WINDOW::DEFAULT_WIDTH / 2 -
                      pauseText.getLocalBounds().height / 2);

    const uint32 texTileWidth = ConfView::PACVIEW::TEXTURE_TILE_WIDTH;
    const uint32 texTileHeight = ConfView::PACVIEW::TEXTURE_TILE_HEIGHT;

    const float fTexTileWidth = static_cast<float>(texTileWidth);
    const float fTileWidth = static_cast<float>(ConfView::PACVIEW::TILE_WIDTH);
    const float fTexTileHeight = static_cast<float>(texTileHeight);
    const float fTileHeight =static_cast<float>(ConfView::PACVIEW::TILE_HEIGHT);

    texture.loadFromFile(ConfView::FILE::LIFE_TEXTURE);
    lifeSprite = sf::Sprite(texture, sf::IntRect(0, 0,
                            ConfView::PACVIEW::TEXTURE_TILE_WIDTH,
                            ConfView::PACVIEW::TEXTURE_TILE_HEIGHT));
    lifeSprite.setScale(fTileWidth / fTexTileWidth, fTileHeight/fTexTileHeight);
    lifeSprite.setPosition(12 * ConfView::PACVIEW::TILE_WIDTH,
                           ConfView::PACVIEW::TILE_HEIGHT);

    uint8 lifenumber = GameFactory::getGameState()->getLifeNumber();
    lifeText = sf::Text(std::to_string(lifenumber), pacfont, 30);
    lifeText.setPosition(14 * ConfView::PACVIEW::TILE_WIDTH,
                         ConfView::PACVIEW::TILE_HEIGHT / 2);

    addController(new PlayCtrl(this));
    pacman.getPacman()->addObserver(*this);
    GameFactory::getTileMap()->addObserver(*this);
}

void Play::drawView()
{
    parent->setCurrentMusic(ConfView::FILE::PLAY_MUSIC);

    map.drawMap();
    if (!directionChanged)
        changePacmanDirection();
    pacman.drawPacman();
    blinky.drawGhost();
    pinky.drawGhost();
    inky.drawGhost();
    clyde.drawGhost();
    const uint64 score = GameFactory::getGameState()->getScore();
    scoreText.setString(std::to_string(score));
    lifeText.setString(std::to_string(GameFactory::getGameState()->
                                      getLifeNumber()));
    getWindow().draw(*this);
}

void Play::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(scoreText, states);
    target.draw(map, states);
    target.draw(pacman, states);
    target.draw(blinky, states);
    target.draw(pinky, states);
    target.draw(inky, states);
    target.draw(clyde, states);
    target.draw(lifeSprite, states);
    target.draw(lifeText);

    if (paused)
        target.draw(pauseText);
}

void Play::changePacmanDirection(const Direction& direction)
{
    lastDirection = direction;
    changePacmanDirection();
}

void Play::changePacmanDirection()
{
    directionChanged = pacman.getPacman()->changeDirection(lastDirection);
}

void Play::actualisate(const Event& event)
{
    switch (event)
    {
    case EAT_ITEM:
        map.removeItem(pacman.getPacman()->getPosition());
        break;
    case END_OF_LEVEL:
        map.loadMap();
        break;
    case FRUIT_APPEAR:
        map.drawFruit();
        break;
    case FRUIT_EATEN:
        map.displayScore();
    case FRUIT_DISAPPEAR:
        map.removeFruit();
        break;
    default: break;
    }
}

bool Play::isPaused() const
{
    return this->paused;
}

void Play::pause()
{
    this->map.setPause(true);
    this->pacman.setPause(true);
    this->paused = true;
    this->blinky.pause();
    this->pinky.pause();
    this->inky.pause();
    this->clyde.pause();
}

void Play::resume()
{
    this->map.setPause(false);
    this->pacman.setPause(false);
    this->paused = false;
    this->blinky.resume();
    this->pinky.resume();
    this->inky.resume();
    this->clyde.resume();
}

Game* Play::getParent() const
{
    return this->parent;
}

}
