#include "pacman/view/PacmanView.hpp"
#include "pacman/view/ConfView.hpp"
#include "pacman/model/GameFactory.hpp"
#include <cmath>

namespace pm
{
    
PacmanView::PacmanView(): pacman{GameFactory::getPacman()}, texture{},
    paused{false}
{
    const uint32 texTileWidth = ConfView::PACVIEW::TEXTURE_TILE_WIDTH;
    const uint32 texTileHeight = ConfView::PACVIEW::TEXTURE_TILE_HEIGHT;

    const float fTexTileWidth = static_cast<float>(texTileWidth);
    const float fTileWidth = static_cast<float>(ConfView::PACVIEW::TILE_WIDTH);
    const float fTexTileHeight = static_cast<float>(texTileHeight);
    const float fTileHeight =static_cast<float>(ConfView::PACVIEW::TILE_HEIGHT);

    const Vector2f pacpos = pacman->getOriginalPosition();
    const uint32 posX = ConfView::PACVIEW::TILE_WIDTH * pacpos.x;
    const uint32 posY = ConfView::PACVIEW::TILE_HEIGHT * pacpos.y;

    texture.loadFromFile(ConfView::FILE::PACMAN_TEXTURE);
    sprite = sf::Sprite(texture, sf::IntRect(0,0, texTileWidth, texTileHeight));
    sprite.setOrigin(texTileWidth / 2, texTileHeight / 2);
    sprite.setScale(fTileWidth / fTexTileWidth, fTileHeight / fTexTileHeight);
    sprite.setPosition(sf::Vector2f(posX, posY+ConfView::PACVIEW::MENU_HEIGHT));
}

PacmanView::~PacmanView()
{}

void PacmanView::changePacmanOrientation()
{
    const Direction pacdir = pacman->getCurrentDirection();
    switch (pacdir)
    {
    case RIGHT:
        sprite.setRotation(0);
        break;
    case LEFT:
        sprite.setRotation(180);
        break;
    case BOTTOM:
        sprite.setRotation(90);
        break;
    case TOP:
        sprite.setRotation(-90);
        break;
    default: break;
    }
}

void PacmanView::animatePacman()
{
    const sf::Time t1 = clock.getElapsedTime();
    if (t1.asMilliseconds() <= 100)
         sprite.setTextureRect(sf::IntRect(0, 0, 34, 34));

    else if (t1.asMilliseconds() <= 200)
         sprite.setTextureRect(sf::IntRect(34, 0, 34, 34));

    else if (t1.asMilliseconds() <= 300)
         sprite.setTextureRect(sf::IntRect(34 * 2, 0, 34, 34));

    else
        clock.restart();
}

void PacmanView::drawPacman()
{
    if (!paused)
        pacman->move();
    pm::Vector2f pos = pacman->getPosition();

    sprite.setPosition(pos.x * ConfView::PACVIEW::TILE_WIDTH,
                       pos.y * ConfView::PACVIEW::TILE_HEIGHT +
                       ConfView::GAME::MENU_GAP -
                       ConfView::PACVIEW::TILE_HEIGHT);

    changePacmanOrientation();
    animatePacman();
}

Pacman* PacmanView::getPacman() const
{
    return pacman;
}

void PacmanView::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(sprite, states);
}

void PacmanView::setPause(const bool& pause)
{
    this->paused = pause;
}

}
