#include "pacman/view/ConfView.hpp"

namespace pm
{

const std::string ConfView::PATH::ASSET{"asset/"};
const std::string ConfView::PATH::ASSET_FONT{ASSET + "font/"};
const std::string ConfView::PATH::ASSET_TEXT{ASSET + "texture/"};
const std::string ConfView::PATH::ASSET_MUSIC{ASSET + "music/"};

const std::string ConfView::FILE::PACMAN_FONT{PATH::ASSET_FONT +"pac-font.ttf"};
const std::string ConfView::FILE::PACMAN_FONT_NUM{PATH::ASSET_FONT +
        "crackman.ttf"};
const std::string ConfView::FILE::MAP_TEXTURE{PATH::ASSET_TEXT +
        "map_tile_34px.png"};
const std::string ConfView::FILE::PACMAN_TEXTURE{PATH::ASSET_TEXT +
        "pacman_34px.png"};
const std::string ConfView::FILE::GHOST_TEXTURE{PATH::ASSET_TEXT +
        "ghost_32px.png"};
const std::string ConfView::FILE::LIFE_TEXTURE{PATH::ASSET_TEXT +
        "34px_life.png"};

const std::string ConfView::FILE::MENU_MUSIC{PATH::ASSET_MUSIC +
        "menu_music.wav"};
const std::string ConfView::FILE::PLAY_MUSIC{PATH::ASSET_MUSIC +
        "play_music.wav"};
const std::string ConfView::FILE::EAT_DOT_SOUND{PATH::ASSET_MUSIC +
        "eat_dot_sound.wav"};
const std::string ConfView::FILE::EAT_ITEM_SOUND{PATH::ASSET_MUSIC +
        "eat_item_sound.wav"};
const std::string ConfView::FILE::DEATH_SOUND{PATH::ASSET_MUSIC + 
        "death_sound.wav"};

const std::string ConfView::TEXT::TITLE{"Pacman"};
const std::string ConfView::TEXT::PLAY{"Play"};
const std::string ConfView::TEXT::SCORE{"Score"};
const std::string ConfView::TEXT::MAP_EDITOR{"Map Editor"};
const std::string ConfView::TEXT::OPTION{"Options"};

const std::string ConfView::WINDOW::TITLE{"Pacman"};

}
