#include "pacman/view/View.hpp"
#include "pacman/view/ConfView.hpp"

namespace pm
{

sf::RenderWindow View::window(sf::VideoMode(ConfView::WINDOW::DEFAULT_WIDTH,
                                            ConfView::WINDOW::DEFAULT_HEIGHT),
                              ConfView::WINDOW::TITLE);

View::View() {}

View::~View()
{
    for (Controller* ctrl: controllers)
        delete ctrl;
}

void View::addController(Controller* const ctrl)
{
    controllers.push_back(ctrl);
}

void View::checkEvent(const sf::Event & event)
{
    for (Controller* ctrl: controllers)
        ctrl->checkEvent(event);
}

void View::checkExtraEvent()
{
    for (Controller* ctrl: controllers)
        ctrl->checkExtraEvent();
}

sf::RenderWindow& View::getWindow()
{
    return window;
}

}
