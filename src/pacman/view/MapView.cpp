#include "pacman/view/MapView.hpp"
#include "pacman/view/ConfView.hpp"

namespace
{

inline bool isNoneTile(const pm::TileType s[])
{
    return (s[0] == pm::WALL && s[1] == pm::WALL && s[2] == pm::WALL &&
            s[3] == pm::WALL && s[4] == pm::WALL &&
            s[5] == pm::WALL && s[6] == pm::WALL && s[7] == pm::WALL);
}

inline bool isTopLeftCornerTile(const pm::TileType s[])
{
    return ((s[0] == pm::WALL && s[1] == pm::WALL && s[2] == pm::WALL &&
             s[3] == pm::WALL && s[4] == pm::WALL &&
             s[5] == pm::WALL && s[6] == pm::WALL && s[7] == pm::GROUND) ||
            (s[0] == pm::GROUND && s[1] == pm::GROUND && s[2] == pm::GROUND &&
             s[3] == pm::GROUND && s[4] == pm::WALL &&
             s[5] == pm::GROUND && s[6] == pm::WALL && s[7] == pm::WALL) ||
            (s[0] == pm::GROUND && s[1] == pm::GROUND && s[2] == pm::GROUND &&
             s[3] == pm::GROUND && s[4] == pm::WALL &&
             s[5] == pm::GROUND && s[6] == pm::WALL && s[7] == pm::GROUND));
}

inline bool isTopRightCornerTile(const pm::TileType s[])
{
    return ((s[0] == pm::WALL && s[1] == pm::WALL && s[2] == pm::WALL &&
             s[3] == pm::WALL && s[4] == pm::WALL &&
             s[5] == pm::GROUND && s[6] == pm::WALL && s[7] == pm::WALL) ||
            (s[0] == pm::GROUND && s[1] == pm::GROUND && s[2] == pm::GROUND &&
             s[3] == pm::WALL && s[4] == pm::GROUND &&
             s[5] == pm::WALL && s[6] == pm::WALL && s[7] == pm::GROUND) ||
            (s[0] == pm::GROUND && s[1] == pm::GROUND && s[2] == pm::GROUND &&
             s[3] == pm::WALL && s[4] == pm::GROUND &&
             s[5] == pm::GROUND && s[6] == pm::WALL && s[7] == pm::GROUND));
}

inline bool isBotLeftCornerTile(const pm::TileType s[])
{
    return ((s[0] == pm::WALL && s[1] == pm::WALL && s[2] == pm::GROUND &&
             s[3] == pm::WALL && s[4] == pm::WALL &&
             s[5] == pm::WALL && s[6] == pm::WALL && s[7] == pm::WALL) ||
            (s[0] == pm::GROUND && s[1] == pm::WALL && s[2] == pm::WALL &&
             s[3] == pm::GROUND && s[4] == pm::WALL &&
             s[5] == pm::GROUND && s[6] == pm::GROUND && s[7] == pm::GROUND) ||
            (s[0] == pm::GROUND && s[1] == pm::WALL && s[2] == pm::GROUND &&
             s[3] == pm::GROUND && s[4] == pm::WALL &&
             s[5] == pm::GROUND && s[6] == pm::GROUND && s[7] == pm::GROUND));
}

inline bool isBotRightCornerTile(const pm::TileType s[])
{
    return ((s[0] == pm::GROUND && s[1] == pm::WALL && s[2] == pm::WALL &&
            s[3] == pm::WALL && s[4] == pm::WALL &&
             s[5] == pm::WALL && s[6] == pm::WALL && s[7] == pm::WALL) ||
            (s[0] == pm::WALL && s[1] == pm::WALL && s[2] == pm::GROUND &&
             s[3] == pm::WALL && s[4] == pm::GROUND &&
             s[5] == pm::GROUND && s[6] == pm::GROUND && s[7] == pm::GROUND) ||
            (s[0] == pm::GROUND && s[1] == pm::WALL && s[2] == pm::GROUND &&
             s[3] == pm::WALL && s[4] == pm::GROUND &&
             s[5] == pm::GROUND && s[6] == pm::GROUND && s[7] == pm::GROUND));
}

inline bool isVerticalTile(const pm::TileType s[])
{
    return s[1] == pm::WALL && s[6] == pm::WALL;
}

inline bool isHorizontalTile(const pm::TileType s[])
{
    return s[3] == pm::WALL && s[4] == pm::WALL;
}

}

namespace pm
{

MapView::MapView(TileMap* const tm): map{tm}, fruitCoordinates{},
    mapLimit{map->getXLimit(), map->getYLimit()}
{
    const uint32 xLim = mapLimit.x;
    const uint32 yLim = mapLimit.y;
    tileset.loadFromFile(ConfView::FILE::MAP_TEXTURE);
    vertices = sf::VertexArray(sf::Quads, 4 * xLim * yLim);
    transformedMap = new uint32*[xLim];
    for (uint32 i = 0; i < xLim; ++i)
        transformedMap[i] = new uint32[yLim];
    loadMap();

    font.loadFromFile(ConfView::FILE::PACMAN_FONT_NUM);
    score = sf::Text("", font, 20);
}

MapView::~MapView()
{
    const uint32 xLim = mapLimit.x;
    for (uint32 i = 0; i < xLim; ++i)
        delete[] transformedMap[i];
    delete[] transformedMap;
}

void MapView::drawMap()
{
    map->isFruitVisible();
}

void MapView::fillTexture(sf::Vertex* const quad, const int32& tu,
                          const int32& tv)
{
    quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
    quad[1].texCoords = sf::Vector2f((tu +1) * tileSize.x, tv * tileSize.y);
    quad[2].texCoords = sf::Vector2f((tu +1) * tileSize.x, (tv +1) *tileSize.y);
    quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv +1) * tileSize.y);
}

void MapView::fillColor(sf::Vertex* const quad, const sf::Color& color)
{
    quad[0].color = color;
    quad[1].color = color;
    quad[2].color = color;
    quad[3].color = color;
}

void MapView::loadMap()
{
    const uint32 width = mapLimit.x;
    const uint32 height = mapLimit.y;

    convertModelMapToDisplayMap();

    for (uint32 j = 0; j < height; ++j)
    {
        for (uint32 i = 0; i < width; ++i)
        {
            int tileNumber = transformedMap[i][j];

            int tu = tileNumber % (tileset.getSize().x / tileSize.x);
            int tv = tileNumber / (tileset.getSize().x / tileSize.x);

            sf::Vertex* const quad = &vertices[(i + j * width) * 4];

            const uint32 X = ConfView::PACVIEW::TILE_WIDTH;
            const uint32 Y = ConfView::PACVIEW::TILE_HEIGHT;
            const uint32 SHIFT = ConfView::PACVIEW::MENU_HEIGHT;
            quad[0].position = sf::Vector2f(i * X, j * Y + SHIFT);
            quad[1].position = sf::Vector2f((i+1) * X, j * Y + SHIFT);
            quad[2].position = sf::Vector2f((i+1) * X, (j+1) * Y + SHIFT);
            quad[3].position = sf::Vector2f(i * X, (j+1) * Y + SHIFT);

            fillTexture(quad, tu, tv);
        }
    }
}

void MapView::removeItem(const Vector2f& pos)
{
    const uint32 x = std::trunc(pos.x);
    const uint32 y = std::trunc(pos.y);
    const uint32 tu = NONE_TILE % (tileset.getSize().x / tileSize.x);
    const uint32 tv = NONE_TILE / (tileset.getSize().x / tileSize.x);
    sf::Vertex* const quad = &vertices[(x + y * mapLimit.x) * 4];
    fillTexture(quad, tu, tv);
}

void MapView::getSurrounding(const Vector2f& pos, TileType surround[]) const
{
    if (pos.x -1 >= 0 && pos.y -1 >= 0)
        surround[0] = map->getTile(Vector2f(pos.x -1, pos.y -1));

    if (pos.y -1 >= 0)
        surround[1] = map->getTile(Vector2f(pos.x, pos.y -1));

    if (pos.x +1 < mapLimit.x && pos.y -1 >= 0)
        surround[2] = map->getTile(Vector2f(pos.x +1, pos.y -1));

    if (pos.x -1 >= 0)
        surround[3] = map->getTile(Vector2f(pos.x -1, pos.y));

    if (pos.x +1 < mapLimit.x)
        surround[4] = map->getTile(Vector2f(pos.x +1, pos.y));

    if (pos.x -1 >= 0 && pos.y +1 < mapLimit.y)
        surround[5] = map->getTile(Vector2f(pos.x -1, pos.y +1));

    if (pos.y +1 < mapLimit.y)
        surround[6] = map->getTile(Vector2f(pos.x, pos.y +1));

    if (pos.x +1 < mapLimit.x && pos.y +1 < mapLimit.y)
        surround[7] = map->getTile(Vector2f(pos.x +1, pos.y +1));
}

void MapView::setWallType(const uint32& w, const uint32& h, const TileType s[])
{
    if (isNoneTile(s))
        transformedMap[w][h] = NONE_TILE;

    else if (isTopLeftCornerTile(s))
        transformedMap[w][h] = TOP_LEFT_CORNER_TILE;

    else if (isTopRightCornerTile(s))
        transformedMap[w][h] = TOP_RIGHT_CORNER_TILE;

    else if (isBotLeftCornerTile(s))
        transformedMap[w][h] = BOT_LEFT_CORNER_TILE;

    else if (isBotRightCornerTile(s))
        transformedMap[w][h] = BOT_RIGHT_CORNER_TILE;

    else if (isVerticalTile(s))
        transformedMap[w][h] = VERTICAL_WALL_TILE;

    else if (isHorizontalTile(s))
        transformedMap[w][h] = HORIZONTAL_WALL_TILE;

    else if (s[1] == WALL)
        transformedMap[w][h] = TOP_ENDED_WALL_TILE;
    else if (s[4] == WALL)
        transformedMap[w][h] = LEFT_ENDED_WALL_TILE;
    else if (s[3] == WALL)
        transformedMap[w][h] = RIGHT_ENDED_WALL_TILE;
    else if (s[6] == WALL)
        transformedMap[w][h] = BOT_ENDED_WALL_TILE;
}

void MapView::setItemType(uint32& w, const uint32& h, const Item& actualItem)
{
    if (actualItem == ItemFactory::getItem(DOT))
        transformedMap[w][h] = DOT_TILE;

    else if (actualItem == ItemFactory::getItem(POWER_PELLET))
        transformedMap[w][h] = POWER_PILLET_TILE;

    else
    {
        transformedMap[w][h] = NONE_TILE;
        if (actualItem == ItemFactory::getItem(FRUIT))
        {
            fruitCoordinates.push_back(
                std::make_tuple(Vector2<uint32>(w, h),
                                Vector2<uint32>(w+1, h)));
            transformedMap[++w][h] = NONE_TILE;
        }
    }
}

void MapView::convertModelMapToDisplayMap()
{
    const uint32 width = mapLimit.x;
    const uint32 height = mapLimit.y;

    for (uint32 h = 0; h < height; h++)
    {
        for (uint32 w = 0; w < width; w++)
        {
            TileType s[] = {WALL, WALL, WALL, WALL, WALL, WALL, WALL, WALL};

            getSurrounding(Vector2f(w, h), s);

            const TileType actualTile = map->getTile(Vector2f(w, h));
            const Item actualItem = map->getItem(Vector2f(w + .5,h + .5));

            if (actualTile == WALL)
                this->setWallType(w, h, s);

            else if (actualTile == GROUND)
                this->setItemType(w, h, actualItem);

            else if (actualTile == GHOST_DOOR)
                transformedMap[w][h] = GHOST_DOOR_TILE;
        }
    }
}

TileSetType MapView::getFruitTileSet() const
{
    TileSetType ret = NONE_TILE;
    switch (GameFactory::getLevel()->getFruitTypeInLevel())
    {
    case CHERRY:
        ret = HALF_CHERRY;
        break;
    case STRAWBERRY:
        ret = HALF_STRAWBERRY;
        break;
    case ORANGE:
        ret = HALF_ORANGE;
        break;
    case APPLE:
        ret = HALF_APPLE;
        break;
    case MELON:
        ret = HALF_MELON;
        break;
    case GALBOSS:
        ret = HALF_GALBOSS;
        break;
    case BELL:
        ret = HALF_BELL;
        break;
    case KEY:
        ret = HALF_KEY;
        break;
    default:
        ret = NONE_TILE;
        break;
    }
    return ret;
}

void MapView::drawFruit()
{
    for (const UIntVectorPair& fruit: fruitCoordinates)
    {
        const uint32 x = std::get<0>(fruit).x;
        const uint32 x1 = std::get<1>(fruit).x;
        const uint32 y = std::get<0>(fruit).y;
        const uint32 tu = getFruitTileSet()%(tileset.getSize().x / tileSize.x);
        const uint32 tu1 = (getFruitTileSet() +1) % (tileset.getSize().x /
                                                   tileSize.x);
        const uint32 tv = getFruitTileSet()/(tileset.getSize().x / tileSize.x);
        const uint32 tv1 = (getFruitTileSet() +1) / (tileset.getSize().x /
                                                    tileSize.x);
        sf::Vertex* const quad = &vertices[(x + y * mapLimit.x) * 4];
        sf::Vertex* const quad1 = &vertices[(x1 + y * mapLimit.x) * 4];

        fillTexture(quad, tu, tv);
        fillTexture(quad1, tu1, tv1);
    }
}

void MapView::displayScore()
{
    score.setString(std::to_string(
                        ItemFactory::getItem(
                            GameFactory::getLevel()->getFruitTypeInLevel()).
                        getScore()));
    score.setPosition((std::get<0>(fruitCoordinates[0]).x) *
                      ConfView::PACVIEW::TILE_WIDTH,
                      (std::get<0>(fruitCoordinates[0]).y + 3) *
                      ConfView::PACVIEW::TILE_HEIGHT);
    scoreClock.restart();
}

void MapView::removeFruit()
{
    for (const UIntVectorPair& fruit: fruitCoordinates)
    {
        const uint32 x = std::get<0>(fruit).x;
        const uint32 x1 = std::get<1>(fruit).x;
        const uint32 y = std::get<0>(fruit).y;
        const int32 tu = NONE_TILE % (tileset.getSize().x / tileSize.x);
        const int32 tv = NONE_TILE / (tileset.getSize().x / tileSize.x);
        sf::Vertex* const quad = &vertices[(x + y * mapLimit.x) * 4];
        sf::Vertex* const quad1 = &vertices[(x1 + y * mapLimit.x) * 4];
        fillTexture(quad, tu, tv);
        fillTexture(quad1, tu, tv);
    }
}

void MapView::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    states.texture = &tileset;
    target.draw(vertices, states);
    if (scoreClock.getElapsedTime().asMilliseconds() <= 1000)
        target.draw(score, states);
}

void MapView::setPause(const bool& pause)
{
    if (pause)
        map->pauseFruitAppearing();
    else
        map->resumeFruitAppearing();
}

}
