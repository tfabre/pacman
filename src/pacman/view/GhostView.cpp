#include "pacman/view/GhostView.hpp"
#include "pacman/view/ConfView.hpp"
#include "pacman/model/GameFactory.hpp"
#include <cmath>

namespace pm
{
    
GhostView::GhostView(Ghost* const ghost, const sf::Color& color): ghost{ghost},
    color{color}, clock{}, isPaused{false}, isDead{false}
{
    const Vector2f ghPos = ghost->getPosition();
    const uint32 tw = ConfView::PACVIEW::TILE_WIDTH * std::trunc(ghPos.x);
    const uint32 th = ConfView::PACVIEW::TILE_HEIGHT * std::trunc(ghPos.y);
    texture.loadFromFile(ConfView::FILE::GHOST_TEXTURE);
    texture.setSmooth(true);
    eye = sf::Sprite(texture, sf::IntRect(
                         ConfView::GHOSTVIEW::GHOST_TILE_WIDTH * 0, 0,
                         ConfView::GHOSTVIEW::GHOST_TILE_WIDTH,
                         ConfView::GHOSTVIEW::GHOST_TILE_HEIGHT));
    ghostSprite = sf::Sprite(texture, sf::IntRect(
                                 ConfView::GHOSTVIEW::GHOST_TILE_WIDTH * 4, 0,
                                 ConfView::GHOSTVIEW::GHOST_TILE_WIDTH,
                                 ConfView::GHOSTVIEW::GHOST_TILE_HEIGHT));
    ghostSprite.setColor(color);

    const float fTexTileWidth = static_cast<float>(
        ConfView::GHOSTVIEW::GHOST_TILE_WIDTH);
    const float fTileWidth = static_cast<float>(ConfView::PACVIEW::TILE_WIDTH)
        + 2;
    const float fTexTileHeight = static_cast<float>(
        ConfView::GHOSTVIEW::GHOST_TILE_HEIGHT);
    const float fTileHeight = static_cast<float>(
        ConfView::PACVIEW::TILE_HEIGHT) + 2;
    ghostSprite.setScale(fTileWidth/fTexTileWidth, fTileHeight/fTexTileHeight);
    ghostSprite.setPosition(sf::Vector2f(tw,th+ConfView::PACVIEW::MENU_HEIGHT));
    eye.setScale(fTileWidth / fTexTileWidth, fTileHeight / fTexTileHeight);
    eye.setPosition(sf::Vector2f(tw, th + ConfView::PACVIEW::MENU_HEIGHT));

    font.loadFromFile(ConfView::FILE::PACMAN_FONT_NUM);
    score = sf::Text("", font, 20);
}

GhostView::~GhostView()
{
    delete ghost;
}

void GhostView::pause()
{
    isPaused = true;
    ghost->pause();
}

void GhostView::resume()
{
    isPaused = false;
    ghost->resume();
}

void GhostView::setEyeDirectionAndColor(const Direction& dir)
{
    const uint32 tWidth = ConfView::GHOSTVIEW::GHOST_TILE_WIDTH;
    const uint32 tHeight = ConfView::GHOSTVIEW::GHOST_TILE_HEIGHT;

    if (ghost->getState() != EnemyState::LEAK)
    {
        eye.setColor(sf::Color::White);
        switch(dir)
        {
        case Direction::BOTTOM:
            eye.setTextureRect(sf::IntRect(ConfView::GHOSTVIEW::EYE_BOTTOM,
                                           0, tWidth, tHeight));
            break;
        case Direction::LEFT:
            eye.setTextureRect(sf::IntRect(ConfView::GHOSTVIEW::EYE_LEFT,
                                           0, tWidth, tHeight));
            break;
        case Direction::RIGHT:
            eye.setTextureRect(sf::IntRect(ConfView::GHOSTVIEW::EYE_RIGHT,
                                           0, tWidth, tHeight));
            break;
        default:
            eye.setTextureRect(sf::IntRect(ConfView::GHOSTVIEW::EYE_TOP,
                                           0, tWidth, tHeight));
            break;
        }
    }
    else
    {
        eye.setTextureRect(sf::IntRect(ConfView::GHOSTVIEW::DEAD_EYE, 0, tWidth,
                                       tHeight));
        eye.setColor(sf::Color(255, 184, 174));
    }
}

void GhostView::drawGhost()
{
    const uint32 tWidth = ConfView::GHOSTVIEW::GHOST_TILE_WIDTH;
    const uint32 tHeight = ConfView::GHOSTVIEW::GHOST_TILE_HEIGHT;

    if (!isPaused)
        ghost->move();
    pm::Vector2f pos = ghost->getPosition();
    ghostSprite.setPosition(pos.x * ConfView::PACVIEW::TILE_WIDTH - tWidth +
                            ConfView::PACVIEW::TILE_WIDTH,
                            pos.y * ConfView::PACVIEW::TILE_HEIGHT +
                            ConfView::GAME::MENU_GAP - tHeight);

    eye.setPosition(pos.x * ConfView::PACVIEW::TILE_WIDTH - tWidth +
                    ConfView::PACVIEW::TILE_WIDTH,
                    pos.y * ConfView::PACVIEW::TILE_HEIGHT +
                    ConfView::GAME::MENU_GAP - tHeight);

    Ghost* g = this->getGhost();

    setEyeDirectionAndColor(g->getDirection());

    if (ghost->getState() == EnemyState::BACK_HOME)
    {
        if (!isDead)
        {
            score.setPosition(ghostSprite.getPosition());
            textClock.restart();
            score.setString(std::to_string(GameFactory::getGameState()->
                                           getEatenGhostScore()));
        }
        isDead = true;
        ghostSprite.setColor(sf::Color(0, 0, 0, 0));
    }
    else
    {
        isDead = false;
        if (ghost->getState() == EnemyState::LEAK)
            ghostSprite.setColor(sf::Color::Blue);

        else
            ghostSprite.setColor(color);
    }

    const sf::Time t1 = clock.getElapsedTime();
    if (t1.asMilliseconds() <= 100)
        ghostSprite.setTextureRect(sf::IntRect(ConfView::GHOSTVIEW::GHOST1, 0,
                                               tWidth, tHeight));
    else if (t1.asMilliseconds() <= 200)
        ghostSprite.setTextureRect(sf::IntRect(ConfView::GHOSTVIEW::GHOST2, 0,
                                  tWidth, tHeight));
    else
        clock.restart();
}

Ghost* GhostView::getGhost() const
{
    return ghost;
}

void GhostView::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(ghostSprite, states);
    target.draw(eye, states);
    if (textClock.getElapsedTime().asMilliseconds() <= 1000)
    {
        target.draw(score, states);
    }
}

}
