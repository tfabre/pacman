#include "pacman/view/ScoreView.hpp"
#include "pacman/view/ConfView.hpp"
#include "pacman/util/Timer.hpp"
#include "pacman/model/GameFactory.hpp"
#include "pacman/controller/ScoreCtrl.hpp"

namespace
{
/*!
** \brief Compute Text final pos to set it in the middle of the window
**
** \param txt Text to compute position
** \return X final position
*/
constexpr float getFinalPos(const sf::Text & txt)
{
    return pm::ConfView::WINDOW::DEFAULT_WIDTH / 2 -
        txt.getGlobalBounds().width /2;
}

/*!
** \brief Animate text to move in to a point
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
** \param isX Indicate if we manipulate the x or the y coordinate value
** \param isLt Indicate if we make comparaison of lower than type or greater
**             than type between position and axis
*/
void moveText(sf::Text & text, const float & axis, const float & speed,
              const bool & isX, const bool & isLt)
{
    sf::Vector2f position = text.getPosition();
    float& coord = isX ? position.x : position.y;

    if ((isLt && coord < axis) || (!isLt && coord > axis))
    {
        coord += speed;
        text.setPosition(position);
    }
}

/*!
** \brief Down text to move in to a point from a top point
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
*/
void moveDownTextToPosition(sf::Text & text, const float & y,
                            const float & speed)
{
    moveText(text, y, speed, false, true);
}

/*!
** \brief Animate text to move in to a point from right to left
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
*/
void moveRightTextToPosition(sf::Text & text, const float & x,
                             const float & speed)
{
    moveText(text, x, speed, true, true);
}

}

namespace pm
{

std::string ScoreView::getScoreText() const
{
    std::string scores = "";
    int rank = 1;
    for (const ScoreLine& sl: GameFactory::getGameState()->getBestScoreList())
        scores += std::to_string(rank++) + " :\t" +
            std::to_string(std::get<0>(sl)) + "\t-\t" +
            Timer::timePointToString(std::get<1>(sl), "%d/%m/%y %T") + "\n";
    return scores;
}

ScoreView::ScoreView(Game* const parent): parent(parent)
{
    font.loadFromFile(ConfView::FILE::PACMAN_FONT_NUM);

    titleText = sf::Text(ConfView::TEXT::TITLE, font,
                         ConfView::TEXT::TITLE_SIZE);
    titleText.setFillColor(sf::Color::Yellow);
    titleText.setPosition(getFinalPos(titleText), -50);

    scoreText = sf::Text(getScoreText(), font, 30);
    scoreText.setPosition(0, ConfView::GAME::MENU_GAP +
                          ConfView::WINDOW::DEFAULT_WIDTH / 2 -
                          scoreText.getGlobalBounds().height / 2);
    addController(new ScoreCtrl(this));
}

void ScoreView::updateScoreText()
{
    scoreText.setString(getScoreText());
}

void ScoreView::drawView()
{
    constexpr float vertSpeed = 9.f;
    moveRightTextToPosition(this->scoreText, getFinalPos(scoreText), vertSpeed);
    moveDownTextToPosition(this->titleText, 0.f, 2.f);
    getWindow().draw(*this);
}

void ScoreView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(titleText, states);
    target.draw(scoreText, states);
}

Game* ScoreView::getParent() const
{
    return this->parent;
}

} // namespace pm

