#include "pacman/view/Game.hpp"
#include "pacman/view/Play.hpp"
#include "pacman/view/GameOverView.hpp"
#include "pacman/view/ScoreView.hpp"
#include "pacman/view/MapEditor.hpp"
#include "pacman/controller/GameCtrl.hpp"
#include "pacman/view/ConfView.hpp"

namespace
{
/// \todo Set these function for text manipulation global!
/*!
** \brief Compute Text final pos to set it in the middle of the window
**
** \param txt Text to compute position
** \return X final position
*/
constexpr float getFinalPos(const sf::Text & txt)
{
    return pm::ConfView::WINDOW::DEFAULT_WIDTH / 2 -
        txt.getGlobalBounds().width /2;
}

/*!
** \brief Animate text to move in to a point
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
** \param isX Indicate if we manipulate the x or the y coordinate value
** \param isLt Indicate if we make comparaison of lower than type or greater
**             than type between position and axis
*/
void moveText(sf::Text & text, const float & axis, const float & speed,
              const bool & isX, const bool & isLt)
{
    sf::Vector2f position = text.getPosition();
    float& coord = isX ? position.x : position.y;

    if ((isLt && coord < axis) || (!isLt && coord > axis))
    {
        coord += speed;
        text.setPosition(position);
    }
}

/*!
** \brief Down text to move in to a point from a top point
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
*/
void moveDownTextToPosition(sf::Text & text, const float & y,
                            const float & speed)
{
    moveText(text, y, speed, false, true);
}

/*!
** \brief Animate text to move in to a point from right to left
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
*/
void moveRightTextToPosition(sf::Text & text, const float & x,
                             const float & speed)
{
    moveText(text, x, speed, true, true);
}

/*!
** \brief Animate text to move in to a point from left to right
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
*/
void moveLeftTextToPosition(sf::Text & text, const float & x,
                            const float & speed)
{
    moveText(text, x, -speed, true, false);
}

}

namespace pm
{

Game::Game(): currentView(this), views{{
        {ViewType::MENU, this},
        {ViewType::PACMAN, new Play(this)},
        {ViewType::SCORES, new ScoreView(this)},
        {ViewType::MAP_EDITOR, new MapEditor(this)},
        {ViewType::GAME_OVER, new GameOverView(this)},}}
{
    getWindow().setFramerateLimit(ConfView::WINDOW::FRAMERATE_LIMITE);
    pacfont.loadFromFile(ConfView::FILE::PACMAN_FONT);

    titleText = sf::Text(ConfView::TEXT::TITLE, pacfont,
                         ConfView::TEXT::TITLE_SIZE);
    titleText.setFillColor(sf::Color::Yellow);
    titleText.setPosition(getFinalPos(titleText), -50);

    playText = sf::Text(ConfView::TEXT::PLAY, pacfont,
                        ConfView::TEXT::MENU_SIZE);
    playText.setPosition(0, ConfView::GAME::PLAY_Y);

    scoreText = sf::Text(ConfView::TEXT::SCORE, pacfont,
                         ConfView::TEXT::MENU_SIZE);
    scoreText.setPosition(ConfView::WINDOW::DEFAULT_WIDTH -
                          scoreText.getGlobalBounds().width,
                          ConfView::GAME::SCORE_Y);

    mapEditorText = sf::Text(ConfView::TEXT::MAP_EDITOR, pacfont,
                             ConfView::TEXT::MENU_SIZE);
    mapEditorText.setPosition(0, ConfView::GAME::MAP_EDITOR_Y);

    optionText = sf::Text(ConfView::TEXT::OPTION, pacfont,
                          ConfView::TEXT::MENU_SIZE);
    optionText.setPosition(ConfView::WINDOW::DEFAULT_WIDTH -
                           optionText.getGlobalBounds().width,
                           ConfView::GAME::OPTION_Y);

    addController(new GameCtrl(this));
}

Game::~Game()
{
    for (auto v: views)
        if (v.first != ViewType::MENU)
            delete v.second;
}

void Game::setCurrentView(const ViewType & type)
{
    if (type == ViewType::SCORES)
        reinterpret_cast<ScoreView*>(views.at(type))->updateScoreText();

    currentView = views.at(type);
}

void Game::setCurrentMusic(const std::string filename, bool loop, uint8 volume)
{
    if(filename == currentlyPlaying)
        return;

    music.stop();

    if(music.openFromFile(filename))
    {
        music.play();
        music.setLoop(loop);
        music.setVolume(volume);
        currentlyPlaying = filename;
    }
}

void Game::rebuildView(const ViewType& type)
{
    if (type == ViewType::PACMAN)
    {
        delete views.at(type);
        views.at(ViewType::PACMAN) = new Play(this);
    }
}

void Game::drawView()
{
    constexpr float vertSpeed = 9.f;
    moveRightTextToPosition(this->playText, getFinalPos(playText), vertSpeed);
    moveLeftTextToPosition(this->scoreText, getFinalPos(scoreText), vertSpeed);
    moveRightTextToPosition(this->mapEditorText, getFinalPos(mapEditorText),
                            vertSpeed);
    moveLeftTextToPosition(this->optionText, getFinalPos(optionText),vertSpeed);
    moveDownTextToPosition(this->titleText, 0.f, 2.f);
    getWindow().draw(*this);
}

void Game::run()
{
    setCurrentMusic(ConfView::FILE::MENU_MUSIC);

    while (getWindow().isOpen())
    {
        sf::Event event;
        while (getWindow().pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                music.stop();
                this->getWindow().close();
            }
            currentView->checkEvent(event);
        }
        currentView->checkExtraEvent();
        getWindow().clear(sf::Color::Black);
        currentView->drawView();
        getWindow().display();
    }
}

std::vector<sf::Text> Game::getMenuTexts() const
{
    return {this->playText, this->scoreText,
            this->mapEditorText, this->optionText};
}

void Game::focusText(const sf::Text & txt, const bool & focus)
{
    if (txt.getString() == playText.getString())
        playText.setFillColor((focus) ? sf::Color::Yellow : sf::Color::White);

    else if (txt.getString() == scoreText.getString())
        scoreText.setFillColor((focus) ? sf::Color::Yellow : sf::Color::White);

    else if (txt.getString() == mapEditorText.getString())
        mapEditorText.setFillColor((focus)?sf::Color::Yellow: sf::Color::White);

    else if (txt.getString() == optionText.getString())
        optionText.setFillColor((focus) ? sf::Color::Yellow : sf::Color::White);
}

void Game::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(titleText, states);
    target.draw(playText, states);
    target.draw(scoreText, states);
    target.draw(mapEditorText, states);
    target.draw(optionText, states);
}

} // namespace pm
