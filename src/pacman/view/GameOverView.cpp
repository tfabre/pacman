#include "pacman/view/GameOverView.hpp"
#include "pacman/view/ConfView.hpp"
#include "pacman/controller/GameOverCtrl.hpp"

namespace
{
/*!
** \brief Compute Text final pos to set it in the middle of the window
**
** \param txt Text to compute position
** \return X final position
*/
constexpr float getFinalPos(const sf::Text & txt)
{
    return pm::ConfView::WINDOW::DEFAULT_WIDTH / 2 -
        txt.getGlobalBounds().width /2;
}

/*!
** \brief Animate text to move in to a point
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
** \param isX Indicate if we manipulate the x or the y coordinate value
** \param isLt Indicate if we make comparaison of lower than type or greater
**             than type between position and axis
*/
void moveText(sf::Text & text, const float & axis, const float & speed,
              const bool & isX, const bool & isLt)
{
    sf::Vector2f position = text.getPosition();
    float& coord = isX ? position.x : position.y;

    if ((isLt && coord < axis) || (!isLt && coord > axis))
    {
        coord += speed;
        text.setPosition(position);
    }
}

/*!
** \brief Animate text to move in to a point from right to left
**
** \param text Text to move
** \param axis Axis (x or y)
** \param speed Animation speed
*/
void moveRightTextToPosition(sf::Text & text, const float & x,
                             const float & speed)
{
    moveText(text, x, speed, true, true);
}
    
}

namespace pm
{

GameOverView::GameOverView(Game* const parent): parent(parent)
{
    font.loadFromFile(ConfView::FILE::PACMAN_FONT_NUM);
    gameOver = sf::Text("Game Over", font, ConfView::TEXT::TITLE_SIZE);
    gameOver.setFillColor(sf::Color::Yellow);
    gameOver.setPosition(0, ConfView::WINDOW::DEFAULT_WIDTH / 2 -
                         gameOver.getGlobalBounds().height / 2);
    addController(new GameOverCtrl(this));
}

void GameOverView::drawView()
{
    constexpr float vertSpeed = 9.f;
    moveRightTextToPosition(this->gameOver, getFinalPos(gameOver), vertSpeed);
    getWindow().draw(*this);
}

void GameOverView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(gameOver, states);
}

Game* GameOverView::getParent() const
{
    return this->parent;
}

} // namespace pm
