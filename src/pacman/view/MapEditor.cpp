#include "pacman/view/MapEditor.hpp"
#include "pacman/view/ConfView.hpp"
#include "pacman/controller/MapEditorCtrl.hpp"
#include "pacman/model/GameFactory.hpp"

namespace pm
{

MapEditor::MapEditor(Game* const parent): parent(parent), texture{}
{
    const uint32 texTileWidth = ConfView::PACVIEW::TEXTURE_TILE_WIDTH;
    const uint32 texTileHeight = ConfView::PACVIEW::TEXTURE_TILE_HEIGHT;

    texture.loadFromFile(ConfView::FILE::MAP_TEXTURE);

    uint8 cpt = 0;

    addWallSprite = sf::Sprite(texture, sf::IntRect(5*texTileWidth ,0, texTileWidth, texTileHeight));
    addWallSprite.setPosition(texTileWidth*cpt++, 0);

    addNoneSprite = sf::Sprite(texture, sf::IntRect(0, 2*texTileHeight, texTileWidth, texTileHeight));
    addNoneSprite.setPosition(texTileWidth*cpt++, 0 );

    addDotSprite = sf::Sprite(texture, sf::IntRect(texTileWidth, texTileHeight*2, texTileWidth, texTileHeight));
    addDotSprite.setPosition(texTileWidth*cpt++, 0 );

    addPowerPelletSprite = sf::Sprite(texture, sf::IntRect(texTileWidth*2, texTileHeight*2, texTileWidth, texTileHeight));
    addPowerPelletSprite.setPosition(texTileWidth*cpt++, 0 );

    addGhostDoorSprite = sf::Sprite(texture, sf::IntRect(texTileWidth*3, texTileHeight*2, texTileWidth, texTileHeight));
    addGhostDoorSprite.setPosition(texTileWidth*cpt++, 0 );

    addPacmanPosShape = sf::CircleShape(texTileWidth/2);
    addPacmanPosShape.setPosition(texTileWidth*cpt++, 0);
    addPacmanPosShape.setFillColor(sf::Color(250, 250, 0));

    addFruitPosShape = sf::RectangleShape(sf::Vector2f(texTileWidth, texTileHeight));
    addFruitPosShape.setPosition(texTileWidth*cpt++, 0);
    addFruitPosShape.setFillColor(sf::Color(250, 0, 0));

    saveTileSprite = sf::RectangleShape(sf::Vector2f(texTileWidth, texTileHeight));
    saveTileSprite.setPosition(texTileWidth*cpt++, 0);

    std::ifstream file;
    file.open(ConfModel::FILE::DEFAULT_MAP_EDITOR_LEVEL);

    std::string mapEditorDefaultLevel;

    if(file && !file.eof())
    {
        std::getline(file, mapEditorDefaultLevel);
        file.close();
    }
    else
    {
        mapEditorDefaultLevel = std::string(28*31, '1');
    }

    tilemap = new TileMap(28, 31, mapEditorDefaultLevel, nullptr);


    map = new MapView(tilemap);

    marker = sf::RectangleShape(sf::Vector2f(ConfView::PACVIEW::TILE_WIDTH, ConfView::PACVIEW::TILE_HEIGHT));
    marker.setFillColor(sf::Color::Transparent);
    marker.setOutlineThickness(2);
    marker.setOutlineColor(sf::Color(250, 0, 0));
    actualisateMarkerPos(0, 0);

    addController(new MapEditorCtrl(this));
}

void MapEditor::actualisateMarkerPos(const uint8 x, const uint8 y)
{
    sf::Vector2i origin;
    origin.x = 0;
    origin.y = 3 * ConfView::PACVIEW::TILE_HEIGHT;

    marker.setPosition(origin.x + x * ConfView::PACVIEW::TILE_HEIGHT,
                       origin.y + y * ConfView::PACVIEW::TILE_WIDTH);
}

MapEditor::~MapEditor()
{
    delete tilemap;
    delete map;
}

void MapEditor::drawView()
{
    map->drawMap();
    map->loadMap();
    getWindow().draw(*this);
}

void MapEditor::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(*map, states);
    target.draw(marker, states);
    target.draw(addWallSprite, states);
    target.draw(addNoneSprite, states);
    target.draw(addDotSprite, states);
    target.draw(addPowerPelletSprite, states);
    target.draw(addGhostDoorSprite, states);
    target.draw(addPacmanPosShape, states);
    target.draw(addFruitPosShape, states);
    target.draw(saveTileSprite, states);

}

Game* MapEditor::getParent() const
{
    return this->parent;
}

} // namespace pm
