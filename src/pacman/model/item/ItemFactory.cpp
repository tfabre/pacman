#include "pacman/model/item/ItemFactory.hpp"

namespace pm
{

std::map<ItemType, Item> ItemFactory::items =
{
    {NONE         , Item(NONE, uint32(NONE_VALUE))},
    {DOT          , Item(DOT, uint32(DOT_VALUE))},
    {POWER_PELLET , Item(POWER_PELLET, uint32(POWER_PELLET_VALUE))},
    {CHERRY       , Item(CHERRY, uint32(CHERRY_VALUE))},
    {STRAWBERRY   , Item(STRAWBERRY, uint32(STRAWBERRY_VALUE))},
    {ORANGE       , Item(ORANGE, uint32(ORANGE_VALUE))},
    {APPLE        , Item(APPLE, uint32(APPLE_VALUE))},
    {MELON        , Item(MELON, uint32(MELON_VALUE))},
    {GALBOSS      , Item(GALBOSS, uint32(GALBOSS_VALUE))},
    {BELL         , Item(BELL, uint32(BELL_VALUE))},
    {KEY          , Item(KEY, uint32(KEY_VALUE))},
    {FRUIT        , Item(FRUIT, 0)}
};

Item ItemFactory::getItem(const ItemType & it)
{
    return items.at(it);
}

}
