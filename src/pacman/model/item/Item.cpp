#include "pacman/model/item/Item.hpp"

namespace pm
{

Item::Item(const ItemType & it, const uint32 & score): type(it), score(score) {}

ItemType Item::getType() const
{
    return this->type;
}

uint32 Item::getScore() const
{
    return this->score;
}


bool operator==(const Item & i1, const Item & i2)
{
    return i1.getScore() == i2.getScore() && i1.getType() == i2.getType();
}

bool operator!=(const Item & i1, const Item & i2)
{
    return !(i1 == i2);
}

} // namespace pm
