#include "pacman/model/map/Level.hpp"
#include <cstdlib>
#include <iostream>

namespace pm
{

Level::Level(const std::string & filename, TileMap* tm, const uint32& levelNum):
    tilemap(tm), mapRead{}, mapLimit{0, 0}, levelNumber{levelNum}
{
    this->tilemap->setLevelManager(this);
    file.open(filename);

    if (!file)
    {
        std::cerr << "[Error] Cannot read level file" << std::endl;
        std::exit(-1);
    }
}
    
Level::~Level()
{
    file.close();
}

void Level::actualisate(const Event & event)
{
    if (event == END_OF_LEVEL)
    {
        setNextTileMapLevel();
        this->levelNumber++;
    }
}

ItemType Level::getFruitTypeInLevel() const
{
    ItemType ret = NONE;
    switch (this->levelNumber)
    {
    case 1:
        ret = CHERRY;
        break;
    case 2:
        ret = STRAWBERRY;
        break;
    case 3: case 4:
        ret = ORANGE;
        break;
    case 5: case 6:
        ret = APPLE;
        break;
    case 7: case 8:
        ret = MELON;
        break;
    case 9: case 10:
        ret = GALBOSS;
        break;
    case 11: case 12:
        ret = BELL;
        break;
    default:
        ret = KEY;
        break;
    }
    return ret;
}

void Level::readNextLevel()
{
    setNextTileMapLevel();
}

void Level::setNextTileMapLevel()
{
    readLevel();
    tilemap->setNewMap(this->mapLimit.x, this->mapLimit.y, this->mapRead);
}
    
void Level::readLevel()
{
    if (!file.eof())
    {
        std::getline(file, this->mapRead);
        this->mapLimit.x = std::stoi(this->mapRead);

        std::getline(file, this->mapRead);
        this->mapLimit.y = std::stoi(this->mapRead);

        std::getline(file, this->mapRead);

        file.get();
    }
    if (!file.eof())
        file.unget();
}

uint32 Level::getLevelNumber() const
{
    return this->levelNumber;
}

void Level::saveNewLevel(const TileMap *tm)
{
    std::ofstream file;
    file.open(ConfModel::FILE::FIRST_LEVEL, std::ios::app);

    if (!file)
    {
        printf("%s\n", "fail loading file");
    }
    else
    {
        file << tm->getXLimit() << "\n";
        file << tm->getYLimit() << "\n";

        for (uint32 i = 0; i < tm->getYLimit(); i++)
        {
            for (uint32 y = 0; y < tm->getXLimit(); y++)
            {
                Vector2<uint32> t = tm->getElementAt(y, i);

                if(t.x == 0)
                    file << t.x;
                else if(t.x == 1)
                {
                    if( i <= tm->getGhostHomeSquare().y && i >= (tm->getGhostHomeSquare().y) - 3 &&
                        y <= tm->getGhostHomeSquare().x && y >= (tm->getGhostHomeSquare().x)- 6)
                        file << "-";
                    else if(t.y == 0 || t.y == 1 || t.y == 2) // none dot or power pellet
                        file << t.y+1; //+1 because 0 is wall in file
                    else if (t.y == PACMAN_NUMBER_FOR_FILE)
                        file << "P";
                    else if (t.y == FRUIT_NUMBER_FOR_FILE)
                        file << "F";
                }
                else if(t.x == 2)
                    file << "_";
            }
        }

        file << "\n";
        file.close();
    }

}

void Level::resynch()
{
    file.close();
    file.open(ConfModel::FILE::FIRST_LEVEL);
    setNextTileMapLevel();
}

} // namespace pm
