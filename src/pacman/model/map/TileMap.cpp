#include "pacman/model/map/TileMap.hpp"
#include "pacman/model/ConfModel.hpp"
#include <iostream>
#include <cmath>
#include <map>

namespace
{
    const std::map<const char, const pm::uint8> TILE_ITEM = {
        {'1', pm::uint8(pm::NONE)},
        {'2', pm::uint8(pm::DOT)},
        {'3', pm::uint8(pm::POWER_PELLET)},
        {'4', pm::uint8(pm::CHERRY)},
        {'5', pm::uint8(pm::STRAWBERRY)},
        {'6', pm::uint8(pm::ORANGE)},
        {'7', pm::uint8(pm::APPLE)},
        {'8', pm::uint8(pm::MELON)},
        {'9', pm::uint8(pm::GALBOSS)},
        {'B', pm::uint8(pm::BELL)},
        {'K', pm::uint8(pm::KEY)},
        {'F', pm::uint8(pm::FRUIT)}
    };

    const std::map<const char, const pm::uint8> TILE_TYPE = {
        {'0', 0},
        {'1', 1},
        {'_', 2},
        {'-', 3}
    };

    const pm::TileType TILE_MAP_TO_TILE_TYPE[] = {
        pm::WALL,
        pm::GROUND,
        pm::GHOST_DOOR,
        pm::GHOST_HOME
    };

bool isFruit(const pm::uint8& item)
{
    return item == pm::uint8(pm::FRUIT) || item == pm::uint8(pm::CHERRY) ||
        item == pm::uint8(pm::STRAWBERRY) || item == pm::uint8(pm::ORANGE) ||
        item == pm::uint8(pm::APPLE) || item == pm::uint8(pm::MELON) ||
        item == pm::uint8(pm::GALBOSS) || item == pm::uint8(pm::BELL) ||
        item == pm::uint8(pm::KEY);
}

}

namespace pm
{

void TileMap::insertWallAt(const uint8 x, const uint8 y)
{
    map[x][y][0] = 0;
    map[x][y][1] = 0;
}

void TileMap::insertElementAt(const uint8 x, const uint8 y, const uint8 elem)
{
    map[x][y][0] = 1;
    map[x][y][1] = elem;
}

void TileMap::insertGhostDoorAt(const uint8 x, const uint8 y)
{
    map[x][y][0] = 2;
    map[x][y][1] = 0;
}

Vector2<uint32> TileMap::getElementAt(uint32 x, uint32 y) const
{
    return Vector2<uint32>(map[x][y][0], map[x][y][1]);
}

uint8 TileMap::getItemValue(const char & tile) const
{
    auto search = TILE_ITEM.find(tile);
    return (search != TILE_ITEM.end()) ? search->second : 0;
}

uint8 TileMap::getGroundValue(const char & tile) const
{
    auto search = TILE_TYPE.find(tile);
    if (search != TILE_TYPE.end())
    {
        if (search->second == 3)
            return TILE_TYPE.at('1');
        return search->second;
    }
    return 1;
}

void TileMap::fillMap(const std::string & map)
{
    for (uint32 y{0}; y < this->limit.y; ++y)
    {
        for (uint32 x{0}; x < this->limit.x; ++x)
        {
            const char & tile = map[x + this->limit.x * y];
            uint8* z_map = this->map[x][y];

            z_map[0] = getGroundValue(tile);
            z_map[1] = getItemValue(tile);
            if (z_map[1] == TILE_ITEM.at('2') || z_map[1] == TILE_ITEM.at('3'))
                this->itemCounter++;
            if( tile == '-' )
            {
                this->ghostHomeSquare.x = x;
                this->ghostHomeSquare.y = y;
            }
            else if (tile == 'P' && map[x+1 + this->limit.x * y] == 'P')
            {
                this->pacmanPos.x = x + .5;
                this->pacmanPos.y = y + .5;
            }
        }
    }
}

void TileMap::allocateNewMap()
{
    this->map = new uint8**[limit.x];
    for (uint32 i{0}; i < this->limit.x; ++i)
    {
        this->map[i] = new uint8*[limit.y];
        for (uint32 j{0}; j < this->limit.y; ++j)
        {
            this->map[i][j] = new uint8[2];
            this->map[i][j][0] = 0;
            this->map[i][j][1] = 0;
        }
    }
}

void TileMap::destroyCurrentMap()
{
    for (uint32 i{0}; i < this->limit.x; ++i)
    {
        for (uint32 j{0}; j < this->limit.y; ++j)
        {
            delete[] this->map[i][j];
        }
        delete[] this->map[i];
    }
    delete[] this->map;

    this->map = nullptr;
}

TileMap::TileMap(): map(nullptr), limit(0.f, 0.f), itemCounter(0), itemAcc(0),
                    fruitActivate{false}, timer(), level(nullptr),
                    ghostHomeSquare{}, pacmanPos{}
{}

TileMap::TileMap(const uint32 & xlim, const uint32 & ylim,
                 const std::string & map, const Level* const level):
    limit(xlim, ylim), itemCounter(0), itemAcc(0), fruitActivate{false},
    timer(), level(level), ghostHomeSquare{}, pacmanPos{}
{
    this->allocateNewMap();
    this->fillMap(map);
}

TileMap::~TileMap()
{
    this->destroyCurrentMap();
}

TileType TileMap::getTile(const Vector2f & position) const
{
    int tile = this->map[(int)position.x][(int)position.y][0];
    return TILE_MAP_TO_TILE_TYPE[tile];
}

Item TileMap::getItem(const Vector2f & position)
{
    float x_intpart;
    float y_intpart;
    float x_fractpart = std::modf(position.x, &x_intpart);
    float y_fractpart = std::modf(position.y, &y_intpart);
    ItemType item = (ItemType)this->map[(int)x_intpart][(int)y_intpart][1];;

    if (isFruitVisible() && item == FRUIT)
    {
        item = level->getFruitTypeInLevel();
    }
    else if (x_fractpart >= 0.25 && x_fractpart <= 0.75 &&
        y_fractpart >= 0.25 && y_fractpart <= 0.75)
    {
        item = (ItemType)this->map[(int)x_intpart][(int)y_intpart][1];
    }
    else
    {
        item = NONE;
    }
    return ItemFactory::getItem(item);
}

Vector2<float> TileMap::getGhostHomeSquare() const
{
    return this->ghostHomeSquare;
}

Vector2<float> TileMap::getRandomTile( TileType type, uint32 xMin, uint32 xMax,
                                       uint32 yMin, uint32 yMax) const
{
    if( xMax >= this->getXLimit() )
        xMax = this->getXLimit() - 1;
    if( yMax >= this->getYLimit() )
        yMax = this->getYLimit() - 1;

    Vector2<float> rdTile;
    Random rd;
    do
    {
        rdTile.x = rd.getRandom( xMin, xMax );
        rdTile.y = rd.getRandom( yMin, yMax );
    } while( this->getTile(rdTile) != type );
    return rdTile;
}

uint32 TileMap::getDistance( Vector2f pos1, Vector2f pos2 ) const
{
    return std::sqrt( pow(pos2.x-pos1.x, 2) + pow(pos2.y-pos1.y, 2) );
}


void TileMap::eatItem(const Vector2f& position)
{
    const int x = std::trunc(position.x);
    const int y = std::trunc(position.y);
    if (this->map[x][y][1] == uint8(DOT))
    {
        this->itemAcc++;
        if (this->itemAcc == 70 || this->itemAcc == 170)
        {
            this->notify(FRUIT_APPEAR);
            this->fruitActivate = true;
            this->timer.start();
            isFruitVisible();
        }
    }
    else if (isFruit(this->map[x][y][1]) && this->isFruitVisible())
    {
        this->fruitEaten();
    }
    if (!isFruit(this->map[x][y][1]))
    {
        this->map[x][y][1] = uint8(NONE);
        this->itemCounter--;

        if (this->itemCounter == 0)
            this->notify(END_OF_LEVEL);
    }
}

bool TileMap::isFruitVisible()
{
    if (this->fruitActivate)
    {
        const fsec diff = timer.getElapsedTime();

        if (diff.count() > ConfModel::CONST::FRUIT_APPEAR_DURATION)
            this->fruitDisappear();
    }
    return this->fruitActivate;
}

void TileMap::fruitDisappear()
{
    this->fruitActivate = false;
    this->notify(FRUIT_DISAPPEAR);
}

void TileMap::fruitEaten()
{
    this->fruitActivate = false;
    this->notify(FRUIT_EATEN);
}

uint32 TileMap::getXLimit() const
{
    return this->limit.x;
}

uint32 TileMap::getYLimit() const
{
    return this->limit.y;
}

void TileMap::setNewMap(const uint32 & xlim, const uint32 & ylim,
                        const std::string & map)
{
    if (this->map != nullptr)
        this->destroyCurrentMap();
    this->limit.x = xlim;
    this->limit.y = ylim;
    this->itemCounter = 0;
    this->itemAcc = 0;
    this->allocateNewMap();
    this->fillMap(map);
}

void TileMap::setLevelManager(const Level* const level)
{
    this->level = level;
}

void TileMap::pauseFruitAppearing()
{
    timer.pause();
}

void TileMap::resumeFruitAppearing()
{
    timer.resume();
}

Vector2f TileMap::getPacmanCoordinate() const
{
    return pacmanPos;
}
    
const Level* TileMap::getLevel() const
{
    return level;
}

} // namespace pm
