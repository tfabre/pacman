#include "pacman/model/GameState.hpp"

namespace pm
{

void GameState::loadScore()
{
    std::ifstream file(ConfModel::FILE::SCORE, std::ios::in | std::ios::binary);
    file.get();
    if (!file.eof())
    {
        while (!file.eof() &&
               bestScore.size() < ConfModel::CONST::BEST_SCORE_TO_SAVE)
        {
            file.unget();
            uint64 score;
            file.read(reinterpret_cast<char*>(&score), sizeof(score));
            TimePoint timePoint = extractDate(file);
            file.get(); // '\n' at the end of line
            file.get(); // eof
            bestScore.push_back(std::make_tuple(score, timePoint));
        }
    }
    file.close();
}

ScoreLine GameState::getScoreToSave() const
{
    return std::make_tuple(score, Clock::now());
}

bool GameState::emplaceScore()
{
    bool isEmplaced{false};
    if (bestScore.size() == 0)
        bestScore.push_back(getScoreToSave());

    for (std::list<ScoreLine>::iterator it = bestScore.begin();
         it != bestScore.end(); ++it)
    {
        if (std::get<0>(*it) < score)
        {
            bestScore.insert(it, getScoreToSave());
            isEmplaced = true;
            break;
        }
    }

    if (!isEmplaced && bestScore.size() <= ConfModel::CONST::BEST_SCORE_TO_SAVE)
    {
        bestScore.push_back(getScoreToSave());
        isEmplaced = true;
    }
    else if (bestScore.size() > ConfModel::CONST::BEST_SCORE_TO_SAVE)
        bestScore.pop_back();

    return isEmplaced;
}

GameState::GameState(Pacman* const pacman, const Level* const level,
                     const uint8& l, const uint64& s):
    life(l), score(s), pacman(pacman), level(level), enemies(), bestScore()
{
    this->pacman->addObserver(*this);
    loadScore();
}

uint8 GameState::getLifeNumber() const
{
    return this->life;
}

uint32 GameState::getLevelNumber() const
{
    return this->level->getLevelNumber();
}

uint64 GameState::getScore() const
{
    return this->score;
}

void GameState::addGhost(Ghost* const ghost)
{
    enemies.push_back(ghost);
}

uint64 GameState::getEatenGhostScore() const
{
    return std::pow(2, ghostEat) * ConfModel::CONST::GHOST_VALUE;
}

void GameState::actualisate(const Event& event)
{
    if (event == EAT_ITEM || event == EAT_POWER_PELLET)
    {
        const pm::uint32 pts4Life = ConfModel::CONST::POINTS_FOR_EXTRA_LIFE;
        pm::uint64 oldScore = (this->score / pts4Life);
        this->score += pacman->getEatenItem().getScore();

        if ((this->score / pts4Life) > oldScore)
            this->life++;

        if (event == EAT_POWER_PELLET)
            ghostEat = 0;
    }
    else if (event == EAT_GHOST)
    {
        ghostEat++;
        this->score += getEatenGhostScore();
    }
    else if (event == EATEN)
    {
        this->life--;
        this->pacman->replace();
        for (Ghost* const ghost: enemies)
            ghost->replace();

        if (this->life == uint8(-1))
            writeScore();
    }
    else if (event == END_OF_LEVEL)
    {
        this->pacman->replace();
        for (Ghost* const ghost: enemies)
            ghost->replace();
    }
}

TimePoint GameState::extractDate(std::ifstream& file)
{
    TimeStamp stamp;
    file.read(reinterpret_cast<char*>(&stamp), sizeof(stamp));
    ClockDuration duration{stamp};
    return TimePoint{duration};
}

void GameState::writeScore()
{
    emplaceScore();
    std::ofstream file(ConfModel::FILE::SCORE, std::ios::out |std::ios::binary);
    for (const ScoreLine& sl: bestScore)
    {
        uint64 score = std::get<0>(sl);
        file.write(reinterpret_cast<char*>(&score), sizeof(score));
        TimeStamp stamp = std::get<1>(sl).time_since_epoch().count();
        file.write(reinterpret_cast<char*>(&stamp), sizeof(stamp));
        file.put('\n');
    }
    file.close();
}

std::list<ScoreLine> GameState::getBestScoreList() const
{
    return this->bestScore;
}

}
