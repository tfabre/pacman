#include "pacman/model/character/Positionned.hpp"

namespace pm
{

Positionned::Positionned(const Vector2f & pos, const float & speed,
                         const float & size, TileMap* tm):
    position(pos), speed(speed), size(size), tileMap(tm)
{}

Positionned::~Positionned()
{}

Vector2f Positionned::getPosition() const
{
    return this->position;
}

float Positionned::getSpeed() const
{
    return this->speed;
}

TileMap* Positionned::getTileMap() const
{
    return this->tileMap;
}

float Positionned::getSize() const
{
    return this->size;
}

void Positionned::setPosition(const Vector2f & pos)
{
    this->position = pos;
}

void Positionned::setSpeed(const float & speed)
{
    this->speed = speed;
}

void Positionned::setTileMap(TileMap* tm)
{
    this->tileMap = tm;
}

void Positionned::setSize(const float & size)
{
    this->size = size;
}

bool Positionned::haveCollisionWith(const Positionned* entity) const
{
    return detectCollision(this, entity);
}

bool Positionned::detectCollision(const Positionned* e1, const Positionned* e2)
{
    return
        e1->position.x + e1->size >= e2->position.x - e2->size &&
        e1->position.x - e1->size <= e2->position.x + e2->size &&
        e1->position.y + e1->size >= e2->position.y - e2->size &&
        e1->position.y - e1->size <= e2->position.y + e2->size;
}

}
