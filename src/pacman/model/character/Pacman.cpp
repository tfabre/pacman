#include "pacman/model/character/Pacman.hpp"
#include "pacman/model/ConfModel.hpp"

#include <cmath>

namespace pm
{

Pacman::Pacman(const Vector2f & pos, const float & speed, TileMap* tm,
               const Vector2f & origin, const Direction & direction):
    Positionned(pos, speed, ConfModel::PACMAN::DEFAULT_SIZE, tm),
    direction(direction), origin(origin),
    lastItemEaten(ItemFactory::getItem(NONE))
{}

void Pacman::getSurrounding(const Vector2f & pos, TileType surrounding[]) const
{
    TileMap* const tm = this->getTileMap();
    if (pos.x -1 >= 0 && pos.y -1 >= 0)
        surrounding[0] = tm->getTile(Vector2f(pos.x -1, pos.y -1));

    if (pos.y -1 >= 0)
        surrounding[1] = tm->getTile(Vector2f(pos.x, pos.y -1));

    if (pos.x +1 < tm->getXLimit() && pos.y -1 >= 0)
        surrounding[2] = tm->getTile(Vector2f(pos.x +1, pos.y -1));

    if (pos.x -1 >= 0)
        surrounding[3] = tm->getTile(Vector2f(pos.x -1, pos.y));

    if (pos.x +1 < tm->getXLimit())
        surrounding[4] = tm->getTile(Vector2f(pos.x +1, pos.y));

    if (pos.x -1 >= 0 && pos.y +1 < tm->getYLimit())
        surrounding[5] = tm->getTile(Vector2f(pos.x -1, pos.y +1));
    
    if (pos.y +1 < tm->getYLimit())
        surrounding[6] = tm->getTile(Vector2f(pos.x, pos.y +1));

    if (pos.x +1 < tm->getXLimit() && pos.y +1 < tm->getYLimit())
        surrounding[7] = tm->getTile(Vector2f(pos.x +1, pos.y +1));
}

bool Pacman::haveCollisionWith(const TileType & tt) const
{
    return tt == WALL || tt == GHOST_DOOR;
}

bool Pacman::detectCollision(const Vector2f & pos) const
{
    TileType surrounding[8] = {GROUND, GROUND, GROUND, GROUND, GROUND, GROUND,
                               GROUND, GROUND};
    float x;
    float y;
    std::modf(pos.x, &x);
    std::modf(pos.y, &y);
    getSurrounding(pos, surrounding);

    if (haveCollisionWith(surrounding[0]) && (-pos.x + 0.5 + x + y) > pos.y)
        return true;
    if (haveCollisionWith(surrounding[1]) && pos.y < (0.5 + y))
        return true;
    if (haveCollisionWith(surrounding[2]) && (pos.x - 0.5 - x + y) > pos.y)
        return true;
    if (haveCollisionWith(surrounding[3]) && pos.x < (0.5 + x))
        return true;
    if (haveCollisionWith(surrounding[4]) && pos.x > (0.5 + x))
        return true;
    if (haveCollisionWith(surrounding[5]) && (pos.x + 0.5 - x + y) < pos.y)
        return true;
    if (haveCollisionWith(surrounding[6]) && pos.y > (0.5 + y))
        return true;
    if (haveCollisionWith(surrounding[7]) && (-pos.x + 1.5 + x + y) < pos.y)
        return true;

    return false;
}

void Pacman::move()
{
    Vector2f position = this->computeNewPosition();
    TileMap* const tm = this->getTileMap();

    if (tm->getTile(position) == GROUND && !detectCollision(position))
    {
        // Rounding to avoid speed decreasement due to moveInAngle methode
        const uint32 roundVal = ConfModel::CONST::ROUND_VALUE;
        position.x = std::round(position.x * roundVal) / roundVal;
        position.y = std::round(position.y * roundVal) / roundVal;

        this->setPosition(position);

        Item item = tm->getItem(position);
        if (item != ItemFactory::getItem(NONE))
        {
            tm->eatItem(position);
            this->lastItemEaten = item;
            if (item == ItemFactory::getItem(POWER_PELLET))
                this->notify(EAT_POWER_PELLET);
            this->notify(EAT_ITEM);
        }
    }
}

Vector2f Pacman::computeNewPosition() const
{
    Vector2f position = this->getPosition();
    const float speed = this->getSpeed();
    TileMap* const tm = this->getTileMap();

    if (this->direction == TOP)
        position.y -= speed;

    else if (this->direction == BOTTOM)
        position.y += speed;

    else if (this->direction == LEFT)
    {
        position.x -= speed;
        if (position.x < 0)
        {
            position.x = tm->getXLimit() - .5;
            if (speed < .5)
                position.x += speed;
        }
    }
    else if (this->direction == RIGHT)
    {
        position.x += speed;
        if (position.x >= tm->getXLimit())
        {
            position.x = .5;
            if (speed < .5)
                position.x -= speed;
        }
    }
    moveInAngle(position, speed);
    return position;
}

void Pacman::moveInAngle(Vector2f& position, const float& speed) const
{
    const float halfX = std::trunc(position.x) + .5;
    const float halfY = std::trunc(position.y) + .5;

    if (this->direction == TOP || this->direction == BOTTOM)
    {
        if (position.x > halfX)
            position.x -= speed - ConfModel::CONST::ROUND_DECREASE;
        else if (position.x < halfX)
            position.x += speed - ConfModel::CONST::ROUND_DECREASE;
    }
    else
    {
        if (position.y > halfY)
            position.y -= speed - ConfModel::CONST::ROUND_DECREASE;
        else if (position.y < halfY)
            position.y += speed - ConfModel::CONST::ROUND_DECREASE;
    }
}

void Pacman::replace()
{
    this->direction = ConfModel::PACMAN::DEFAULT_DIRECTION;
    this->setPosition(origin);
}

bool Pacman::changeDirection(const Direction & direction)
{
    if (this->direction != direction)
    {
        const Direction directionBackUp = this->direction;
        this->direction = direction;

        const Vector2f position = this->computeNewPosition();
        TileMap* const tm = this->getTileMap();
        const bool movePossibility = tm->getTile(position) == GROUND
            && !detectCollision(position);

        if (!movePossibility)
            this->direction = directionBackUp;

        return movePossibility;
    }
    return true;
}

Item Pacman::getEatenItem() const
{
    return this->lastItemEaten;
}

Vector2f Pacman::getOriginalPosition() const
{
    return this->origin;
}

Direction Pacman::getCurrentDirection() const
{
    return this->direction;
}

}
