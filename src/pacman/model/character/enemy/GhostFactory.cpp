#include "pacman/model/character/enemy/GhostFactory.hpp"
#include "pacman/model/character/enemy/behavior/decorator/Patrol.hpp"
#include "pacman/model/character/enemy/behavior/Patrolling.hpp"
#include "pacman/model/ConfModel.hpp"
#include "pacman/model/GameFactory.hpp"

namespace pm
{

Ghost* GhostFactory::getNewBashful(const Vector2f& position,
                                   const Ghost* blinky)
{
    TileMap* const map = GameFactory::getTileMap();
    PathFinding* const pf = GameFactory::getPathFinding();
    Bashful* const bash = new Bashful(position, ConfModel::GHOST::DEFAULT_SPEED,
                                      ConfModel::GHOST::DEFAULT_SIZE,
                                      map, GameFactory::getPacman(), pf,
                                      blinky->getBehavior());
    return buildDecorator(bash);
}

Ghost* GhostFactory::getNewPokey(const Vector2f& position)
{
    TileMap* const map = GameFactory::getTileMap();
    PathFinding* const pf = GameFactory::getPathFinding();
    Pokey* const pokey = new Pokey(position, ConfModel::GHOST::DEFAULT_SPEED,
                                   ConfModel::GHOST::DEFAULT_SIZE,
                                   map, GameFactory::getPacman(), pf);
    return buildDecorator(pokey);
}

Ghost* GhostFactory::getNewShadow(const Vector2f& position)
{
    TileMap* const map = GameFactory::getTileMap();
    PathFinding* const pf = GameFactory::getPathFinding();
    Shadow* const shadow = new Shadow(position, ConfModel::GHOST::DEFAULT_SPEED,
                                      ConfModel::GHOST::DEFAULT_SIZE,
                                      map, GameFactory::getPacman(), pf);
    return buildDecorator(shadow);
}

Ghost* GhostFactory::getNewSpeedy(const Vector2f& position)
{
    TileMap* const map = GameFactory::getTileMap();
    PathFinding* const pf = GameFactory::getPathFinding();
    Speedy* const speedy = new Speedy(position, ConfModel::GHOST::DEFAULT_SPEED,
                                      ConfModel::GHOST::DEFAULT_SIZE,
                                      map, GameFactory::getPacman(), pf);
    return buildDecorator(speedy);
}

Ghost* GhostFactory::buildDecorator(EnemyBehavior* const behavior)
{
    TileMap* const map       = GameFactory::getTileMap();
    PathFinding* const pf    = GameFactory::getPathFinding();
    Leak* const leak         = new Leak(behavior->getPosition(),
                                        ConfModel::GHOST::LEAK_SPEED,
                                        ConfModel::GHOST::DEFAULT_SIZE,
                                        map, GameFactory::getPacman(), pf);
    Eatable* const eatable   = new Eatable(leak);
    eatable->addObserver(*GameFactory::getGameState());
    Patrolling* const patrolling =
        new Patrolling(behavior->getPosition(), ConfModel::GHOST::DEFAULT_SPEED,
                       ConfModel::GHOST::DEFAULT_SIZE, map,
                       GameFactory::getPacman(), pf);
    Patrol* const patrol = new Patrol(behavior, patrolling,
                                      ConfModel::GHOST::PATROLLING_DURATION,
                                      ConfModel::GHOST::CHASING_DURATION);
    PacmanEater* const eater = new PacmanEater(patrol);
    eater->addObserver(*GameFactory::getGameState());
    PowerPelletAffected* const ppa =
        new PowerPelletAffected(eater, eatable,
                                ConfModel::GHOST::POWER_PELLET_DURATION);
    GameFactory::getPacman()->addObserver(*ppa);

    BackHome* const backHome = new BackHome(behavior->getPosition(),
                                            ConfModel::GHOST::BACK_HOME_SPEED,
                                            ConfModel::GHOST::DEFAULT_SIZE,
                                            map, GameFactory::getPacman(), pf);
    Ghost* ghost = new Ghost(ppa, backHome);
    eatable->addObserver(*ghost);
    GameFactory::getGameState()->addGhost(ghost);
    return ghost;
}

}
