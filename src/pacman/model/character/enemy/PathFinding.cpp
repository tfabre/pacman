#include "pacman/model/character/enemy/PathFinding.hpp"

namespace pm
{

void PathFinding::path( std::stack< Vector2<float> >* pathStack, const Vector2<float>& start, const Vector2<float>& end )
{
    this->path( pathStack, (uint32)start.x, (uint32)start.y, (uint32)end.x, (uint32)end.y );
}

void PathFinding::path( std::stack< Vector2<float> >* pathStack, uint32 xStart, uint32 yStart, uint32 xEnd, uint32 yEnd)
{
    if( xStart == xEnd && yStart == yEnd )
        return;

    // Clear the stack
    while( pathStack->size() > 0 )
    {
        pathStack->pop();
    }
    freeLists();

    // Making the start square
    s_square* startSquare = new s_square( xStart, yStart, 0, getDistance(xStart, yStart, xEnd, yEnd), NULL );
    openList.push_back( startSquare );

    #ifdef DEBUG
    std::cout << "\nScore of the start square : " << startSquare->getScore() << '\n';
 	#endif

    bool isPathFound = false;
    s_square* currentSquare;
    do
    {
        // Retrieve the square with the lowest score
        uint32 lowestScoreIndex = 0;
        for( uint32 i = 1; i < openList.size(); ++i )
        {
            if( openList.at(lowestScoreIndex)->getScore() > openList.at(i)->getScore() )
                lowestScoreIndex = i;
        }
        currentSquare = openList.at(lowestScoreIndex);

        #ifdef DEBUG
        std::cout << "\n* This turn, the lowest score is " << currentSquare->getScore()
            << " (G: " << currentSquare->g << ", " << "H: " << currentSquare->h << ") ; {"
            << currentSquare->pos.x << ',' << currentSquare->pos.y << "}\n";
	    #endif

        // On retire la case traîtée de l'openList et on l'ajoute à la closeList
        closeList.push_back( openList.at(lowestScoreIndex) );
        openList.erase( openList.begin() + lowestScoreIndex );

        #ifdef DEBUG
        std::cout << "\nWe add the square {" << currentSquare->pos.x
            << "," << currentSquare->pos.y << "} to closeList and we remove it from openList.\n";
     	#endif

        // Si la case de destination est dans la closeList, le chemin est trouvé
        for( uint32 i = 0; i < closeList.size(); ++i )
        {
            if( closeList.at(i)->pos.x == xEnd && closeList.at(i)->pos.y == yEnd )
            {
                isPathFound = true;
                #ifdef DEBUG
                std::cout << "Path found! !!.\n";
                #endif
            }
        }
        if( isPathFound )
            break;

        // Récupère les cases adjacentes de notre case de départ
        std::vector< Vector2<float> > adjSquareList;

        // Top adjacent square
        Vector2<float> adjSquare( currentSquare->pos.x, currentSquare->pos.y - 1);
        if( adjSquare.y >= 0 && tm->getTile(adjSquare) != TileType::WALL)
        {
            adjSquareList.push_back( adjSquare );
            #ifdef DEBUG
            std::cout << "\nCase haute de notre currentSquare en {" << adjSquare.x << "," << adjSquare.y << "}.\n";
            #endif
        }
        // Left adjacent square
        adjSquare.x = currentSquare->pos.x - 1;
        adjSquare.y = currentSquare->pos.y;
        if( adjSquare.x >= 0 && tm->getTile(adjSquare) != TileType::WALL)
        {
            adjSquareList.push_back( adjSquare );
            #ifdef DEBUG
            std::cout << "\nCase gauche de notre currentSquare en {" << adjSquare.x << "," << adjSquare.y << "}.\n";
            #endif
        }
        // Bottom adjacent square
        adjSquare.x = currentSquare->pos.x;
        adjSquare.y = currentSquare->pos.y + 1;
        if( adjSquare.y < tm->getYLimit() && tm->getTile(adjSquare) != TileType::WALL)
        {
            adjSquareList.push_back( adjSquare );
            #ifdef DEBUG
            std::cout << "\nCase basse de notre currentSquare en {" << adjSquare.x << "," << adjSquare.y << "}.\n";
            #endif
        }
        // Right adjacent square
        adjSquare.x = currentSquare->pos.x + 1;
        adjSquare.y = currentSquare->pos.y;
        if( adjSquare.x < tm->getXLimit() && tm->getTile(adjSquare) != TileType::WALL)
        {
            adjSquareList.push_back( adjSquare );
            #ifdef DEBUG
            std::cout << "\nCase droite de notre currentSquare en {" << adjSquare.x << "," << adjSquare.y << "}.\n";
            #endif
        }

        // On va désormais traîter chacune des cases adjacentes recensées
        for( uint32 i = 0 ; i < adjSquareList.size() ; ++i )
        {
            // Si la case est présente dans la closeList, on ignore la case
            bool isAdjSquarePresent = false;
            for( uint32 j = 0; j < closeList.size(); ++j )
            {
                if( adjSquareList.at(i).x == closeList.at(j)->pos.x
                        && adjSquareList.at(i).y == closeList.at(j)->pos.y )
                {
                    isAdjSquarePresent = true;
                    break;
                    #ifdef DEBUG
                    std::cout << "Case {" << adjSquareList.at(i).x << ","
                        << adjSquareList.at(i).y << "} deja presente dans la closeList. Donc, on continue...\n";
                    #endif
                }
            }
            if( isAdjSquarePresent )
            {
                continue;
            }

            // On vérifie si la case existe dans l'openList
            isAdjSquarePresent = false;
            for( uint32 j = 0; j < openList.size(); ++j )
            {
                if( adjSquareList.at(i).x == openList.at(j)->pos.x
                        && adjSquareList.at(i).y == openList.at(j)->pos.y )
                {
                    isAdjSquarePresent = true;
                    break;
                }
            }
            // Si la case n'est pas présente dans l'openlist, on rajoute
            if( !isAdjSquarePresent )
            {
                float score = getDistance( adjSquareList.at(i).x, adjSquareList.at(i).y, xEnd, yEnd );
                openList.push_back( new s_square(adjSquareList.at(i), currentSquare->g+1, score, currentSquare) );
                #ifdef DEBUG
                std::cout << "Case {" << adjSquareList.at(i).x << "," << adjSquareList.at(i).y
                    << "} rajoutee a openList (G: " << currentSquare->g+1 << ", H : " << score << ").\n";
                #endif
            }
        }
    } while( !openList.empty() );

    if( isPathFound )
    {
        // Add to the stack the squares of the path
        s_square* squareTmp = closeList.at( closeList.size() - 1 );
        do
        {
            #ifdef DEBUG
            std::cout << " (" << squareTmp->pos.x << "," << squareTmp->pos.y << ") ;";
            #endif
            pathStack->push( Vector2<float>(squareTmp->pos.x, squareTmp->pos.y) );
            squareTmp = squareTmp->parent;
        } while( squareTmp->parent != NULL );
            
        #ifdef DEBUG
        std::cout << "\n";
        #endif
    }
    else
    {
        #ifdef DEBUG
        std::cout << "\nNo path available...\n";
        #endif
        return;
    }
}

float PathFinding::getDistance( uint32 xA, uint32 yA, uint32 xB, uint32 yB )
{
    return ((xA > xB) ? xA - xB : xB - xA) + ((yA > yB) ? yA - yB : yB - yA);
}

void PathFinding::freeLists()
{
    while( openList.size() > 0 )
    {
        s_square* s = openList.back();
        openList.pop_back();
        delete s;
        s = NULL;
    }
    while( closeList.size() > 0 )
    {
        s_square* s = closeList.back();
        closeList.pop_back();
        delete s;
        s = NULL;
    }
}

} // nampespace pm
