#include "pacman/model/character/enemy/Ghost.hpp"
#include "pacman/model/GameFactory.hpp"
#include "pacman/model/ConfModel.hpp"

namespace pm
{

Ghost::Ghost(EnemyBehavior* const behavior, EnemyBehavior* const backHome):
    behavior(behavior), backHome(backHome), origin(behavior->getPosition())
{}

Ghost::~Ghost()
{
    delete behavior;
    delete backHome;
}

void Ghost::move()
{
    if (this->behavior->getState() == EnemyState::BACK_HOME)
        this->backHome->move();
    else
        this->behavior->move();

    if (this->behavior->getState() == EnemyState::BACK_HOME &&
        this->backHome->getState() == EnemyState::NORMAL)
    {
        this->behavior->setState(EnemyState::NORMAL);

        Vector2f posTmp = this->backHome->getPosition();
        posTmp.x = (uint32)posTmp.x + 0.5;
        posTmp.y = (uint32)posTmp.y + 0.5;
        this->behavior->setPosition( posTmp );

        this->behavior->setDirection(this->backHome->getDirection());
        this->behavior->clearPath();
    }
}
    
Vector2f Ghost::getPosition() const
{
    Vector2f position = this->behavior->getPosition();
    if (this->behavior->getState() == EnemyState::BACK_HOME)
        position = this->backHome->getPosition();
    return position;
}

Direction Ghost::getDirection() const
{
    Direction direction = this->behavior->getDirection();
    if (this->behavior->getState() == EnemyState::BACK_HOME)
        direction = this->backHome->getDirection();

    return direction;
}

EnemyBehavior* Ghost::getBehavior() const
{
    EnemyBehavior* behavior = this->behavior;
    if (this->behavior->getState() == EnemyState::BACK_HOME)
        behavior = this->backHome;

    return behavior;
}

EnemyState Ghost::getState() const
{
    return this->behavior->getState();
}

void Ghost::actualisate(const Event& event)
{
    Vector2f posTmp = this->behavior->getPosition();
    switch (event)
    {
    case EAT_GHOST:
        this->behavior->setState(EnemyState::BACK_HOME);

        posTmp.x = (uint32)posTmp.x+0.5;
        posTmp.y = (uint32)posTmp.y+0.5;
        this->backHome->setPosition(posTmp);

        this->backHome->setState(EnemyState::BACK_HOME);
        this->backHome->setDirection(this->behavior->getDirection());
        this->backHome->clearPath();
        break;
    default: break;
    }
}

void Ghost::replace()
{
    this->behavior->replace(origin);
    this->backHome->replace(origin);
}

void Ghost::pause()
{
    this->behavior->pause();
    this->backHome->pause();
}

void Ghost::resume()
{
    this->behavior->resume();
    this->backHome->resume();
}

}
