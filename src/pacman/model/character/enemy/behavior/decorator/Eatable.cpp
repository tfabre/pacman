#include "pacman/model/character/enemy/behavior/decorator/Eatable.hpp"

namespace pm
{

Eatable::Eatable(EnemyBehavior* behavior):
    EnemyBehaviorDecorator(behavior), Subject()
{
    this->setPosition(behavior->getPosition());
}

void Eatable::move()
{
    EnemyBehavior* this_behavior = this->getBehavior();
    this_behavior->setPosition(this->getPosition());
    this_behavior->move();

    this->setPosition(this_behavior->getPosition());
    if (this->haveCollisionWith(this->getPacman()))
        this->notify(EAT_GHOST);
    this->setDirection(this->getBehavior()->getDirection());
}

void Eatable::clearPath()
{
    this->getBehavior()->clearPath();
}

void Eatable::setDirection(const Direction& direction)
{
    EnemyBehavior::setDirection(direction);
    this->getBehavior()->setDirection(direction);
}

void Eatable::setPosition(const Vector2f& position)
{
    EnemyBehavior::setPosition(position);
    this->getBehavior()->setPosition(position);
}

}
