#include "pacman/model/character/enemy/behavior/decorator/PowerPelletAffected.hpp"

namespace pm
{

PowerPelletAffected::PowerPelletAffected(EnemyBehavior* mainBehavior,
                                         EnemyBehavior* ppaBehavior,
                                         const float & duration):
    EnemyBehaviorDecorator(mainBehavior),
    affectedPelletBehavior(ppaBehavior),
    timer(), duration(duration), isEffectActive(false)
{}

PowerPelletAffected::~PowerPelletAffected()
{
    delete this->affectedPelletBehavior;
}

void PowerPelletAffected::actualisate(const Event & event)
{
    if (event == EAT_POWER_PELLET)
    {
        this->timer.start();
        this->isEffectActive = true;
        if (this->getState() != EnemyState::BACK_HOME)
            this->setState(EnemyState::LEAK);

        this->affectedPelletBehavior->setPosition( this->getPosition() );

        this->affectedPelletBehavior->setDirection(
            this->getBehavior()->getDirection());
        this->setDirection(this->getBehavior()->getDirection());
        this->affectedPelletBehavior->setDirection(this->getDirection());
    }
}

void PowerPelletAffected::move()
{
    EnemyBehavior* activeBehavior;
    if (this->isEffectActive && this->getState() == EnemyState::LEAK)
    {
        activeBehavior = this->affectedPelletBehavior;
        Vector2f behaviorPos = activeBehavior->getPosition();
        uint32 x = (behaviorPos.x * 10);
        uint32 y = (behaviorPos.y * 10);
        
        if (timer.getElapsedTime().count() > this->duration &&
            x % 2 == 1 && y % 2 == 1)
        {
            this->isEffectActive = false;
            this->setState(EnemyState::NORMAL);
            activeBehavior = this->getBehavior();

            activeBehavior->setPosition(this->getPosition());
            activeBehavior->setDirection(
                affectedPelletBehavior->getDirection());
            this->setDirection(activeBehavior->getDirection());
            this->getBehavior()->clearPath();
        }
    }
    else
    {
        activeBehavior = this->getBehavior();
    }
    activeBehavior->setPosition( this->getPosition() );
    activeBehavior->move();
    this->setPosition(activeBehavior->getPosition());
    this->setDirection(activeBehavior->getDirection());
}

void PowerPelletAffected::clearPath()
{
    this->getBehavior()->clearPath();
    this->affectedPelletBehavior->clearPath();
}

void PowerPelletAffected::setDirection(const Direction& direction)
{
    EnemyBehavior::setDirection(direction);
    this->getBehavior()->setDirection(direction);
    this->affectedPelletBehavior->setDirection(direction);
}
    
void PowerPelletAffected::setPosition(const Vector2f& position)
{
    EnemyBehavior::setPosition(position);
    this->getBehavior()->setPosition(position);
    this->affectedPelletBehavior->setPosition(position);
}

void PowerPelletAffected::replace(const Vector2f& origin)
{
    EnemyBehaviorDecorator::replace(origin);
    this->affectedPelletBehavior->replace(origin);
    this->isEffectActive = false;
}

void PowerPelletAffected::pause()
{
    timer.pause();
    this->getBehavior()->pause();
    this->affectedPelletBehavior->pause();
}

void PowerPelletAffected::resume()
{
    this->getBehavior()->resume();
    this->affectedPelletBehavior->resume();
    timer.resume();
}

} // namespace pm
