#include "pacman/model/character/enemy/behavior/decorator/Patrol.hpp"

namespace pm
{

Patrol::Patrol(EnemyBehavior* const mainBehavior, EnemyBehavior*const pBehavior,
               const float& patrolDuration, const float chaseDuration):
    EnemyBehaviorDecorator(mainBehavior),
    patrolBehavior(pBehavior),
    timer(),
    patrolDuration(patrolDuration),
    chaseDuration(chaseDuration),
    isPatrolling(true)
{
    timer.start();
}

Patrol::~Patrol()
{
    delete this->patrolBehavior;
}

void Patrol::move()
{
    EnemyBehavior* activeBehavior;
    if (isPatrolling)
    {
        activeBehavior = this->patrolBehavior;
        if (timer.getElapsedTime().count() > patrolDuration)
        {
            timer.start();
            isPatrolling = false;
            activeBehavior = this->getBehavior();
            activeBehavior->setPosition(this->getPosition());
            activeBehavior->setDirection(this->patrolBehavior->getDirection());
            this->setDirection(activeBehavior->getDirection());
            activeBehavior->clearPath();
        }
    }
    else
    {
        activeBehavior = this->getBehavior();
        if (timer.getElapsedTime().count() > chaseDuration)
        {
            timer.start();
            isPatrolling = true;
            activeBehavior = this->patrolBehavior;
            activeBehavior->setPosition(this->getPosition());
            activeBehavior->setDirection(this->getBehavior()->getDirection());
            this->setDirection(activeBehavior->getDirection());
            activeBehavior->clearPath();
        }
    }
    activeBehavior->setPosition(this->getPosition());
    activeBehavior->move();
    this->setPosition(activeBehavior->getPosition());
    this->setDirection(activeBehavior->getDirection());
}

void Patrol::clearPath()
{
    this->getBehavior()->clearPath();
    this->patrolBehavior->clearPath();
}

void Patrol::setDirection(const Direction& direction)
{
    EnemyBehavior::setDirection(direction);
    this->getBehavior()->setDirection(direction);
    this->patrolBehavior->setDirection(direction);
}

void Patrol::setPosition(const Vector2f& position)
{
    EnemyBehavior::setPosition(position);
    this->getBehavior()->setPosition(position);
    this->patrolBehavior->setPosition(position);
}

void Patrol::replace(const Vector2f& origin)
{
    EnemyBehaviorDecorator::replace(origin);
    this->patrolBehavior->replace(origin);
    this->isPatrolling = true;
    this->timer.start();
}

void Patrol::pause()
{
    timer.pause();
    this->getBehavior()->pause();
    this->patrolBehavior->pause();
}

void Patrol::resume()
{
    timer.resume();
    this->getBehavior()->resume();
    this->patrolBehavior->resume();
}

} // namespace pm
