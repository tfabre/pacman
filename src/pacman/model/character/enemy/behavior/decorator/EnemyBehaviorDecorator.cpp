#include "pacman/model/character/enemy/behavior/decorator/EnemyBehaviorDecorator.hpp"

namespace pm
{

EnemyBehaviorDecorator::EnemyBehaviorDecorator(EnemyBehavior* behavior):
    EnemyBehavior(behavior->getPosition(), behavior->getSpeed(),
                  behavior->getSize(), behavior->getTileMap(),
                  behavior->getPacman(), nullptr),
    behavior(behavior)
{}

EnemyBehaviorDecorator::~EnemyBehaviorDecorator()
{
    delete behavior;
}

EnemyBehavior* EnemyBehaviorDecorator::getBehavior() const
{
    return this->behavior;
}

void EnemyBehaviorDecorator::replace(const Vector2f& origin)
{
    EnemyBehavior::replace(origin);
    behavior->replace(origin);
    this->setState(EnemyState::NORMAL);
}

}
