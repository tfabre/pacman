#include "pacman/model/character/enemy/behavior/decorator/PacmanEater.hpp"
#include <iostream>
namespace pm
{

PacmanEater::PacmanEater(EnemyBehavior* behavior):
    EnemyBehaviorDecorator(behavior), Subject()
{
    this->setPosition(behavior->getPosition());
}
    
void PacmanEater::move()
{
    EnemyBehavior* this_behavior = this->getBehavior();
    this_behavior->setPosition(this->getPosition());
    this_behavior->move();

    this->setPosition(this_behavior->getPosition());
    if (this->haveCollisionWith(this->getPacman()))
        this->notify(EATEN); 
    this->setDirection(this->getBehavior()->getDirection());
}

void PacmanEater::clearPath()
{
    this->getBehavior()->clearPath();
}

void PacmanEater::setDirection(const Direction& direction)
{
    EnemyBehavior::setDirection(direction);
    this->getBehavior()->setDirection(direction);
}

void PacmanEater::setPosition(const Vector2f& position)
{
    EnemyBehavior::setPosition(position);
    this->getBehavior()->setPosition(position);
}

}
