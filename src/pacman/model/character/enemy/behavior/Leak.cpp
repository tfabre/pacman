#include "pacman/model/character/enemy/behavior/Leak.hpp"

namespace pm
{

Leak::Leak( const Vector2f & pos, const float & speed, 
                const float & size, TileMap* tm, Pacman* const pm,
                PathFinding* pf )
    : EnemyBehavior( pos, speed, size, tm, pm, pf )
{
    this->setPosition( Vector2f((uint32)pos.x + 0.5, (uint32)pos.y + 0.5) );
}

void Leak::move()
{
    Vector2f myPos = this->getPosition();
    if( finishMove() )
    {
        return;
    }
    else if( isPacmanOnView(5) )
    {
        bool canMove[4];
        const TileMap* const tm = this->getTileMap();
        Vector2f pmPos = this->getPacman()->getPosition(), ghPos = this->getPosition();
        canMove[Direction::TOP] = tm->getTile(Vector2f(ghPos.x, ghPos.y-1)) == TileType::GROUND;
        canMove[Direction::BOTTOM] = tm->getTile(Vector2f(ghPos.x, ghPos.y+1)) == TileType::GROUND;
        canMove[Direction::RIGHT] = tm->getTile(Vector2f(ghPos.x+1, ghPos.y)) == TileType::GROUND;
        canMove[Direction::LEFT] = tm->getTile(Vector2f(ghPos.x-1, ghPos.y)) == TileType::GROUND;

        Direction dirPm = Direction::NO_DIRECTION;
        if( pmPos.x < ghPos.x && (uint32)pmPos.y == (uint32)ghPos.y )
            dirPm = Direction::RIGHT;
        else if( pmPos.x > ghPos.x && (uint32)pmPos.y == (uint32)ghPos.y )
            dirPm = Direction::LEFT;
        else if( pmPos.y < ghPos.y && (uint32)pmPos.x == (uint32)ghPos.x )
            dirPm = Direction::BOTTOM;
        else if( pmPos.y > ghPos.y && (uint32)pmPos.x == (uint32)ghPos.x )
            dirPm = Direction::TOP;

        switch( dirPm )
        {
        case Direction::TOP:
            if( canMove[Direction::TOP] )
                this->moveTo( Direction::TOP );
            else if( canMove[Direction::LEFT] )
                this->moveTo( Direction::LEFT );
            else if( canMove[Direction::RIGHT] )
                this->moveTo( Direction::RIGHT );
            break;
        case Direction::LEFT:
            if( canMove[Direction::LEFT] )
                this->moveTo( Direction::LEFT );
            else if( canMove[Direction::TOP] )
                this->moveTo( Direction::TOP );
            else if( canMove[Direction::BOTTOM] )
                this->moveTo( Direction::BOTTOM );
            break;
        case Direction::BOTTOM:
            if( canMove[Direction::BOTTOM] )
                this->moveTo( Direction::BOTTOM );
            else if( canMove[Direction::LEFT] )
                this->moveTo( Direction::LEFT );
            else if( canMove[Direction::RIGHT] )
                this->moveTo( Direction::RIGHT );
            break;
        case Direction::RIGHT:
            if( canMove[Direction::RIGHT] )
                this->moveTo( Direction::RIGHT );
            else if( canMove[Direction::TOP] )
                this->moveTo( Direction::TOP );
            else if( canMove[Direction::BOTTOM] )
                this->moveTo( Direction::BOTTOM );
            break;
        default:
            break;
        }
        clearPath();
        return;
    }

    if( this->getPath()->empty() )
    {
        Vector2<float> pmPos = this->getPacman()->getPosition(), endPos;
        TileMap* tm = this->getTileMap();
        uint32 xLim = tm->getXLimit(), yLim = tm->getYLimit();
        bool goLeft = false, goTop = false;

        if( pmPos.x > xLim/2 )
            goLeft = true;
        if( pmPos.y > yLim/2 )
            goTop = true;
        
        endPos = tm->getRandomTile( TileType::GROUND, 
                                    (goLeft) ? 1 : xLim/2,
                                    (goLeft) ? xLim/2 : xLim-1,
                                    (goTop) ? 1 : yLim/2,
                                    (goTop) ? yLim/2 : yLim-1
            );
        #ifdef DEBUG
        std::cout << "[Else] Ask PF from {" << this->getPosition().x
                  << "," << this->getPosition().y << "} to {" << endPos.x
                  << "," << endPos.y << "}" << std::endl;
        #endif
        this->getPathFinding()->path( this->path, this->getPosition(), endPos );
        #ifdef DEBUG
        std::cout << "[Else] Path found? " << !this->getPath()->empty() << std::endl;
        #endif
    }
    if( !this->getPath()->empty() )
    {
        //std::cout << "Cas 4" << std::endl;
        reposition();
    }
    #ifdef DEBUG
    else
        std::cout << "No Path Leak!" << std::endl;
    #endif
    
    if( myPos.x == this->getPosition().x &&
        myPos.y == this->getPosition().y )
    {
        clearPath();
        this->move();
    }
}

}
