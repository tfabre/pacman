#include "pacman/model/character/enemy/behavior/Speedy.hpp"

namespace pm
{

Speedy::Speedy( const Vector2f & pos, const float & speed,
                const float & size, TileMap* tm, Pacman* const pm,
                PathFinding* pf )
    : EnemyBehavior( pos, speed, size, tm, pm, pf )
{
}

void Speedy::move()
{
    Vector2f spPos = this->getPosition(),
        pmPos = this->getPacman()->getPosition();
    if( finishMove() )
    {
        return;
    }
    else if( isPacmanOnView(5) )
    {
        this->attack();
        return;
    }
    else if( this->getTileMap()->getDistance(spPos, pmPos) <= 5 )
    {
        clearPath();
        this->getPathFinding()->path( this->path, spPos, pmPos );
    }
    else if( this->getPath()->empty() )
    {
        TileMap* tm = this->getTileMap();
        Vector2f endPos;
        switch( this->getPacman()->getCurrentDirection() )
        {
        case Direction::TOP :
            endPos.x = pmPos.x;
            if( (pmPos.y-4) < 0 )
                endPos.y = 0;
            else
                endPos.y = pmPos.y - 4;
            while( tm->getTile(endPos) != TileType::GROUND )
                endPos.y++;
            break;
        case Direction::LEFT :
            endPos.y = pmPos.y;
            if( (pmPos.x-4) < 0 )
                endPos.x = 0;
            else
                endPos.x = pmPos.x - 4;
            while( tm->getTile(endPos) != TileType::GROUND )
                endPos.x++;
            break;
        case Direction::BOTTOM :
            endPos.x = pmPos.x;
            if( (pmPos.y+4) >= tm->getYLimit() )
                endPos.y = tm->getYLimit() - 1;
            else
                endPos.y = pmPos.y + 4;
            while( tm->getTile(endPos) != TileType::GROUND )
                endPos.y--;
            break;
        case Direction::RIGHT :
            endPos.y = pmPos.y;
            if( (pmPos.x+4) >= tm->getXLimit() )
                endPos.x = tm->getXLimit() - 1;
            else
                endPos.x = pmPos.x + 4;
            while( tm->getTile(endPos) != TileType::GROUND )
                endPos.x--;
            break;
        default:
            break;
        }
        this->getPathFinding()->path( this->path, this->getPosition(), endPos );
    }

    if( !this->getPath()->empty() )
        reposition();
    #ifdef DEBUG
    else
        std::cout << "No path Speedy!" << std::endl;
    #endif
}

}
