#include "pacman/model/character/enemy/behavior/Pokey.hpp"

namespace pm
{

Pokey::Pokey( const Vector2f & pos, const float & speed, 
              const float & size, TileMap* tm, Pacman* const pm,
              PathFinding* pf )
    : EnemyBehavior( pos, speed, size, tm, pm, pf )
{
}


void Pokey::move()
{
    if( finishMove() )
        return;
    else if( isPacmanOnView(4) )
        this->attack();
    else if( this->getPath()->empty() )
    {
        TileMap* tm = this->getTileMap();
        Vector2f pmPos = this->getPacman()->getPosition(),
            pkPos = this->getPosition();
        // If Clyde is far than 8 squares
        if( tm->getDistance(pkPos, pmPos) > 8 )          
        {
            this->getPathFinding()->path( this->path, pkPos, pmPos );
        }
        // If Clyde is near than 8 squares
        else
        {
            Vector2f endPos;
            Random r;
            do
            {
                float angle = r.getRandom() * M_PI * 2;
                Vector2f tmp( cos(angle)*8, sin(angle)*8 );
                endPos.x = pmPos.x+tmp.x;
                endPos.y = pmPos.y+tmp.y;
                bool isOnTop = false, isOnLeft = false;

                if( endPos.x < 0 )
                {
                    isOnTop = true;
                    endPos.x = 0;
                }
                if( endPos.y < 0 )
                {
                    isOnLeft = true;
                    endPos.y = 0;
                }
                while( endPos.x >= tm->getXLimit() )
                    endPos.x--;
                while( endPos.y >= tm->getYLimit() )
                    endPos.y--;
                
                while( !(endPos.x < 0 || endPos.x >= tm->getXLimit() ||
                         endPos.y < 0 || endPos.y >= tm->getYLimit()) &&
                       tm->getTile(endPos) != TileType::GROUND )
                {
                    endPos.x = (isOnLeft) ? endPos.x-1 : endPos.x+1;
                    endPos.y = (isOnTop) ? endPos.y-1 : endPos.y+1;
                }
                #ifdef DEBUG
                std::cout << "[POKEY TARGET] X: " << (uint32)endPos.x
                          << ", Y: " << (uint32)endPos.y << std::endl
                          << "[PK PCMN DIST] "
                          << tm->getDistance(endPos,pmPos)
                          << std::endl << "[THIS GROUND?] "
                          << tm->getTile(endPos)
                          << std::endl;
                #endif
            } while( tm->getDistance(pmPos,endPos) <= 6 &&
                     tm->getDistance(pmPos,endPos) >= 10 );
            
            this->getPathFinding()->path( this->path, pkPos, endPos );
        }
           
    }
    if( !this->getPath()->empty() )
        reposition();
    #ifdef DEBUG
    else
        std::cout << "No Path Pokey!" << std::endl;
    #endif
}

}
