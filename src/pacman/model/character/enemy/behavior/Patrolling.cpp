#include "pacman/model/character/enemy/behavior/Patrolling.hpp"

namespace pm
{

Patrolling::Patrolling( const Vector2f & pos, const float & speed, 
                        const float & size, TileMap* tm, Pacman* const pm,
                        PathFinding* pf )
    : EnemyBehavior( pos, speed, size, tm, pm, pf )
{
}

void Patrolling::move()
{
    if( finishMove() )
        return;
    else if( isPacmanOnView(4) )
        this->attack();
    else if( this->getPath()->empty() )
    {
        TileMap* tm = this->getTileMap();
        Vector2<float> endPos;
        
        endPos = tm->getRandomTile( TileType::GROUND, 
                                    0,
                                    tm->getXLimit()-1,
                                    0,
                                    tm->getYLimit()-1
            );
        this->getPathFinding()->path( this->path, this->getPosition(), endPos );
    }

    if( !this->getPath()->empty() )
        reposition();
    #ifdef DEBUG
    else
        std::cout << "No Path Patrolling!" << std::endl;
    #endif
}

}
