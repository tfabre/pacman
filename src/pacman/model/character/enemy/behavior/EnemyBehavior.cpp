#include "pacman/model/character/enemy/behavior/EnemyBehavior.hpp"

namespace pm
{

EnemyBehavior::EnemyBehavior(const Vector2f & pos, const float & speed,
                             const float & size, TileMap* tm,
                             Pacman* const pacman, PathFinding* pf ):
    Positionned(pos, speed, size, tm), pacman(pacman), pf(pf),
    state(EnemyState::NORMAL)
{
    path = new std::stack< Vector2<float> >;
    dir = Direction::NO_DIRECTION;
}

EnemyBehavior::~EnemyBehavior()
{
    delete path;
}

Pacman* EnemyBehavior::getPacman()
{
    return this->pacman;
}

std::stack< Vector2<float> >* EnemyBehavior::getPath() const
{
    return this->path;
}

PathFinding* EnemyBehavior::getPathFinding()
{
    return this->pf;
}

Direction EnemyBehavior::getDirection() const
{
    return this->dir;
}

void EnemyBehavior::setDirection(const Direction& direction)
{
    this->dir = direction;
}

void EnemyBehavior::reposition()
{
    Vector2f target = this->path->top(), ghPos = this->getPosition();
    if( (uint32)target.x > (uint32)ghPos.x && // move to RIGHT
        (uint32)target.y == (uint32)ghPos.y )
    {
        this->moveTo( Direction::RIGHT );
        this->dir = Direction::RIGHT;
    }
    else if( (uint32)target.x < (uint32)ghPos.x && // move to LEFT
        (uint32)target.y == (uint32)ghPos.y )
    {
        this->moveTo( Direction::LEFT );
        this->dir = Direction::LEFT;
    }
    else if( (uint32)target.x == (uint32)ghPos.x && // move to TOP
        (uint32)target.y < (uint32)ghPos.y )
    {
        this->moveTo( Direction::TOP );
        this->dir = Direction::TOP;
    }
    else if( (uint32)target.x == (uint32)ghPos.x && // move to BOTTOM
        (uint32)target.y > (uint32)ghPos.y )
    {
        this->moveTo( Direction::BOTTOM );
        this->dir = Direction::BOTTOM;
    }
}

void EnemyBehavior::clearPath()
{
    while( !this->getPath()->empty() )
    {
        this->getPath()->pop();
    }
}

void EnemyBehavior::moveTo( Direction dir )
{
    uint32 newPos;
    Vector2f start = this->getPosition();
    switch( dir )
    {
    case Direction::TOP:
        newPos = start.y * 10 - this->getSpeed() * 10;
        this->setPosition( Vector2<float>(start.x, newPos/10.) );
        this->dir = Direction::TOP;
        break;
    case Direction::LEFT:
        newPos = start.x * 10 - this->getSpeed() * 10;
        this->setPosition( Vector2<float>(newPos/10., start.y) );
        this->dir = Direction::LEFT;
        break;
    case Direction::BOTTOM:
        newPos = start.y * 10 + this->getSpeed() * 10;
        this->setPosition( Vector2<float>(start.x, newPos/10.) );
        start = this->getPosition();
        this->dir = Direction::BOTTOM;
        break;
    case Direction::RIGHT:
        newPos = start.x * 10 + this->getSpeed() * 10;
        this->setPosition( Vector2<float>(newPos/10., start.y) );
        this->dir = Direction::RIGHT;
        break;
    default:
        return;
    }
}

bool EnemyBehavior::isPacmanOnView( uint32 distance )
{
    Vector2f pmPos = this->getPacman()->getPosition(),
        enPos = this->getPosition();
    if((enPos.x - (uint32)enPos.x == 0.5 || enPos.y - (uint32)enPos.y == 0.5) &&
     ((uint32)enPos.x == (uint32)pmPos.x || (uint32)enPos.y == (uint32)pmPos.y))
    {
        float distEnemy_Pm = this->getPathFinding()->getDistance(
        this->getPosition().x, this->getPosition().y, pmPos.x, pmPos.y );
        if( distEnemy_Pm <= distance )
        {
            std::stack<Vector2f> path;
            this->getPathFinding()->path( &path, enPos, pmPos );
            return path.size() <= distEnemy_Pm;
        }
        else
            return false;
    }
    else
    {
        return false;
    }
}

void EnemyBehavior::attack()
{
    Vector2f pmPos = this->getPacman()->getPosition(),
        ghPos = this->getPosition();
    if( (uint32)pmPos.x > (uint32)ghPos.x && // move to RIGHT
        (uint32)pmPos.y == (uint32)ghPos.y )
    {
        this->moveTo( Direction::RIGHT );
        this->dir = Direction::RIGHT;
    }
    else if( (uint32)pmPos.x < (uint32)ghPos.x && // move to LEFT
        (uint32)pmPos.y == (uint32)ghPos.y )
    {
        this->moveTo( Direction::LEFT );
        this->dir = Direction::LEFT;
    }
    else if( (uint32)pmPos.x == (uint32)ghPos.x && // move to TOP
        (uint32)pmPos.y < (uint32)ghPos.y )
    {
        this->moveTo( Direction::TOP );
        this->dir = Direction::TOP;
    }
    else if( (uint32)pmPos.x == (uint32)ghPos.x && // move to BOTTOM
        (uint32)pmPos.y > (uint32)ghPos.y )
    {
        this->moveTo( Direction::BOTTOM );
        this->dir = Direction::BOTTOM;
    }
    clearPath();
}

bool EnemyBehavior::finishMove()
{
    if( this->dir == Direction::NO_DIRECTION )
        return false;

    Vector2f ghPos = this->getPosition();
    if( (ghPos.x - (uint32)ghPos.x == 0.5) &&
        (ghPos.y - (uint32)ghPos.y == 0.5) )
    {
        if( this->path->empty() )
            return false;

        Vector2f target = this->path->top();
        if( ((uint32)ghPos.x == (uint32)target.x) &&
            ((uint32)ghPos.y == (uint32)target.y) )
        {
            this->path->pop();
            return false;
        }

        return false;
    }
    else
    {
        this->moveTo( this->getDirection() );
        return true;
    }
}

void EnemyBehavior::setState(const EnemyState& state)
{
    this->state = state;
}

EnemyState EnemyBehavior::getState() const
{
    return this->state;
}

void EnemyBehavior::replace(const Vector2f& origin)
{
    this->setPosition(origin);
    this->clearPath();
}

void EnemyBehavior::pause()
{}

void EnemyBehavior::resume()
{}

}
