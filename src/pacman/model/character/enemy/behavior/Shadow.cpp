#include "pacman/model/character/enemy/behavior/Shadow.hpp"

namespace pm
{

Shadow::Shadow( const Vector2f & pos, const float & speed, 
                const float & size, TileMap* tm, Pacman* const pm,
                PathFinding* pf )
    : EnemyBehavior( pos, speed, size, tm, pm, pf )
{   
}

void Shadow::move()
{
    if( finishMove() )
        return;
    else if( isPacmanOnView(4) )
        this->attack();
    else if( this->getPath()->empty() )
    {
        Vector2f pmPos = this->getPacman()->getPosition(),
            shPos = this->getPosition();
        this->getPathFinding()->path( this->path, shPos, pmPos );
    }

    if( !this->getPath()->empty() )
        reposition();
    #ifdef DEBUG
    else
        std::cout << "No Path Shadow!" << std::endl;
    #endif
}

}
