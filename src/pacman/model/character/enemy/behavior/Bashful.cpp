#include "pacman/model/character/enemy/behavior/Bashful.hpp"

namespace pm
{

Bashful::Bashful( const Vector2f & pos, const float & speed, 
                  const float & size, TileMap* tm, Pacman* const pm,
                  PathFinding* pf, EnemyBehavior* shadow )
    : EnemyBehavior( pos, speed, size, tm, pm, pf ), sh(shadow)
{
}

    void dispPos( Vector2f pos, int val)
{
    std::cout << "[POS " << val << "] X: " << pos.x << ", Y: " << pos.y
              << std::endl;
}

void Bashful::move()
{
    if( finishMove() )
        return;
    else if( isPacmanOnView(4) )
        this->attack();
    else if( this->getPath()->empty() )
    {
        const TileMap* tm = this->getTileMap();
        Vector2<float> endPos, pmSpot;
        const Vector2<float> pmPos = this->getPacman()->getPosition(),
            shadowPos = this->sh->getPosition();
        // Compute the spot 2 grid spaces ahead of pm
        switch( this->getPacman()->getCurrentDirection() )
        {
        case Direction::TOP :
            pmSpot.x = pmPos.x;
            pmSpot.y = (pmPos.y - 2 < 0) ? 0 : pmPos.y - 2;
            while( tm->getTile(pmSpot) != TileType::GROUND )
                pmSpot.y++;
            break;
        case Direction::LEFT :
            pmSpot.x = (pmPos.x - 2 < 0) ? 0 : pmPos.x - 2;
            pmSpot.y = pmPos.y;
            while( tm->getTile(pmSpot) != TileType::GROUND )
                pmSpot.x++;
            break;
        case Direction::BOTTOM :
            pmSpot.x = pmPos.x;
            pmSpot.y = (pmPos.y + 2 >= tm->getYLimit() )
                ? tm->getYLimit() - 1
                : pmPos.y + 2;
            while( tm->getTile(pmSpot) != TileType::GROUND )
                pmSpot.y--;
            break;
        case Direction::RIGHT :
            pmSpot.x = (pmPos.x + 2 >= tm->getXLimit() )
                ? tm->getXLimit() - 1
                : pmPos.x + 2;
            pmSpot.y = pmPos.y;
            while( tm->getTile(pmSpot) != TileType::GROUND )
                pmSpot.x--;
            break;
        default: 
            break;
        }
        // Compute a square TileType::GROUND in the map
        float tmp = 1.1;
        do
        {
            do
            {
                tmp -= 0.1;
                if( pmSpot.x < shadowPos.x )
                    endPos.x = pmSpot.x - (shadowPos.x - pmSpot.x) * tmp;
                else
                    endPos.x = pmSpot.x + (pmSpot.x - shadowPos.x) * tmp;

                if( pmSpot.y < shadowPos.y )
                    endPos.y = pmSpot.y - (shadowPos.y - pmSpot.y) * tmp;
                else
                    endPos.y = pmSpot.y - (pmSpot.y - shadowPos.y) * tmp;
            } while( endPos.x < 0 || endPos.x >= tm->getXLimit() ||
                     endPos.y < 0 || endPos.y >= tm->getYLimit() );
        } while( tm->getTile(endPos) != TileType::GROUND );
        this->getPathFinding()->path( this->path, this->getPosition(), endPos);
    }
    if( !this->getPath()->empty() )
        reposition();
    else
    {
        Vector2f pmPos = this->getPacman()->getPosition(),
            bhPos = this->getPosition();
        this->getPathFinding()->path( this->path, bhPos, pmPos );
    }
}

}
