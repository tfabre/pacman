#include "pacman/model/character/enemy/behavior/BackHome.hpp"

namespace pm
{

BackHome::BackHome( const Vector2f & pos, const float & speed, 
                    const float & size, TileMap* tm, Pacman* const pm,
                    PathFinding* pf )
    : EnemyBehavior( pos, speed, size, tm, pm, pf )
{}

void BackHome::move()
{
    if( finishMove() )
        return;
    else if( this->path->empty() )
    {
        const Vector2f homePos = this->getTileMap()->getGhostHomeSquare(),
            bhPos = this->getPosition();
        this->getPathFinding()->path( this->path, bhPos, homePos );
    }
    if( !this->getPath()->empty() )
        reposition();
    else
    {
        this->setState(EnemyState::NORMAL);
    #ifdef DEBUG
        std::cout << "No Path BackHome!" << std::endl;
    #endif
    }
}

}
