#include "pacman/model/ConfModel.hpp"

namespace pm
{

const std::string ConfModel::PATH::ASSET{"asset/"};
const std::string ConfModel::PATH::ASSET_MAP{ASSET + "map/"};

const std::string ConfModel::FILE::FIRST_LEVEL{PATH::ASSET_MAP + "level.pm"};
const std::string ConfModel::FILE::SCORE{PATH::ASSET + "score.spm"};
const std::string ConfModel::FILE::DEFAULT_MAP_EDITOR_LEVEL{PATH::ASSET_MAP+ "defaultMapEditor_28_31.pm"};

const float ConfModel::CONST::FRUIT_APPEAR_DURATION{10};

const float ConfModel::PACMAN::DEFAULT_SIZE{0.4};
const float ConfModel::PACMAN::X_ORIGINAL_POSITION{13.5};
const float ConfModel::PACMAN::Y_ORIGINAL_POSITION{23.5};
const float ConfModel::PACMAN::MIN_SPEED{.125};
const float ConfModel::PACMAN::DEFAULT_SPEED{.2};
const float ConfModel::PACMAN::SPEED_SUP{.25};
const float ConfModel::PACMAN::MAX_SPEED{.5};
const Direction ConfModel::PACMAN::DEFAULT_DIRECTION{LEFT};

const float ConfModel::GHOST::DEFAULT_SIZE{0.4};
const float ConfModel::GHOST::LEAK_SPEED{0.1};
const float ConfModel::GHOST::DEFAULT_SPEED{ConfModel::PACMAN::DEFAULT_SPEED};
const float ConfModel::GHOST::BACK_HOME_SPEED{0.1};
const float ConfModel::GHOST::POWER_PELLET_DURATION{10.f};
const float ConfModel::GHOST::PATROLLING_DURATION{7.f};
const float ConfModel::GHOST::CHASING_DURATION{20.f};

}
