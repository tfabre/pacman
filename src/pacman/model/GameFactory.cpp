#include "pacman/model/GameFactory.hpp"
#include "pacman/model/ConfModel.hpp"
#include "pacman/util/Vector2.hpp"

namespace pm
{

GameFactory* GameFactory::factory{nullptr};

GameFactory::GameFactory()
{
    this->map = new TileMap();
    this->level = new Level(ConfModel::FILE::FIRST_LEVEL, this->map);
    this->level->readNextLevel();
    Vector2f pos = this->map->getPacmanCoordinate();
    this->pacman = new Pacman(pos, ConfModel::PACMAN::DEFAULT_SPEED, this->map,
                              pos, LEFT);
    this->state = new GameState(this->pacman, this->level);
    this->map->addObserver(*this->level);
    this->map->addObserver(*this->state);
    this->pathFinding = new PathFinding(this->map);
}

GameFactory::~GameFactory()
{
    delete this->state;
    delete this->pacman;
    delete this->pathFinding;
    delete this->map;
    delete this->level;
}

Pacman* GameFactory::getPacman()
{
    return getFactory()->pacman;
}

TileMap* GameFactory::getTileMap()
{
    return getFactory()->map;
}

GameState* GameFactory::getGameState()
{
    return getFactory()->state;
}

PathFinding* GameFactory::getPathFinding()
{
    return getFactory()->pathFinding;
}

Level* GameFactory::getLevel()
{
    return getFactory()->level;
}

void GameFactory::deleteGameModel()
{
    delete getFactory();
    getFactory() = nullptr;
}

GameFactory*& GameFactory::getFactory()
{
    if (factory == nullptr)
    {
        factory = new GameFactory();
    }

    return factory;
}

}
