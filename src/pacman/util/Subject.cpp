#include "pacman/util/Subject.hpp"

namespace pm
{

Subject::~Subject() {}
    
void Subject::addObserver(Observer & obs)
{
    this->observers.push_back(&obs);
}

void Subject::delObserver(Observer & obs)
{
    std::vector<Observer*>::iterator it;
    for (it = this->observers.begin(); *it != &obs
             && it != this->observers.end(); ++it);
    this->observers.erase(it);
}

void Subject::notify(const Event & event)
{
    for (Observer* & obs: this->observers)
        obs->actualisate(event);
}

}
