#include "pacman/util/Timer.hpp"

namespace pm
{

Timer::Timer(): begin(), lapBegin(), isPaused(false)
{}

Timer::~Timer()
{}

void Timer::start()
{
    begin = Clock::now();
}

void Timer::pause()
{
    if (!isPaused)
    {
        lapBegin = Clock::now();
        isPaused = true;
    }
}

void Timer::resume()
{
    if (isPaused)
    {
        fsec lap = Clock::now() - lapBegin;
        begin += std::chrono::duration_cast<ClockDuration>(lap);
        isPaused = false;
    }
}

fsec Timer::getElapsedTime()
{
    if (isPaused)
    {
        fsec lap = Clock::now() - lapBegin;
        lapBegin = Clock::now();
        begin += std::chrono::duration_cast<ClockDuration>(lap);
    }
    fsec elasped = Clock::now() - begin;
    return elasped;
}

bool Timer::isInPause() const
{
    return this->isPaused;
}

TimeStamp Timer::getActualTimeStamp()
{
    return Clock::now().time_since_epoch().count();
}

std::string Timer::actualTimeToString(const std::string& format)
{
    return timePointToString(Clock::now(), format);
}

std::string Timer::timePointToString(const TimePoint& tp,
                                     const std::string& format)
{
    std::time_t time = Clock::to_time_t(tp);
    char buff[100];
    if (std::strftime(buff, sizeof(buff),format.c_str(), std::localtime(&time)))
        return std::string(buff);
    return {""};
}

} // namspace pm
