#include "pacman/controller/ScoreCtrl.hpp"
#include "pacman/view/Game.hpp"
#include "pacman/view/ScoreView.hpp"

#include <iostream>

namespace pm
{

ScoreCtrl::~ScoreCtrl()
{}

void ScoreCtrl::checkEvent(const sf::Event& event)
{
    if (event.type == sf::Event::KeyPressed)
    {
        ScoreView* sv = reinterpret_cast<ScoreView*>(this->getView());
        Game* game = sv->getParent();
        switch (event.key.code)
        {
        case sf::Keyboard::Escape:
            game->setCurrentView(ViewType::MENU);
            break;
        default: break;
        }
    }
}

}
