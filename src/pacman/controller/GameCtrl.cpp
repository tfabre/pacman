#include "pacman/controller/GameCtrl.hpp"
#include "pacman/view/ConfView.hpp"
#include "pacman/view/Game.hpp"

namespace
{

/*!
** \brief Detect if there is a collision between mouse position and Text rect
**
** \return True if the collision is there
*/
bool haveCollision(const sf::Vector2i & mousePos, const sf::Text & txt)
{
    const sf::FloatRect rect = txt.getGlobalBounds();
    const sf::Vector2f pos = txt.getPosition();
    return (mousePos.x > pos.x) &&
        (mousePos.x < (pos.x + rect.width)) && 
        (mousePos.y > rect.top) &&
        (mousePos.y < (rect.top + rect.height));
}

}

namespace pm
{

GameCtrl::~GameCtrl() {}

void GameCtrl::checkEvent(const sf::Event & event)
{
    Game* view = dynamic_cast<Game*>(getView());
    if (event.type == sf::Event::MouseMoved)
    {
        sf::Vector2i mousePos{event.mouseMove.x, event.mouseMove.y};
        bool focus{false};

        for (sf::Text & txt: view->getMenuTexts())
        {
            focus = haveCollision(mousePos, txt);
            view->focusText(txt, focus);
            if (focus)
                break;
        }
    }
    else if (event.type == sf::Event::MouseButtonPressed)
    {
        sf::Vector2i mousePos = sf::Mouse::getPosition(view->getWindow());
        for (sf::Text & txt: view->getMenuTexts())
        {
            if (haveCollision(mousePos, txt))
            {
                if (txt.getString() == ConfView::TEXT::PLAY)
                    view->setCurrentView(ViewType::PACMAN);

                else if (txt.getString() == ConfView::TEXT::SCORE)
                {
                    view->setCurrentView(ViewType::SCORES);
                }
                if (txt.getString() == ConfView::TEXT::MAP_EDITOR)
                    view->setCurrentView(ViewType::MAP_EDITOR);
                break;
            }
        }
    }
    else if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::Space)
            view->setCurrentView(ViewType::PACMAN);
    }
}

}
