#include "pacman/controller/Controller.hpp"

namespace pm
{

Controller::Controller(View* const v): view(v)
{}

Controller::~Controller() {}

View* Controller::getView() const
{
    return this->view;
}

void Controller::checkExtraEvent()
{}

}
