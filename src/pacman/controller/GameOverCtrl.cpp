#include "pacman/controller/GameOverCtrl.hpp"
#include "pacman/view/Game.hpp"
#include "pacman/view/GameOverView.hpp"

#include <iostream>

namespace pm
{

GameOverCtrl::~GameOverCtrl()
{}

void GameOverCtrl::checkEvent(const sf::Event& event)
{
    if (event.type == sf::Event::KeyPressed)
    {
        GameOverView* go = reinterpret_cast<GameOverView*>(this->getView());
        Game* game = go->getParent();
        game->rebuildView(ViewType::PACMAN);
        switch (event.key.code)
        {
        case sf::Keyboard::Escape:
            game->setCurrentView(ViewType::MENU);
            break;
        case sf::Keyboard::Space:
        case sf::Keyboard::Return:
            game->setCurrentView(ViewType::PACMAN);
            break;
        default: break;
        }
    }
}

}
