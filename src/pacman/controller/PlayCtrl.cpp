#include "pacman/controller/PlayCtrl.hpp"
#include "pacman/model/GameFactory.hpp"
#include "pacman/view/Play.hpp"

namespace pm
{

PlayCtrl::~PlayCtrl()
{}

void PlayCtrl::checkEvent(const sf::Event & event)
{
    if (event.type == sf::Event::KeyPressed)
    {
        Play* view = reinterpret_cast<Play*>(getView());
        switch (event.key.code)
        {
        case sf::Keyboard::Up:
            view->changePacmanDirection(TOP);
            break;
        case sf::Keyboard::Down:
            view->changePacmanDirection(BOTTOM);
            break;
        case sf::Keyboard::Right:
            view->changePacmanDirection(RIGHT);
            break;
        case sf::Keyboard::Left:
            view->changePacmanDirection(LEFT);            
            break;
        case sf::Keyboard::Escape:
            if (view->isPaused())
                view->resume();
            else
                view->pause();
            break;
        default: break;
        }
    }
}

void PlayCtrl::checkExtraEvent()
{
    if (GameFactory::getGameState()->getLifeNumber() == uint8(-1))
    {
        Play* view = reinterpret_cast<Play*>(getView());
        view->getParent()->setCurrentView(ViewType::GAME_OVER);
        GameFactory::deleteGameModel();
    }
}

}
