#include "pacman/controller/MapEditorCtrl.hpp"
#include "pacman/model/GameFactory.hpp"
#include "pacman/view/MapEditor.hpp"
#include "pacman/view/ConfView.hpp"

namespace pm
{

MapEditorCtrl::MapEditorCtrl(View* const v): Controller(v)
{
    currentPos.x = 0;
    currentPos.y = 0;
}

MapEditorCtrl::~MapEditorCtrl() {}

void MapEditorCtrl::checkEvent(const sf::Event& event)
{
    MapEditor* view = reinterpret_cast<MapEditor*>(getView());

    if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::Escape)
        {
            GameFactory::getLevel()->resynch();
            MapEditor* ed = reinterpret_cast<MapEditor*>(this->getView());
            Game* game = ed->getParent();
            game->setCurrentView(ViewType::MENU);
        }
    }
    else if (event.type == sf::Event::MouseButtonPressed)
    {
        sf::Vector2i mousePos = sf::Mouse::getPosition(view->getWindow());

        //adding button selected
        if (mousePos.y <= (int)ConfView::PACVIEW::TEXTURE_TILE_HEIGHT)
        {
            //current position is max pos possible
            if(currentPos.x < (int)view->tilemap->getXLimit() && currentPos.y < (int)view->tilemap->getYLimit())
            {
                uint8 numSelectedButton = mousePos.x/ConfView::PACVIEW::TEXTURE_TILE_WIDTH;

                switch(numSelectedButton)
                {
                    case ADD_WALL_TILE:
                        view->tilemap->insertWallAt(currentPos.x++, currentPos.y);
                        break;

                    case ADD_NONE_TILE:
                        view->tilemap->insertElementAt(currentPos.x++, currentPos.y, NONE);
                        break;

                    case ADD_DOT_TILE:
                        view->tilemap->insertElementAt(currentPos.x++, currentPos.y, DOT);
                        break;

                    case ADD_POWER_PELLET_TILE:
                        view->tilemap->insertElementAt(currentPos.x++, currentPos.y, POWER_PELLET);
                        break;

                    case ADD_GHOST_WALL_TILE:
                        view->tilemap->insertGhostDoorAt(currentPos.x++, currentPos.y);
                        break;

                    case ADD_PACMAN_TILE:
                        view->tilemap->insertElementAt(currentPos.x++, currentPos.y, PACMAN_NUMBER_FOR_FILE);
                        view->tilemap->insertElementAt(currentPos.x++, currentPos.y, PACMAN_NUMBER_FOR_FILE);
                        break;

                    case ADD_FRUIT_TILE:
                        view->tilemap->insertElementAt(currentPos.x++, currentPos.y, FRUIT_NUMBER_FOR_FILE);
                        view->tilemap->insertElementAt(currentPos.x++, currentPos.y, FRUIT_NUMBER_FOR_FILE);
                        break;

                    case SAVE_LEVEL:
                        Level::saveNewLevel(view->tilemap);
                        break;
                        
                    default:
                    break;
                }

                if(currentPos.x == 28)
                {
                    printf("%d\n", view->tilemap->getXLimit());
                    currentPos.x = 0;
                    currentPos.y++;
                }
            }

        }

        if( mousePos.x >= 0 && mousePos.y >= 3*(int)ConfView::PACVIEW::TILE_HEIGHT )
        {
            currentPos.x = mousePos.x / (ConfView::PACVIEW::TILE_WIDTH);
            currentPos.y = (mousePos.y - 3*ConfView::PACVIEW::TILE_HEIGHT) / (ConfView::PACVIEW::TILE_HEIGHT);
        }

        if(currentPos.y > 30)
            currentPos.y = 30;

        view->actualisateMarkerPos(currentPos.x, currentPos.y);
    }

}


} // namespace pm
