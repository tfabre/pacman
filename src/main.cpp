#include "pacman/view/Game.hpp"
#include "pacman/model/GameFactory.hpp"

int main()
{
    pm::Game* pacman = new pm::Game();
    pacman->run();
    delete pacman;
    pm::GameFactory::deleteGameModel();
    return 0;
}
